///////////////INCLUDE//////////////

#include <PubSubClient.h>                       //Libreria per protocollo MQTT
#include <Adafruit_GFX.h>                       //Libreria generica dysplay
#include <Adafruit_ST7735.h>                    //Libreria Modello display
#include <SPI.h>                                //Libreria protocollo SPI per display
#include "HX711.h"                              //Libreria per modulo controllo cella di carico
#include "SoftwareSerial.h"                     //Libreria Per la comunicazione seriale, personalizzata per ESP32
#include "SparkFun_DE2120_Arduino_Library.h"    //Libreria Lettore codice a barre
#include "WiFi.h"                               //Libreria per connessione WiFi

////////////////////////////////////////////

//////////////DEFINE///////////////////////

#define BTN_SAVE      34
#define RX            16                           //Pin RX
#define TX            17                           //Pin TX
#define BUFFER_LEN    40                           //Dim Buffer per lettore codBar 
#define TFT_PIN_CS    27                           //Pin CS
#define TFT_PIN_DC    26                           //Pin DC
#define TFT_PIN_RST   14                           //Pin RST
#define TFT_PIN_MOSI  13                           //Pin SPI MOSI 
#define TFT_PIN_SCLK  12                           //Pin SPI CLK 
#define LC_SDA        21                           //Pin SDA I2C Cella di carico
#define LC_SCL        22                           //Pin SCL I2C Cella di carico
#define N_SIZE        4                            //Vettore con i nutrienti calcolati, sono 4 valori: Calorie, Grassi, Proteine, Carboidrati

//////////////////////////////////////////////


//Per una questione di TESTING all'interno del codice ci sono molte SerialPrint, così è possibile aprire il monitor seriale
//e verificare il corretto funzionamento del tutto.


////////////INIT/////////////////////////////

WiFiClient      bilanciaClient;                      //Client per mqtt
PubSubClient    mqttClient(bilanciaClient);
SoftwareSerial  softSerial(RX, TX);                 //Inizializzazione comunicazione seriale lettore
DE2120          scanner;                            //"Oggetto" scanner
HX711 scale;                                       //Modulo cella di carico

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_PIN_CS, TFT_PIN_DC, TFT_PIN_MOSI, TFT_PIN_SCLK, TFT_PIN_RST);   //Display TFT

//////////////////////////////////////////////

///////VARIABILI GLOBALI//////////////////////

const char*   ssid = "CasaCelentano";
const char*   password = "LogitechG402";
const char*   mqttServer = "broker.hivemq.com";
const int     mqttPort = 1883; 
const char    ID[10] = "A001";                         //ID Bilancia
char          scanBuffer[BUFFER_LEN];
char          nome[50];                             //Nome prodotto 
char          stringaInvio[200];                     //Stringa contentente tutti i dati necessari al salvataggio da parte dell'utente di un determinato prodotto pesato
float         peso = 0;
float         valori_N[4];
float         carboidrati,grassi,proteine,calorie;
float         abilitaSalvataggio = false;
float         calibrazione = -87;                   //Fattore di calibrazione per la cella di carico                          
bool          abilitaCalcolo = false;               //Questa variabile diventa vera quando la bilancia riceve i valori
                                                    //dal database, attiva la funzione che calcola i nutrienti in base al peso corrente
//////////////////////////////////////////////

////////////MQTT/////////////////////////////

void setupMQTT(){
  //Setup del client per connessione al broker MQTT
  
  mqttClient.setServer(mqttServer, mqttPort);
  mqttClient.setCallback(callback);
}

void callback(char* topic, byte* payload, unsigned int lenght){
  //Funzione callback, all'arrivo di un messaggio si attiva, nel nostro caso
  //riceverà un messaggio contenente i valori nutrizionali per 100g dell'alimento scansionato
  //abilita il calcolo dei valori
  
  int y = 0;
  int somma = 0;
  int j = 0;
  char buffer_messaggio[100];
  for(int i = 0; i < lenght; i++)
    buffer_messaggio[i] = payload[i];
  
  buffer_messaggio[lenght] = '\0';      
    
  char rcvID[10];
  char* token_1 = strtok((char*)buffer_messaggio, "-");
  
  strcpy(rcvID, token_1);
  token_1 = strtok(NULL, "-");
  strcpy(buffer_messaggio, token_1);
    
  if(strcmp(ID, rcvID) == 0){
      
    char* token = strtok((char*)buffer_messaggio, " ");
      while(token != NULL){
        if(j==0){
          j++;
          strcpy(nome, token);
          Serial.println(nome);
          if(strcmp(nome,"Prodotto_inesistente") == 0){
            abilitaCalcolo = false;  
            tft.fillRect(1,53,157,80, ST77XX_BLACK);
            token = NULL;
          }
          else{
            abilitaCalcolo = true;  
            string_replace(nome, '_', ' ');
            tft.fillRect(1,53,157,80, ST77XX_BLACK);
          }
        }
        else{
          valori_N[y] = atoi(token);
          Serial.println(valori_N[y]);
          y++;
        }
        token = strtok(NULL, " ");
      }
      Serial.println();   
    }
}

void reconnect(){
  //Questa connettersi al broker MQTT, infatti verrà chiamata
  //nel void loop così da essere sempre connessi al broker e iscritti al topic
  
  while(!mqttClient.connected()){
    Serial.println("Mi riconnetto al broker...");
    String clientId = "BilanciaESP32";
    clientId += String(random(0xffff), HEX);

    if(mqttClient.connect(clientId.c_str())){
      Serial.println("Connesso al broker");
      mqttClient.subscribe("Bilancia/Valori");
    }
  }
}


////////////////////////////////////////////


//////////Funzioni Personali////////////////

void letturaCodice(){
  //Questa funzione va in loop nel void loop, aspetta che lo scanner rilevi un codice a barre
  //per poi inviarlo al broker MQTT.
  char codice[50];
  
  if(scanner.readBarcode(scanBuffer, BUFFER_LEN)){
      Serial.print("Codice trovato:");
      sprintf(codice, "%s,%s", ID, scanBuffer);
      mqttClient.publish("Bilancia/Codice", codice);
      
    for(int i = 0; i < strlen(scanBuffer); i++)
      Serial.print(scanBuffer[i]);
      
    Serial.println();
  } 
}




void salvaValori(){
  Serial.println(stringaInvio);
  if(abilitaCalcolo && abilitaSalvataggio){
    mqttClient.publish("Bilancia/Salva", stringaInvio);
    tft.fillRect(1,108,158,30, ST77XX_BLACK);
    tft.setTextSize(1);
    tft.setTextColor(ST77XX_GREEN);
    tft.setCursor(1,110); 
    tft.print("Prodotto Salvato");
    
  }
  abilitaSalvataggio = false;
}

void rettangoloInt(){
  //Questa funzione serve per stampare il rettangolo dove va inserito il valore del peso sul display
  tft.fillScreen(ST77XX_BLACK);
  tft.fillRect(2, 2, 157, 49, ST77XX_WHITE);
  tft.drawRect(1,1,158,50, ST77XX_RED);
}

void pesa(){
  float peso_curr = scale.get_units();
  
  if(peso_curr <= 0) peso_curr = 0.00;
  
  if(peso_curr == 0) peso = 0.00;
  
  else if(abs(peso_curr - peso) >= 10){
    char pesoStampa[20];
    peso = peso_curr;
    sprintf(pesoStampa, "%.2fg", peso);
    
    tft.fillRect(10,10,140,30, ST77XX_BLACK);
    tft.setCursor(48, 20);
    tft.setTextColor(ST77XX_RED);
    tft.setTextSize(2.5);
    tft.println(pesoStampa);
    if(abilitaCalcolo) calcolaNutrienti();
  }
}

void calcolaNutrienti(){
  char nomeVir[50];
  
  calorie     = valori_N[0]*(peso/100);
  carboidrati = valori_N[1]*(peso/100);
  grassi      = valori_N[2]*(peso/100);
  proteine    = valori_N[3]*(peso/100);

  char calorieStampa[20];
  char carboidratiStampa[20];
  char grassiStampa[20];
  char proteineStampa[20];
  
  strcpy(nomeVir, nome);
  string_replace(nomeVir, ' ', '_');
  strcat(nomeVir, ",");
  
  sprintf(stringaInvio, "%s,%.2f,%.2f,%.2f,%.2f,%.2f,", ID, peso, calorie, carboidrati, grassi, proteine);

  
  strcat(stringaInvio, nomeVir);
  //strcat(stringaInvio, scanBuffer);
  
  sprintf(calorieStampa, "%.2fkcal", calorie);
  sprintf(carboidratiStampa, "%.2fg", carboidrati);
  sprintf(grassiStampa, "%.2fg", grassi);
  sprintf(proteineStampa, "%.2fg", proteine);
  
  tft.setTextSize(1);
  tft.setTextColor(ST77XX_RED);
  tft.setCursor(3,70);
  tft.print("Calorie:");
  tft.setCursor(3,78);
  tft.print("Carboidrati:");
  tft.setCursor(3,86);
  tft.print("Grassi:");
  tft.setCursor(3,94);
  tft.print("Proteine:");
  
  tft.fillRect(79,69,157,40, ST77XX_BLACK);
  delay(500);
  tft.setTextSize(1);
  tft.setTextColor(ST77XX_GREEN);
  tft.setCursor(1,54);
  tft.print(nome);
  tft.setTextColor(ST77XX_RED);
  tft.setCursor(80,70);
  tft.print(calorieStampa);
  tft.setCursor(80,78);
  tft.print(carboidratiStampa);
  tft.setCursor(80,86);
  tft.print(grassiStampa);
  tft.setCursor(80,94);
  tft.print(proteineStampa);
  
}

void string_replace(char* string, char oldc, char newc){
  int i = 0;
  while(string[i]!='\0'){
    if(string[i] == oldc) string[i]=newc;
    i++;
   }
}


/////////////////////////////////////////////


void setup() {
  Serial.begin(115200);          //Begin monitor Seriale

  pinMode(BTN_SAVE, INPUT);
  
  tft.initR(INITR_BLACKTAB);     //Init display
  tft.setRotation(1);            //Ruoto il Display nella posizione corretta
  tft.fillScreen(ST77XX_BLACK);  //BackScreen del display nero
  scale.begin(LC_SDA, LC_SCL);   //Init cella di carico
  scale.set_scale();
  scale.tare();                  //Tara iniziale
  scale.set_scale(calibrazione); //Tara cella di carico in base al fattore di cal


  //Inizializzazione e verifica scanner
  if(scanner.begin(softSerial) == false){
    Serial.println("Lo scanner non è online");
    while(1)
    ;
  }
  Serial.println("Scanner Online");
  
  //Connessione e verifica della wifi
  WiFi.begin(ssid, password);   //Begin per connettermi alla linea WiFi
  while(WiFi.status() != WL_CONNECTED){
    //Rimane nel ciclo finché non riesce a connettersi alla linea Wifi
    delay(500);
    Serial.println("Mi connetto al WiFi");
  }
  Serial.println("Connesso alla linea Wifi");
  setupMQTT();

  rettangoloInt();    //Stampo l'interfaccia grafica sul display
  
  
}


void loop() {
  
  if(!mqttClient.connected())   //Funzione di connessione o riconnesione al broker
    reconnect(); 
  mqttClient.loop();

  if(abilitaSalvataggio == false){     
    if(digitalRead(BTN_SAVE) == HIGH)     //Verifica la pressione del pulsante fisico
                                          //per il salvataggio del prodotto scansionato e pesato
      abilitaSalvataggio = true;
  }else if (abilitaSalvataggio == true) salvaValori();
  
  pesa();         //Fuzione per pesare in tempo reale
  
  letturaCodice();  //Funzione di attesa che venga letto un codice a barre

  
  delay(500);
}
