import {Component, OnInit, Input} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
    selector: 'app-s-spinner',
    template: `<ngx-spinner
        bdColor="rgba(51, 51, 51, 0.8)"
        size="default"
        type="ball-spin-clockwise"
        ><p style="color: white">Please Wait.</p></ngx-spinner
    >`
})
export class SpinnerComponent implements OnInit {
    private _visible: boolean;

    constructor(private spinner: NgxSpinnerService) {}

    ngOnInit() {
        if (this._visible) this.spinner.show();
    }

    @Input()
    set visible(val: any) {
        //console.log('***** set visible '+val)
        this._visible = val;

        if (this._visible) this.spinner.show();
        else this.spinner.hide();
    }
}
