import {FormGroup} from '@angular/forms';
import CodiceFiscale  from 'codice-fiscale-js';

// custom validator to check Italian Code
export function CodiceFiscaleValidator(controlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        control.setErrors(CodiceFiscale.check(control.value) ? null : {invalidFormat: true});
    };
}