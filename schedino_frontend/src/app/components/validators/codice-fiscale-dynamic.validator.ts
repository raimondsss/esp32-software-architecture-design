import {FormGroup} from '@angular/forms';
import CodiceFiscale  from 'codice-fiscale-js';

// custom validator to check Italian Code
export function CodiceFiscaleDynamicValidator(controlType: string, controlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const ctrltype = formGroup.controls[controlType];
        if(ctrltype.value == 'pers.Fisica'){
            control.setErrors(CodiceFiscale.check(control.value) ? null : {invalidFormat: true});
        }
        
    };
}