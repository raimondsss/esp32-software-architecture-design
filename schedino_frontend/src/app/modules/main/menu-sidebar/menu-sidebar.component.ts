import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '@services/app.service';
import {UserInfoService} from '@services/user-info.service';
import * as $ from 'jquery';
import * as AdminLte from 'admin-lte';

@Component({
    selector: 'app-menu-sidebar',
    templateUrl: './menu-sidebar.component.html',
    styleUrls: ['./menu-sidebar.component.scss']
})
export class MenuSidebarComponent implements OnInit {
    public user;
    public superuser: boolean;
    public companyAdmin: boolean;
    public companyController: boolean;
    public utente: boolean;
    public scoutuser: boolean;
    public compagniaId: number;
    public menu: any;
    @Input() opened: boolean;
    constructor(
        public appService: AppService,
        private userInfoService: UserInfoService,
    ) {}

    ngOnInit() {
        //this.user = this.appService.user;
        this.user = this.userInfoService.getUserInfo();
        this.superuser = this.userInfoService.isSuperuser();
        this.companyAdmin = this.userInfoService.isCompanyAdmin();
        this.utente = this.userInfoService.isUser();

        this.compagniaId = this.userInfoService.getOrganizationId();

        this.menu = [
            {
                name: 'Dashboard',
                path: ['/'],
                icon: 'fas fa-tachometer-alt',
                visible: true
            },
            {
                name: 'Utenti',
                path: ['/user'],
                icon: 'fas fa-users',
                visible: this.superuser 
            },

            {
                name: 'Bilancini',
                path: ['/schedini'],
                icon: 'fas fa-microchip',
                visible: this.superuser 
            },
            {
                name: 'Prodotti',
                path: ['/prodotti'],
                icon: 'fas fa-cart-plus',
                visible: this.superuser 
            },
            {
                name: 'Pasti',
                path: ['/pasti'],
                icon: 'fas fa-bacon',
                visible: this.utente 
            },
        ];
    }

    ngAfterViewInit() {
        $('[data-widget="treeview"]').each(function() {
            AdminLte.Treeview._jQueryInterface.call($(this), 'init');
        });
    }
}
