import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {AppService} from '@services/app.service';
import {UserInfoService} from '@services/user-info.service';
import {AppConfig} from '../../../app-config';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Output() toggleMenuSidebar: EventEmitter<any> = new EventEmitter<any>();
    public searchForm: FormGroup;    
    public compagniaId: any;
    public logo = null;
    public nomeCompagnia = null;
    public item: boolean = true;

    constructor(
        private appService: AppService,
        private userInfoService: UserInfoService,
    ) {}

    ngOnInit() {
        this.compagniaId = this.userInfoService.getOrganizationId();

        /*this.theService.getEntity(this.compagniaId).subscribe((data) => {
            this.logo = data.data.logo;
            this.nomeCompagnia = data.data.ragioneSociale;
        });*/

        this.searchForm = new FormGroup({
            search: new FormControl(null)
        });
    }

    hasImage(): boolean {
        if (this.logo && this.logo.length) return true;

        return false;
    }


    getImageUrl() {
        if (this.logo === null)
            return '#';
        return AppConfig.settings.apiServer.apiFilestoragePath+"compagnia$"+this.compagniaId+"$"+this.logo+"/001";
    }

    logout() {
        this.appService.logout();
    }
}
