import {
    Component,
    OnInit,
    Renderer2,
    OnDestroy,
    HostBinding
} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {AppService} from '@services/app.service';
import {SchedinoService} from '@services/api/schedino.service';
import {UserService} from '@services/api/user.service';
import {ToastrService} from 'ngx-toastr';
import {NgxAlertService} from '@services/ngxalert.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
    @HostBinding('class') class = 'register-box';

    public registerForm: FormGroup;
    public isAuthLoading = false;
    public isGoogleLoading = false;
    public isFacebookLoading = false;
    isLoading = false;
    schedini: any[];
    schedinoId: any;
    schedino: any;

    constructor(
        private renderer: Renderer2,
        private toastr: ToastrService,
        private appService: AppService,
        private schedinoService: SchedinoService,
        private userService: UserService,
        private ngxAlertService: NgxAlertService,
        private router: Router,
    ) {}

    ngOnInit() {
        this.renderer.addClass(
            document.querySelector('app-root'),
            'register-page'
        );
        this.registerForm = new FormGroup({
            nome: new FormControl(null, [Validators.required]),
            cognome: new FormControl(null, [Validators.required]),
            email: new FormControl(null, Validators.required),
            password: new FormControl(null, [Validators.required]),
            matchingPassword: new FormControl(null, [Validators.required]),
            obbiettivo: new FormControl(null, [Validators.required]),
            schedino: new FormControl(null, [Validators.required]),
        });

        this.schedinoService.getList().subscribe((pagedData) => {
            this.schedini = pagedData.items;
            console.log(this.schedini);
        });
    }

    async registerByAuth() {
        if(this.registerForm.value.password != this.registerForm.value.matchingPassword)
            return alert("Le password non coincidono")

        if (this.registerForm.valid ) {
            console.log('register');
            this.isAuthLoading = true;

            const user = {
                firstName:this.registerForm.value.nome,
                lastName:this.registerForm.value.cognome,
                username:this.registerForm.value.email,
                email: this.registerForm.value.email,
                password: this.registerForm.value.password,
                matchingPassword: this.registerForm.value.matchingPassword,
                obbiettivoGiornaliero: this.registerForm.value.obbiettivo,
                schedino: this.registerForm.value.schedino.toUpperCase(),
            }
            
            console.log(user);

            this.userService.registerUser(user).subscribe((data) => {
                if (data.message === 'success') {
                    this.ngxAlertService.success(
                        '',
                        'Registrazione avvenuta con successo'
                    );

                    this.router.navigate(['/login']);
                } else {
                    console.log('boh');
                    this.ngxAlertService.error('', data.result);
                }
                this.isLoading = false;
            });
            this.isAuthLoading = false;
        } else {
            this.toastr.error('Form is not valid!');
        }
    }

    async registerByGoogle() {
        this.isGoogleLoading = true;
        await this.appService.registerByGoogle();
        this.isGoogleLoading = false;
    }

    async registerByFacebook() {
        this.isFacebookLoading = true;
        await this.appService.registerByFacebook();
        this.isFacebookLoading = false;
    }

    onChangeSchedino(id) {   
        console.log('prova', id); 
        let schedinoTest = this.schedini.find(c => c.codice.toString().toUpperCase() === id.toUpperCase());
        
        if(!schedinoTest){
            alert('bilancino non esistente');
            this.registerForm.controls['schedino'].setValue('');
        }else{
            if(schedinoTest.utenti > 0){
                alert('bilancino già associato');
                this.registerForm.controls['schedino'].setValue('');
            }else{
                this.schedino = id.toUpperCase();
            }
        }         
    }
    onChangeEmail(id) {   
        console.log('email', id); 
        
        if(!id.includes("@") || id.includes("@@")){
            alert('email non valida');
            this.registerForm.controls['email'].setValue('');
        }  
    }

    ngOnDestroy() {
        this.renderer.removeClass(
            document.querySelector('app-root'),
            'register-page'
        );
    }
}
