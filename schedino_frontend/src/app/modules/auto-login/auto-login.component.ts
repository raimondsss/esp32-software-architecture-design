import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '@services/api/login.service';

@Component({
  selector: 'app-auto-login',
  templateUrl: './auto-login.component.html',
  styleUrls: ['./auto-login.component.css']
})
export class AutoLoginComponent implements OnInit {
  errMsg: string = '';
  homePage: string;
  submitted = true;
  token: string;
  landingPage: string;
  errorPage: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private theService: LoginService,
  ) { }

  ngOnInit(): void {
    this.theService.logout(false);
    this.errorPage = '/login';

    this.route.queryParams.subscribe(params => {
        this.token = params['token'];
        this.landingPage = params['action'];

        this.checkToken();
    });


  }

  checkToken() {
    this.theService.checkToken(this.token)
        .subscribe(resp => {
                if (resp.length == 0)
                    return;
                if (resp.success == false) {
                    switch (resp.error) {
                        case 200:
                        case 400:
                        case 401:
                            this.errMsg = 'Username or password is incorrect';
                            break;
                        case 404:
                            this.errMsg = 'Service not found';
                            break;
                        case 408:
                            this.errMsg = 'Request Timedout';
                            break;
                        case 500:
                            this.errMsg = 'Internal Server Error';
                            break;
                        default:
                            this.errMsg = 'Server Error';
                            break;
                    }

                    this.router.navigate([this.errorPage]);
                } else {
                    if (resp.oauth === undefined || resp.oauth.accessToken === undefined || resp.oauth.accessToken === "INVALID") {
                        this.errMsg = 'Username or password is incorrect';
                        return;
                    }

                    this.router.navigate([this.landingPage]);
                }
            },
            errResponse => {
                switch (errResponse.status) {
                    case 400:
                    case 401:
                        this.errMsg = 'Username or password is incorrect';
                        break;
                    case 404:
                        this.errMsg = 'Service not found';
                        break;
                    case 408:
                        this.errMsg = 'Request Timeout';
                        break;
                    case 500:
                        this.errMsg = 'Internal Server Error';
                        break;
                    default:
                        this.errMsg = 'Server Error';
                        break;
                }

                this.router.navigate([this.errorPage]);
            }
        );
}

}
