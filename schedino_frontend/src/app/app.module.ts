import {BrowserModule} from '@angular/platform-browser';
import {NgModule, APP_INITIALIZER} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';

import {AppButtonComponent} from './components/app-button/app-button.component';
import {SpinnerComponent} from './components/spinner/spinner.component';
import {ConfirmationModalComponent} from './components/dialog/confirmation-modal.component';
import {MenuItemComponent} from './components/menu-item/menu-item.component';
import {ContextMenuComponent} from './context-menu/context-menu.component';

import {AppRoutingModule} from '@/app-routing.module';
import {AppComponent} from './app.component';
import {MainComponent} from '@modules/main/main.component';
import {LoginComponent} from '@modules/login/login.component';
import {AutoLoginComponent} from '@modules/auto-login/auto-login.component';
import {HeaderComponent} from '@modules/main/header/header.component';
import {FooterComponent} from '@modules/main/footer/footer.component';
import {MenuSidebarComponent} from '@modules/main/menu-sidebar/menu-sidebar.component';
import {BlankComponent} from '@pages/blank/blank.component';
import {ProfileComponent} from '@pages/profile/profile.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RegisterComponent} from '@modules/register/register.component';
import {DashboardComponent} from '@pages/dashboard/dashboard.component';

import {UserComponent} from '@pages/user/user.component';
import {UserDetailComponent} from '@pages/user/user-detail.component';
import {UserPasswordComponent} from '@pages/user/user-password.component';

import {SchedinoComponent} from '@pages/schedino/schedino.component';
import {SchedinoDetailComponent} from '@pages/schedino/schedino-detail.component';
import {ProdottoComponent} from '@pages/prodotto/prodotto.component';
import {ProdottoDetailComponent} from '@pages/prodotto/prodotto-detail.component';
import {PastoComponent} from '@pages/pasto/pasto.component';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ModalModule} from 'ngx-bootstrap/modal';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {TimepickerModule} from 'ngx-bootstrap/timepicker';
import {NgxSpinnerModule} from 'ngx-spinner';
//import {TabsModule} from 'ngx-tabset';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {NgSelectModule} from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask';

import {registerLocaleData} from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeIt from '@angular/common/locales/it';
import {UserDropdownMenuComponent} from '@modules/main/header/user-dropdown-menu/user-dropdown-menu.component';
import {ForgotPasswordComponent} from '@modules/forgot-password/forgot-password.component';
import {RecoverPasswordComponent} from '@modules/recover-password/recover-password.component';
import { ArchwizardModule } from 'angular-archwizard';

// Service
import {AppConfig} from './app-config';
import {UserInfoService} from './services/user-info.service';
import {AuthGuard} from './services/auth_guard.service';
import {AuthApiRequestService} from './services/api/auth-api-request.service';
import {ResApiRequestService} from './services/api/res-api-request.service';
import {PubSubService} from './services/pub-sub.service';
import {NgxAlertService} from './services/ngxalert.service';
import {LoginService} from './services/api/login.service';
import {UserService} from './services/api/user.service';
import {RoleService} from './services/api/role.service';
import {SchedinoService} from './services/api/schedino.service';
import {ProdottoService} from './services/api/prodotto.service';
import {PastoService} from './services/api/pasto.service';
import {StatisticService} from './services/api/statistic.service';

import {httpInterceptorProviders} from './services/http-interceptors';

export function initializeApp(appConfig: AppConfig) {
    return () => appConfig.load();
}

//registerLocaleData(localeEn, 'en-EN');
registerLocaleData(localeIt, 'it-IT');

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        LoginComponent,
        AutoLoginComponent,
        ContextMenuComponent,
        HeaderComponent,
        FooterComponent,
        MenuSidebarComponent,
        BlankComponent,
        SchedinoComponent,
        SchedinoDetailComponent,
        ProdottoComponent,
        PastoComponent,
        ProdottoDetailComponent,
        ProfileComponent,
        RegisterComponent,
        DashboardComponent,
        UserComponent,
        UserDetailComponent,
        UserPasswordComponent,
        AppButtonComponent,
        UserDropdownMenuComponent,
        ForgotPasswordComponent,
        RecoverPasswordComponent,
        SpinnerComponent,
        ConfirmationModalComponent,
        MenuItemComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule,
        NgxDatatableModule,
        NgxChartsModule,
        ArchwizardModule,
        AutocompleteLibModule,
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true
        }),
        NgxMaskModule.forRoot(),
        NgSelectModule
    ],
    providers: [
        httpInterceptorProviders,
        AuthGuard,
        UserInfoService,
        AuthApiRequestService,
        ResApiRequestService,
        PubSubService,
        NgxAlertService,
        LoginService,
        RoleService,
        UserService,
        SchedinoService,
        ProdottoService,
        PastoService,
        StatisticService,

        AppConfig,
        {
            provide: APP_INITIALIZER,
            useFactory: initializeApp,
            deps: [AppConfig],
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
