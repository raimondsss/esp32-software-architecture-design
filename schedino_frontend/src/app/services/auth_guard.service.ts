import {Injectable} from '@angular/core';
import {
    Router,
    CanActivate,
    CanActivateChild,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import {UserInfoService} from './user-info.service';
import {LoginService} from './api//login.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(
        private router: Router,
        private loginService: LoginService,
        private userInfoService: UserInfoService
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {
        const url: string = state.url;
        return this.checkLogin(url);
        //return true;
    }

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {
        return this.canActivate(route, state);
    }

    checkAccessToken(): boolean {
        let result = false;
        const accessToken = this.userInfoService.getOauthInfo().accessToken;
        this.loginService.checkToken(accessToken).subscribe(
            (resp) => {
                if (resp.length == 0 || resp.success == false) {
                    result = false;
                } else {
                    result = true;
                }
            },
            (errResponse) => {
                result = false;
            }
        );

        return result;
    }

    checkLogin(url: string): boolean {
        if (this.userInfoService.isLoggedIn()) {
            // check the token
            const expiresIn = this.userInfoService.getOauthInfo().expiresIn;
            const lastAccess = this.userInfoService.getOauthInfo().lastAccess;
            if (new Date().getTime() - lastAccess < expiresIn * 1000)
                return true;
            //if (this.checkAccessToken())
            //    return true;
        }

        console.log(
            'User is not logged - This routing guard prevents redirection to any routes that needs logging.'
        );
        //Store the original url in login service and then redirect to login page
        //this.loginService.landingPage = url;
        this.router.navigate(['login']);
        return false;
    }
}
