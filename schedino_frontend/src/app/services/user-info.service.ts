import {Injectable} from '@angular/core';

export interface UserInStorage {
    userId: string;
    userName: string;
    email: string;
    displayName: string;
    role: string;
    roleDescription: string;
    organizationId: number;
    organizationName: string;
    companyId: number;
    companyCategoryId: number;
}

export interface OAuthStorage {
    accessToken: string;
    refreshToken: string;
    tokenType: string;
    expiresIn: number;
    scope: string;
    lastAccess: number;
}

export interface LoginInfoInStorage {
    success: boolean;
    error: number;
    message: string;
    landingPage: string;
    user?: UserInStorage;
    oauth?: OAuthStorage;
}

@Injectable()
export class UserInfoService {
    public currentUserKey: string = 'currentUser';
    public currentOauthKey: string = 'currentOauth';
    public storage: Storage = sessionStorage; // <--- you may switch between sessionStorage or LocalStrage (only one place to change)

    constructor() {}

    //Store userinfo from session storage
    storeUserInfo(userInfoString: string) {
        this.storage.setItem(this.currentUserKey, userInfoString);
    }

    //Remove userinfo from session storage
    removeUserInfo() {
        this.storage.removeItem(this.currentUserKey);
    }

    //Store oauthinfo from session storage
    storeOauthInfo(oauthInfoString: string) {
        this.storage.setItem(this.currentOauthKey, oauthInfoString);
    }

    //Remove userinfo from session storage
    removeOauthInfo() {
        this.storage.removeItem(this.currentOauthKey);
    }

    //Get userinfo from session storage
    getUserInfo(): UserInStorage | null {
        try {
            let userInfoString: string = this.storage.getItem(
                this.currentUserKey
            );
            if (userInfoString) {
                let userObj: UserInStorage = JSON.parse(
                    this.storage.getItem(this.currentUserKey)
                );
                return userObj;
            } else {
                return null;
            }
        } catch (e) {
            return null;
        }
    }

    //Get oauthinfo from session storage
    getOauthInfo(): OAuthStorage | null {
        try {
            let oauthInfoString: string = this.storage.getItem(
                this.currentOauthKey
            );
            if (oauthInfoString) {
                let oauthObj: OAuthStorage = JSON.parse(
                    this.storage.getItem(this.currentOauthKey)
                );
                return oauthObj;
            } else {
                return null;
            }
        } catch (e) {
            return null;
        }
    }

    isLoggedIn(): boolean {
        return this.storage.getItem(this.currentUserKey) ? true : false;
    }

    //Get User's id from session storage
    getUserId(): string {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.userId;
        }
        return 'no-user';
    }

    //Get User's Display name from session storage
    getUserName(): string {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.userName;
        }
        return 'no-user';
    }

    getDisplayName(): string {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.displayName;
        }
        return 'no-user';
    }

    getUserRole(): string {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.role;
        }
        return 'no-role';
    }

    getOrganizationId(): number {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.organizationId;
        }
        return 0;
    }

    setOrganizationId(id: number) {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            userObj.organizationId = id;
            this.storeUserInfo(JSON.stringify(userObj));
        }
    }
    getOrganization(): string {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.organizationName;
        }
        return '';
    }

    getCompanyId(): number {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.companyId;
        }
        return 0;
    }

    getCompanyCategoryId(): number {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null) {
            return userObj.companyCategoryId;
        }
        return 0;
    }

    getStoredToken(): string | null {
        let oauthObj: OAuthStorage = this.getOauthInfo();
        if (oauthObj !== null) {
            return oauthObj.accessToken;
        }
        return null;
    }

    isSuperuser(): boolean {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null && userObj.role === 'ROLE_ADMIN') {
            return true;
        }

        return false;
    }

    isCompanyAdmin(): boolean {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null && userObj.role === 'ROLE_MANAGER') {
            return true;
        }

        return false;
    }

    isUser(): boolean {
        let userObj: UserInStorage = this.getUserInfo();
        if (userObj !== null && userObj.role === 'ROLE_USER') {
            return true;
        }

        return false;
    }

}
