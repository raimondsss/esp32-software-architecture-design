import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {JsonDateInterceptor} from './JsonDateInterceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
    {provide: HTTP_INTERCEPTORS, useClass: JsonDateInterceptor, multi: true}
];
