import {Injectable, Inject} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {ResApiRequestService} from './res-api-request.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class SchedinoService {
    constructor(private apiRequest: ResApiRequestService) {}

    getList(
        compagniaId?: number,
        nome?: string,
        codice?: string,
        page?: number,
        size?: number
    ): Observable<any> {
        let me = this;
        let params: HttpParams = new HttpParams();
        params = params.append('compagniaId', compagniaId);
        if (typeof nome === 'string' && nome !== '') {
            params = params.append('nome', '%' + nome + '%');
        }
        if (typeof codice === 'string' && codice !== '') {
            params = params.append('codice', '%' + codice + '%');
        }
        params = params.append(
            'page',
            typeof page === 'number' ? page.toString() : '0'
        );
        params = params.append(
            'size',
            typeof size === 'number' ? size.toString() : '1000'
        );
        return this.apiRequest.get('/schedini', params);
    }

    getEntity(id: string): Observable<any> {
        let me = this;
        //let params: HttpParams = new HttpParams();
        //params = params.append('id', id);
        return this.apiRequest.get('/schedino/' + id);
    }

    getEntityByScreening(id: number, tipo: string): Observable<any> {
        let me = this;
        //let params: HttpParams = new HttpParams();
        //params = params.append('id', id);
        return this.apiRequest.get('/schedino-by-screening/' + id +'/'+ tipo);
    }

    addEntity(entity: any): Observable<any> {
        let me = this;
        return this.apiRequest.post('/schedino', entity);
    }


    updateEntity(entity: any): Observable<any> {
        let me = this;
        return this.apiRequest.put('/schedino', entity);
    }

    updateEntityWithPdf(entity: any): Observable<any> {
        let me = this;
        return this.apiRequest.putWithPdf('/schedino_and_print', entity);
    }

    saveFile(response, filename, mediaType) {
        this.apiRequest.saveFile(response, filename, mediaType);
    }

    deleteEntity(id: string): Observable<any> {
        let me = this;
        return this.apiRequest.delete('/schedino/' + id);
    }

    uploadSchedinoDefinitivo(formData: any): Observable<any> {
        return this.apiRequest.multipartPost('/schedino/schedino_definitivo', formData);
    }
    uploadSchedinoPreliminareFirmato(formData: any): Observable<any> {
        return this.apiRequest.multipartPost('/schedino/schedino_preliminare_firmato', formData);
    }

    uploadExcel(formData: any): Observable<any> {
        return this.apiRequest.multipartPost(
            'api/organization/upload',
            formData
        );
    }
}
