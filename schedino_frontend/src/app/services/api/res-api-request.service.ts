import {Injectable, Inject} from '@angular/core';
import {
    HttpClient,
    HttpHeaders,
    HttpResponse,
    HttpRequest,
    HttpParams,
    HttpErrorResponse
} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {saveAs} from 'file-saver';
import {UserInfoService, LoginInfoInStorage} from '../user-info.service';
import {AppConfig} from '../../app-config';

@Injectable()
export class ResApiRequestService {
    constructor(
        private http: HttpClient,
        private router: Router,
        private userInfoService: UserInfoService
    ) {}

    /**
     * This is a Global place to add all the request headers for every REST calls
     */
    getHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
        const token = this.userInfoService.getStoredToken();
        const organizationId = this.userInfoService.getOrganizationId();
        headers = headers.append('Content-Type', 'application/json');
        if (token !== null) {
            headers = headers.append('Authorization', 'Bearer ' + token);
        }
        if (organizationId != null)
            headers = headers.append('X-TenantID', organizationId.toString());
        return headers;
    }

    /**
     * This is a Global place to add all the request headers for every REST calls
     */
    getHeadersNoTenant(): HttpHeaders {
        let headers = new HttpHeaders();
        const token = this.userInfoService.getStoredToken();
        headers = headers.append('Content-Type', 'application/json');
        if (token !== null) {
            headers = headers.append('Authorization', 'Bearer ' + token);
        }
        return headers;
    }

    getHeadersMultipart(): HttpHeaders {
        let headers = new HttpHeaders();
        const token = this.userInfoService.getStoredToken();
        if (token !== null) {
            headers = headers.append('Authorization', 'Bearer ' + token);
        }
        return headers;
    }

    getNoTenant(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeadersNoTenant(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    get(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    get_sync(url: string, urlParams?: HttpParams): Promise<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeaders(),
                params: urlParams
            }).toPromise();
    }

    getBinary(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseResPath + url, {
                responseType: 'arraybuffer',
                headers: this.getHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    saveFile(response, filename, mediaType) {
        var blob = new Blob([response], { type: mediaType });
        saveAs(blob, filename);
    }

    getPDF(url: string, filename:string, urlParams?: HttpParams): any {
        var mediaType = 'application/pdf';
        this.http.get(AppConfig.settings.apiServer.baseResPath + url, { 
            responseType: 'blob',
            headers: this.getHeaders(),
            params: urlParams
        }).subscribe(
            (response) => {
                var blob = new Blob([response], { type: mediaType });
                saveAs(blob, filename);
            },
            e => { throwError(e); }
        );
    }

    async getPDFSync(url: string, filename:string, urlParams?: HttpParams): Promise<any> {
        var mediaType = 'application/pdf';        
        const response = await this.http.get(AppConfig.settings.apiServer.baseResPath + url, { 
            responseType: 'blob',
            headers: this.getHeaders(),
            params: urlParams
        }).toPromise();
        
        var blob = new Blob([response], { type: mediaType });
        saveAs(blob, filename);
    }
    
    post(url: string, body: Object): Observable<any> {
        let me = this;
        return this.http
            .post(
                AppConfig.settings.apiServer.baseResPath + url,
                JSON.stringify(body),
                {headers: this.getHeaders()}
            )
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    put(url: string, body: Object): Observable<any> {
        let me = this;
        return this.http
            .put(
                AppConfig.settings.apiServer.baseResPath + url,
                JSON.stringify(body),
                {headers: this.getHeaders()}
            )
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    putWithPdf(url: string, body: Object): Observable<any> {
        let me = this;
        return this.http
            .put(
                AppConfig.settings.apiServer.baseResPath + url,
                JSON.stringify(body),
                {responseType: 'blob', headers: this.getHeaders()}
            )
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    delete(url: string): Observable<any> {
        let me = this;
        return this.http
            .delete(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeaders()
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    multipartPost(url: string, formdata: Object): Observable<any> {
        let me = this;
        return this.http
            .post(AppConfig.settings.apiServer.baseResPath + url, formdata, {
                headers: this.getHeadersMultipart()
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

}
