import {Injectable, Inject} from '@angular/core';
import {
    HttpClient,
    HttpHeaders,
    HttpResponse,
    HttpRequest,
    HttpParams,
    HttpErrorResponse
} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {UserInfoService, LoginInfoInStorage} from '../user-info.service';
import {AppConfig} from '../../app-config';

@Injectable()
export class AuthApiRequestService {
    constructor(
        private http: HttpClient,
        private router: Router,
        private userInfoService: UserInfoService
    ) {}

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong.
            console.error(
                `Backend returned code ${error.status}, ` +
                    `body was: ${error.error}`
            );
        }
        // Return an observable with a user-facing error message.
        return throwError('Something bad happened; please try again later.');
    }

    /**
     * This is a Global place to add all the request headers for every REST calls
     */
    getOauthTokenHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
        const clientId = AppConfig.settings.apiServer.clientId;
        const clientSecret = AppConfig.settings.apiServer.clientSecret;
        const authHeader = 'Basic ' + btoa(clientId + ':' + clientSecret);
        console.log(authHeader);
        //        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', authHeader);
        return headers;
    }

    getHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
        let token = this.userInfoService.getStoredToken();
        headers = headers.append('Content-Type', 'application/json');
        if (token !== null) {
            headers = headers.append('Authorization', 'Bearer ' + token);
        }
        return headers;
    }

    getHeadersMultipart(): HttpHeaders {
        let headers = new HttpHeaders();
        let token = this.userInfoService.getStoredToken();
        if (token !== null) {
            headers = headers.append('Authorization', token);
        }
        return headers;
    }

    oauthToken(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .post(AppConfig.settings.apiServer.baseApiPath + url, null, {
                headers: this.getOauthTokenHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    checkToken(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseApiPath + url, {
                headers: this.getOauthTokenHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    getProfile(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    get(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .get(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    post(url: string, body: Object): Observable<any> {
        let me = this;
        return this.http
            .post(
                AppConfig.settings.apiServer.baseResPath + url,
                JSON.stringify(body),
                {headers: this.getHeaders()}
            )
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    postAuth(url: string, body: Object): Observable<any> {
        let me = this;
        return this.http
            .post(
                AppConfig.settings.apiServer.baseApiPath + url,
                JSON.stringify(body),
                {headers: this.getHeaders()}
            )
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }


    postWithParams(url: string, urlParams?: HttpParams): Observable<any> {
        let me = this;
        return this.http
            .post(AppConfig.settings.apiServer.baseResPath + url, null, {
                headers: this.getHeaders(),
                params: urlParams
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    put(url: string, body: Object): Observable<any> {
        let me = this;
        return this.http
            .put(
                AppConfig.settings.apiServer.baseResPath + url,
                JSON.stringify(body),
                {headers: this.getHeaders()}
            )
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    delete(url: string): Observable<any> {
        let me = this;
        return this.http
            .delete(AppConfig.settings.apiServer.baseResPath + url, {
                headers: this.getHeaders()
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }

    multipartPost(url: string, formdata: Object): Observable<any> {
        let me = this;
        return this.http
            .post(AppConfig.settings.apiServer.baseResPath + url, formdata, {
                headers: this.getHeadersMultipart()
            })
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    console.log('Some error in catch');
                    if (error.status === 401 || error.status === 403) {
                        me.router.navigate(['/logout']);
                    }
                    return throwError(error || 'Server error');
                    //                return Observable.throw(error || 'Server error')
                })
            );
    }
}
