import {Injectable, Inject} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {ResApiRequestService} from './res-api-request.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class PastoService {
    constructor(private apiRequest: ResApiRequestService) {}

    getList(
        compagniaId?: number,
        nome?: string,
        codice?: string,
        data?: string,
        id?: number,
        page?: number,
        size?: number
    ): Observable<any> {
        let me = this;
        let params: HttpParams = new HttpParams();
        params = params.append('compagniaId', compagniaId);
        if (typeof nome === 'string' && nome !== '') {
            params = params.append('nome', '%' + nome + '%');
        }
        if (typeof codice === 'string' && codice !== '') {
            params = params.append('codice', '%' + codice + '%');
        }
        if (typeof data === 'string' && data !== '') {
            params = params.append('data', data);
        }
        if (typeof id === 'number' && id !== null) {
            params = params.append('id', id);
        }
        params = params.append(
            'page',
            typeof page === 'number' ? page.toString() : '0'
        );
        params = params.append(
            'size',
            typeof size === 'number' ? size.toString() : '1000'
        );
        return this.apiRequest.get('/pasti', params);
    }

    getEntity(id: string): Observable<any> {
        let me = this;
        //let params: HttpParams = new HttpParams();
        //params = params.append('id', id);
        return this.apiRequest.get('/pasto/' + id);
    }

    addEntity(entity: any): Observable<any> {
        let me = this;
        return this.apiRequest.post('/pasto', entity);
    }


    updateEntity(entity: any): Observable<any> {
        let me = this;
        return this.apiRequest.put('/pasto', entity);
    }

    updateEntityWithPdf(entity: any): Observable<any> {
        let me = this;
        return this.apiRequest.putWithPdf('/pasto_and_print', entity);
    }

    saveFile(response, filename, mediaType) {
        this.apiRequest.saveFile(response, filename, mediaType);
    }

    deleteEntity(id: string): Observable<any> {
        let me = this;
        return this.apiRequest.delete('/pasto/' + id);
    }

    uploadExcel(formData: any): Observable<any> {
        return this.apiRequest.multipartPost(
            'api/organization/upload',
            formData
        );
    }
}
