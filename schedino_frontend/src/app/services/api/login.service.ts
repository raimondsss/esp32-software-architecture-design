import {Injectable, Inject} from '@angular/core';
import {Router} from '@angular/router';

import {HttpParams} from '@angular/common/http';
import {Observable, Subject, BehaviorSubject} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ToastrService} from 'ngx-toastr';
import {
    UserInfoService,
    LoginInfoInStorage,
    UserInStorage,
    OAuthStorage
} from '../user-info.service';
import {AuthApiRequestService} from './auth-api-request.service';
import {ResApiRequestService} from './res-api-request.service';

export interface LoginRequestParam {
    grant_type: string;
    username: string;
    password: string;
}

export interface checkTokenParam {
    token: string;
}

@Injectable()
export class LoginService {
    public landingPage: string = '/';
    public landingPageAdmin: string = '/';

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private userInfoService: UserInfoService,
        private authApiRequest: AuthApiRequestService,
        private resApiRequest: ResApiRequestService
    ) {}

    loginByAuth({email, password}) {
        let params = new HttpParams()
            .set('grant_type', 'password')
            .set('username', email)
            .set('password', password);

        this.authApiRequest.oauthToken('oauth/token', params).subscribe(
            (resp) => {
                console.log(resp);
                this.userInfoService.storeUserInfo(
                    JSON.stringify({
                        userId: email,
                        userName: '',
                        email: '',
                        displayName: '',
                        role: '',
                        roleDescription: '',
                        organizationId: 0,
                        organizationName: '',
                        companyId: 0,
                        companyCategoryId: 0,
                        picture: ''
                    })
                );
                this.userInfoService.storeOauthInfo(
                    JSON.stringify({
                        accessToken: resp.access_token,
                        refreshToken: resp.refresh_token,
                        tokenType: resp.token_type,
                        expiresIn: resp.expires_in,
                        scope: resp.scope,
                        lastAccess: new Date().getTime()
                    })
                );

                localStorage.setItem('token', resp.access_token);
                this.getUserProfile();
            },
            (err) => {
                let errmsg = '';
                switch (err.status) {
                    case 400:
                    case 401:
                        errmsg = 'Username or password incorrect';
                        break;
                    case 404:
                        errmsg = 'Service not found';
                        break;
                    case 408:
                        errmsg = 'Request Timedout';
                        break;
                    case 500:
                        errmsg = 'Internal Server Error';
                        break;
                    default:
                        errmsg = 'Server Error';
                        break;
                }

                this.toastr.error(errmsg);
                this.logout();
            }
        );

        //this.router.navigate(['/']);
    }

    getUserProfile() {
        let me = this;

        const accessToken = this.userInfoService.getOauthInfo().accessToken;
        const params = new HttpParams().set('token', accessToken);

        this.authApiRequest.getProfile('/users/profile', params).subscribe(
            (resp) => {
                console.log(resp);
                const userObj: UserInStorage = this.userInfoService.getUserInfo();

                userObj.userId = resp.result.id;
                userObj.userName = resp.result.username;
                userObj.email = resp.result.email;
                userObj.displayName =
                    resp.result.firstName + ' ' + resp.result.lastName;
                userObj.organizationId = resp.result.organizationId;
                userObj.role = resp.result.role[0];
                userObj.roleDescription = resp.result.roleDescription;

                // store username and jwt token in session storage to keep user logged in between page refreshes
                this.userInfoService.storeUserInfo(JSON.stringify(userObj));

                //const landingPage = this.userInfoService.isSuperuser()
                //    ? me.landingPageAdmin
                //    : me.landingPage;

                //this.router.navigate([landingPage]);
                this.getCompany();
            },
            (err) => {
                let errmsg = '';
                switch (err.status) {
                    case 400:
                    case 401:
                        errmsg = 'Username or password incorrect';
                        break;
                    case 404:
                        errmsg = 'Service not found';
                        break;
                    case 408:
                        errmsg = 'Request Timedout';
                        break;
                    case 500:
                        errmsg = 'Internal Server Error';
                        break;
                    default:
                        errmsg = 'Server Error';
                        break;
                }

                this.toastr.error(errmsg);
            }
        );
    }

    getCompany() {
        let me = this;

        let compagniaId = this.userInfoService.getOrganizationId().toString();
        this.resApiRequest.get('/compagnia/' + compagniaId).subscribe(
            (resp) => {
                console.log(resp);
                if (resp.operationStatus !== 'SUCCESS') {
                    let errmsg = 'Compagnia non trovata';
                    this.toastr.error(errmsg);
                    return;
                }

                let userObj: UserInStorage = this.userInfoService.getUserInfo();
                userObj.companyId = resp.data.id;
                userObj.organizationName = resp.data.ragioneSociale;
                this.userInfoService.storeUserInfo(JSON.stringify(userObj));

                const landingPage = this.userInfoService.isSuperuser()
                    ? me.landingPageAdmin
                    : me.landingPage;

                this.router.navigate([landingPage]);
            },
            (err) => {
                let errmsg = 'Errore fatale nel caricamento della compagnia';
                this.toastr.error(errmsg);
            }
        );
    }

    checkToken(accessToken: string): Observable<any> {
        let me = this;

        let params = new HttpParams().set('token', accessToken);

        /*
            Using BehaviorSubject instead of Subject
            In Angular services are initialized before the components, if any component is
            subscribing, it will only receive events which are executed after subscription.
            therefore if you put a syncronize next() in the service, the component wont get it.

            A BehaviourSubject will always provide the values wheather the subscription happened after or before event

        */

        let loginDataSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
            []
        ); // Will use this BehaviorSubject to emit data that we want after ajax login attempt
        let loginInfoReturn: LoginInfoInStorage; // Object that we want to send back to Login Page

        this.authApiRequest.checkToken('oauth/check_token', params).subscribe(
            (jsonResp) => {
                console.log(jsonResp);
                if (jsonResp !== undefined && jsonResp !== null) {
                    // get username and jwt token from session storage
                    let userObj: UserInStorage = this.userInfoService.getUserInfo();
                    let oauthObj: OAuthStorage = this.userInfoService.getOauthInfo();

                    userObj.userId = jsonResp.user_name;
                    userObj.role = jsonResp.authorities[0];
                    oauthObj.expiresIn = jsonResp.exp;

                    // store username and jwt token in session storage to keep user logged in between page refreshes
                    this.userInfoService.storeUserInfo(JSON.stringify(userObj));
                    this.userInfoService.storeOauthInfo(
                        JSON.stringify(oauthObj)
                    );

                    loginInfoReturn = {
                        success: true,
                        message: '',
                        error: 200,
                        landingPage: '',
                        user: userObj,
                        oauth: oauthObj
                    };
                } else {
                    //Create a faliure object that we want to send back to login page
                    loginInfoReturn = {
                        success: false,
                        error: 200,
                        message: jsonResp.msgDesc,
                        landingPage: '/login'
                    };
                }

                loginDataSubject.next(loginInfoReturn);
            },
            (err) => {
                loginInfoReturn = {
                    success: false,
                    error: err.status,
                    message:
                        err.url +
                        ' >>> ' +
                        err.statusText +
                        '[' +
                        err.status +
                        ']',
                    landingPage: '/login'
                };

                loginDataSubject.next(loginInfoReturn);
            }
        );

        return loginDataSubject;
    }

    logout(navigatetoLogout = true): void {
        // clear token remove user from local storage to log user out
        localStorage.removeItem('token');
        this.userInfoService.removeUserInfo();
        this.userInfoService.removeOauthInfo();
        if (navigatetoLogout) {
            this.router.navigate(['logout']);
        }
    }
}
