import {Injectable, Inject} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {AuthApiRequestService} from './auth-api-request.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class UserService {
    constructor(private apiRequest: AuthApiRequestService) {}

    getUsers(
        organizationId: string,
        username: string,
        role: string,
        page?: number,
        size?: number
    ): Observable<any> {
        //Create Request URL params
        let me = this;
        let params: HttpParams = new HttpParams();
        if (typeof organizationId === 'string')
            params = params.append('organizationId', organizationId);
        if (typeof username === 'string')
            params = params.append('username', '%' + username + '%');
        if (typeof role === 'string' && role != '')
            params = params.append('role', role);
        params = params.append(
            'page',
            typeof page === 'number' ? page.toString() : '0'
        );
        params = params.append(
            'size',
            typeof size === 'number' ? size.toString() : '1000'
        );
        return this.apiRequest.get('/users/list', params);
    }

    getUser(userId: string): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.get('/users/get/' + userId);
    }

    getUserByUsername(username: string): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.get('/users/getbyusername/' + username);
    }

    registerUser(user: any): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.postAuth('api/registration/registerUser', user)
    }

    addUser(user: any): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.post('/users/create', user);
    }

    updateUser(user: any): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.put('/users/update', user);
    }

    deleteUser(userId: string): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.delete('/users/delete/' + userId);
    }

    updatePassword(password: any): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.post('/users/changePassword', password);
    }

    resetPassword(email: any): Observable<any> {
        //Create Request URL params
        let me = this;
        let params: HttpParams = new HttpParams();
        params = params.append('email', email);
        return this.apiRequest.postWithParams('/users/resetPassword', params);
    }
    
    resetPasswordByEmail(email: any): Observable<any> {
        //Create Request URL params
        let me = this;
        let params: HttpParams = new HttpParams();
        params = params.append('email', email);
        return this.apiRequest.postWithParams('/users/resetPasswordByEmail', params);
    }
}
