import {Injectable, Inject} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {ResApiRequestService} from './res-api-request.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class StatisticService {
    constructor(private apiRequest: ResApiRequestService) {}

    getGeneralStats(startDate?:string, endDate?:string, userId?:number): Observable<any> {
        let me = this;
        let params: HttpParams = new HttpParams();
        if (typeof startDate === 'string' && startDate !== '') {
            params = params.append('startDate', startDate);
        }
        if (typeof endDate === 'string' && endDate !== '') {
            params = params.append('endDate', endDate);
        } 
        if ((typeof userId === "number" || typeof userId === "string") && userId !== null){
            params = params.append('credentialsId', userId);
        }   

        return this.apiRequest.get('/dashboard-stats', params);
    }

    getWeekStats(userId?:number, startDate?:string, endDate?:string): Observable<any> {
        let me = this;
        let params: HttpParams = new HttpParams();
        if ((typeof userId === "number" || typeof userId === "string") && userId !== null){
            params = params.append('userId', userId);
        }    
        if (typeof startDate === 'string' && startDate !== '') {
            params = params.append('startDate', startDate);
        }
        if (typeof endDate === 'string' && endDate !== '') {
            params = params.append('endDate', endDate);
        }

        return this.apiRequest.get('/week-stats', params);
    }

    getSchediniAssociati(): Observable<any> {
        let me = this;
        let params: HttpParams = new HttpParams();

        return this.apiRequest.get('/schedini-associati', params);
    }

}
