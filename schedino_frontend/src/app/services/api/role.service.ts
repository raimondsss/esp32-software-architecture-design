import {Injectable, Inject} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {AuthApiRequestService} from './auth-api-request.service';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class RoleService {
    constructor(private apiRequest: AuthApiRequestService) {}

    getRoles(
        organizationId?: number,
        roleId?: number,
        page?: number,
        size?: number
    ): Observable<any> {
        //Create Request URL params
        let me = this;
        let params: HttpParams = new HttpParams();
        if (typeof organizationId === 'number')
            params = params.append('organizationId', organizationId.toString());
        if (typeof roleId === 'number')
            params = params.append('roleId', roleId.toString());
        params = params.append(
            'page',
            typeof page === 'number' ? page.toString() : '0'
        );
        params = params.append(
            'size',
            typeof size === 'number' ? size.toString() : '1000'
        );
        return this.apiRequest.get('/authorities/list', params);
    }

    getRole(roleId: string): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.get('/authorities/get/' + roleId);
    }

    addRole(role: any): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.post('/authorities/create', role);
    }

    updateRole(role: any): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.put('/authorities/role', role);
    }

    deleteRole(roleId: string): Observable<any> {
        //Create Request URL params
        let me = this;
        return this.apiRequest.delete('/authorities/delete/' + roleId);
    }
}
