import {Injectable} from '@angular/core';
import 'rxjs/add/operator/filter';
import {ToastrService} from 'ngx-toastr';
import {AppConfig} from '../app-config';

@Injectable()
export class NgxAlertService {
    constructor(private toastr: ToastrService) {}

    success(title: string, message: string) {
        //this.toastr.success(title, message, AppConfig.settings.toastConfig);
        this.toastr.success(title, message);
    }

    error(title: string, message: string) {
        //this.toastr.error(title, message, AppConfig.settings.toastConfig);
        this.toastr.error(title, message);
    }

    info(title: string, message: string) {
        //this.toastr.info(title, message, AppConfig.settings.toastConfig);
        this.toastr.info(title, message);
    }

    warn(title: string, message: string) {
        //this.toastr.info(title, message, AppConfig.settings.toastConfig);
        this.toastr.info(title, message);
    }
}
