import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from '@modules/main/main.component';
import {LoginComponent} from '@modules/login/login.component';
import {AutoLoginComponent} from '@modules/auto-login/auto-login.component';
import {ProfileComponent} from '@pages/profile/profile.component';
import {RegisterComponent} from '@modules/register/register.component';
import {DashboardComponent} from '@pages/dashboard/dashboard.component';
import {AuthGuard} from '@guards/auth.guard';
import {ForgotPasswordComponent} from '@modules/forgot-password/forgot-password.component';
import {RecoverPasswordComponent} from '@modules/recover-password/recover-password.component';
import {UserComponent} from '@pages/user/user.component';
import {UserDetailComponent} from '@pages/user/user-detail.component';
import {UserPasswordComponent} from '@pages/user/user-password.component';
import {SchedinoComponent} from '@pages/schedino/schedino.component';
import {SchedinoDetailComponent} from '@pages/schedino/schedino-detail.component';
import {ProdottoComponent} from '@pages/prodotto/prodotto.component';
import {ProdottoDetailComponent} from '@pages/prodotto/prodotto-detail.component';
import {PastoComponent} from '@pages/pasto/pasto.component';

const routes: Routes = [
    {
        path: '',
        
        component: MainComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            {path: 'user', component: UserComponent},
            {path: 'adduser/:compagniaId', component: UserDetailComponent},
            {path: 'edituser/:id', component: UserDetailComponent},
            {path: 'pwduser/:id', component: UserPasswordComponent},

            {path: 'profile', component: ProfileComponent},

            {path: 'schedini', component: SchedinoComponent},
            {path: 'addschedino', component: SchedinoDetailComponent},
            {path: 'editschedino/:id', component: SchedinoDetailComponent},

            {path: 'prodotti', component: ProdottoComponent},
            {path: 'addprodotto', component: ProdottoDetailComponent},
            {path: 'editprodotto/:id', component: ProdottoDetailComponent},

            {path: 'pasti', component: PastoComponent},

            {
                path: '',
                component: DashboardComponent
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'auto-login',
        component: AutoLoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent
    },
    {
        path: 'recover-password',
        component: RecoverPasswordComponent
    },
    {path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
