import {Component, OnInit, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from '@angular/forms';
import {PubSubService} from '@services/pub-sub.service';
import {UserInfoService} from '../../services/user-info.service';
import {SchedinoService} from '@services/api/schedino.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {AppConfig} from '../../app-config';

@Component({
    selector: 'app-schedino-detail',
    templateUrl: './schedino-detail.component.html',
    styleUrls: ['./schedino-detail.component.scss']
})
export class SchedinoDetailComponent implements OnInit {
    superuser: boolean;
    utente: boolean;
    scoutuser: boolean;
    isLoading = false;
    newSchedino: boolean;
    entityId: any;
    schedino: any;
    title: string;
    schedinoForm: FormGroup;
    submitted = true;
    schedinoId: any;
    navi: any[];
    compagniaId: any;
    compagnie: any[];
    paeseId: any;
    paesi: any[];
    filteredPaesi: any[];
    tipologieSchedino: any[];
    dynamicPositionType: any[];
    entityForm: FormGroup;
    fileUpload = {
        filePath: '',
        file: null
    };
    logo = null;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userInfoService: UserInfoService,
        private theService: SchedinoService,
        private ngxAlertService: NgxAlertService,
        private formBuilder: FormBuilder,
        private pubSubService: PubSubService
    ) {}

    ngOnInit() {
        var me = this;

        this.superuser = this.userInfoService.isSuperuser();
        this.utente = this.userInfoService.isUser();

        this.schedinoForm = this.formBuilder.group({
            id: [''],
            compagniaId: [''],
            nome:['', [Validators.required]],
            codice: ['', [Validators.required]],
            descrizione: ['', [Validators.required]],
        });

        this.title = 'Nuovo Bilancino';
        this.newSchedino = true;
        let schedinoId = this.route.snapshot.params['id'];
        if (schedinoId) {
            this.title = 'Aggiona Bilancino';
            this.newSchedino = false;
            this.schedinoId = schedinoId;
            this.theService.getEntity(this.schedinoId).subscribe((data) => {
                this.schedino = data.data;
                this.schedinoForm.patchValue({
                    id: this.schedino.id,
                    nome: this.schedino.nome,
                    codice: this.schedino.codice,
                    descrizione: this.schedino.descrizione
                });
            });
        } else {
            this.schedinoForm.controls['compagniaId'].setValue(this.compagniaId);
        }
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.schedinoForm.controls;
    }

    onChangeCompagnia(compagniaId) {
        this.compagniaId = compagniaId;
    }

    selectEvent(item) {
        // do something with selected item
        console.log(item);
        //this.schedinoForm.controls['bandiera'].setValue(item.nome);
    }

    onChangeSearch(search: string) {
        this.filteredPaesi = [];
        for (var paese of this.paesi) {
            if (paese.nome.toLowerCase().indexOf(search.toLowerCase()) === 0) {
                //this.filteredPaesi.push( {
                //    id: paese.id,
                //    nome: paese.nome,
                //    sigla: paese.siglaIso3
                //});
                this.filteredPaesi.push(paese.nome);
            }
        }
    }

    onFocused(e) {
        // do something
    }

    hasImage(): boolean {
        if (this.logo && this.logo.length) return true;

        return false;
    }

    getImageUrl() {
        if (this.logo === null) return '#';
        return (
            AppConfig.settings.apiServer.apiFilestoragePath +
            'compagnia$' +
            this.compagniaId +
            '$' +
            this.logo +
            '/001'
        );
    }

    uploadFileChange(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length == 0) {
            return;
        }

        let file: File = fileList[0];
        let fileSize: number = fileList[0].size;
        if (fileSize > 10485760) {
            this.ngxAlertService.success('', 'File size is exceeded');
            return;
        }

        this.fileUpload.file = file;
        this.entityForm.markAsDirty();
    }

    saveEntity() {
        this.submitted = true;

        if (this.schedinoForm.invalid) {
            return;
        }

        this.isLoading = true;

        this.schedino = {
            id: this.schedinoForm.value.id,
            compagnia: {
                id: this.schedinoForm.value.compagniaId
            },
            nome: this.schedinoForm.value.nome,
            codice: this.schedinoForm.value.codice,
            descrizione: this.schedinoForm.value.descrizione,
            credentials:{
                id:1
            }
        };

        if (this.newSchedino) {
            this.theService.addEntity(this.schedino).subscribe((data) => {
                if (data.operationStatus === 'SUCCESS') {
                    this.ngxAlertService.success(
                        '',
                        'Aggiunta avvenuta con successo'
                    );

                    // publish event so list controller can refresh
                    //this.pubSubService.publish('schedino-updated');
                    this.router.navigate(['/schedini']);
                } else {
                    console.log('ciao')
                    this.ngxAlertService.error('', data.operationMessage);
                }
                this.isLoading = false;
            });
        } else {
            this.theService.updateEntity(this.schedino).subscribe((data) => {
                if (data.operationStatus === 'SUCCESS') {
                    this.ngxAlertService.success(
                        '',
                        'Aggiornamento avvenuto con successo'
                    );

                    // publish event so list controller can refresh
                    //this.pubSubService.publish('schedino-updated');
                    this.router.navigate(['/schedini']);
                } else {
                    this.ngxAlertService.error('', data.operationMessage);
                }
                this.isLoading = false;
            });
        }
    }
}
