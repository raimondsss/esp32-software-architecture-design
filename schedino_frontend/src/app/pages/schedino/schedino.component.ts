import {Component, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ConfirmationModalComponent} from 'app/components/dialog/confirmation-modal.component';
import {PubSubService} from '@services/pub-sub.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {UserInfoService} from '../../services/user-info.service';
import {SchedinoService} from '@services/api/schedino.service';
import {Page} from 'app/models/paging/page';
import {AppConfig} from '../../app-config';

@Component({
    selector: 'app-schedino',
    templateUrl: './schedino.component.html',
    styleUrls: ['./schedino.component.scss']
})
export class SchedinoComponent {
    superuser: boolean;
    navigationSubscription;
    subscription: Subscription;
    showSlideOut: boolean;
    isLoading = false;
    columns: any[];
    page = new Page();
    rows: any[];
    bckrows: any[];
    nave: any = {};
    isNewUser: boolean;
    filterData = [];
    naveId: any;
    navi: any[];
    compagniaId: any;
    compagnie: any[];
    username: any;
    nome: any;
    codice: any;

    @ViewChild('naveTable') table;

    constructor(
        private router: Router,
        private userInfoService: UserInfoService,
        private modalService: BsModalService,
        private pubSubService: PubSubService,
        private theService: SchedinoService,
        private ngxAlertService: NgxAlertService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 10;
    }

    ngOnInit() {
        var me = this;
        //me.getPageData();

        this.superuser = this.userInfoService.isSuperuser();
        /*if (this.superuser) {
            this.theService.getList().subscribe((pagedData) => {
                this.compagnie = pagedData.items;
            });
        } else {*/
        this.compagniaId = this.userInfoService.getOrganizationId();
        me.setPage({offset: 0});
        //}

        // reload users when updated
        this.subscription = this.pubSubService
            .on('nave-updated')
            .subscribe(() => this.getPageData());

        /*this.columns = [
            {prop: 'userId', name: 'ID', width: 100},
            {prop: 'firstName', name: 'First Name', width: 120},
            {prop: 'lastName', name: 'Last Name', width: 120},
            {prop: 'email', name: 'Email', width: 250},
            {prop: 'active', name: 'Attivo', width: 120}
        ];*/
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();

        // avoid memory leaks here by cleaning up after ourselves. If we
        // don't then we will continue to run our initialiseInvites()
        // method on every navigationEnd event.
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }

    getImageUrl(row) {
        if (row.nome == null) return '#';
        return (
            AppConfig.settings.apiServer.apiFilestoragePath +
            'contratti$' +
            row.id+'$'+
            row.nome +
            '/001'
        );
    }

    getPageData() {
        var me = this;
        this.isLoading = true;
        this.theService.getList(this.compagniaId).subscribe((data) => {
            this.rows = data.items;

            this.bckrows = data.result;
            this.filterData = [...this.rows];

            this.page.pageNumber = 0;
            this.page.size = data.items.length;
            this.page.totalPages = 1;
            this.page.totalElements = data.items.length;

            this.isLoading = false;
        });
    }

    /**
     * Populate the table with new data based on the page number
     * @param page The page to select
     */
    setPage(pageInfo) {
        this.isLoading = true;
        this.page.pageNumber = pageInfo.offset;

        this.theService
            .getList(
                this.compagniaId,
                this.nome,
                this.codice,
                this.page.pageNumber,
                this.page.size
            )
            .subscribe((pagedData) => {
                this.page.pageNumber = pagedData.currentPageNumber;
                this.page.size = pagedData.pageSize;
                this.page.totalPages = pagedData.totalPages;
                this.page.totalElements = pagedData.totalItems;

                this.rows = pagedData.items;
                this.filterData = [...this.rows];
                this.isLoading = false;
            });
    }

    search() {
        if (this.compagniaId === undefined || this.compagniaId == '') return;

        this.setPage({offset: 0});
        //this.getPageData();
    }

    onChangeCompagnia(compagniaId) {
        this.compagniaId = compagniaId;
        this.search();
    }

    // filters results
    filterDatatable(event) {
        // get the value of the key pressed and make it lowercase
        let val = event.target.value.toLowerCase();
        // get the amount of columns in the table
        let colsAmt = this.columns.length;
        // get the key names of each column in the dataset
        let keys = Object.keys(this.bckrows[0]);
        // assign filtered matches to the active datatable
        this.rows = this.filterData.filter(function (item) {
            // iterate through each row's column data
            for (let i = 0; i < colsAmt; i++) {
                // check for a match
                if (
                    item[keys[i]].toString().toLowerCase().indexOf(val) !==
                        -1 ||
                    !val
                ) {
                    // found match, return true to add to result set
                    return true;
                }
            }
        });
        // whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    deleteEntity(nave: any) {
        var me = this;

        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(
            'Confermi la cancellazione del bilancino?',
            ''
        );

        (<ConfirmationModalComponent>modal.content).onClose.subscribe(
            (result) => {
                if (result === true) {
                    this.theService.deleteEntity(nave.id).subscribe((data) => {
                        if (data.operationStatus === 'SUCCESS') {
                            this.ngxAlertService.success(
                                '',
                                'Eliminazione avvenuta con successo'
                            );
                            me.getPageData();
                        } else {
                            console.log('no')
                            this.ngxAlertService.error(
                                '',
                                data.operationMessage
                            );
                        }
                    });
                } else if (result === false) {
                    console.log('no2')
                    // when pressed No
                } else {
                    console.log('no3')
                    // When closing the modal without no or yes
                }
            }
        );
    }
}
