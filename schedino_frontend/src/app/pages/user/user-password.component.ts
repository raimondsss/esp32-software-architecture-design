import {Component, OnInit, Input, TemplateRef, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from '@angular/forms';
import {PubSubService} from '@services/pub-sub.service';
import {UserService} from '@services/api/user.service';
import {RoleService} from '@services/api/role.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {MustMatch} from '../../components/helpers/must-match.validator';

@Component({
    selector: 'app-user-detail-pg',
    templateUrl: './user-password.component.html',
    styleUrls: ['./user-password.component.scss']
})
export class UserPasswordComponent implements OnInit {
    newUser: boolean;
    userId: any;
    user: any;
    title: string;
    userForm: FormGroup;
    isLoading = false;
    submitted = true;
    organizationId: any;
    organizations: any[];
    roleId: any;
    authorities = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private roleService: RoleService,
        private ngxAlertService: NgxAlertService,
        private formBuilder: FormBuilder,
        private pubSubService: PubSubService
    ) {}

    ngOnInit() {
        var me = this;

        this.userForm = this.formBuilder.group(
            {
                username: ['', Validators.required],
                oldPassword: [''],
                newPassword: [
                    '',
                    [Validators.required, Validators.minLength(6)]
                ],
                matchingPassword: ['', [Validators.required]]
            },
            {
                validator: MustMatch('newPassword', 'matchingPassword')
            }
        );

        let userId = this.route.snapshot.params['id'];
        this.title = 'Change Password';
        this.newUser = false;
        this.userId = userId;
        this.userService.getUser(this.userId).subscribe((data) => {
            this.user = data.result;
            this.userForm.patchValue({
                username: this.user.username,
                oldPassword: this.user.password,
                newPassword: '',
                matchingPassword: ''
            });
        });
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.userForm.controls;
    }

    changePassword() {
        this.submitted = true;

        if (this.userForm.invalid) {
            return;
        }

        this.isLoading = true;
        this.user = {
            username: this.userForm.value.username,
            oldPassword: this.userForm.value.oldPassword,
            newPassword: this.userForm.value.newPassword
        };

        this.userService.updatePassword(this.user).subscribe((data) => {
            if (data.message === 'success') {
                this.ngxAlertService.success('', 'Update successful');

                // publish event so list controller can refresh
                this.pubSubService.publish('users-updated');
            } else {
                this.ngxAlertService.error('', data.result);
                this.isLoading = false;
            }
        });

        // redirect to users view
        this.router.navigate(['/home/security/users']);
    }
}
