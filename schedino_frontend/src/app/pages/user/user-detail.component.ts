import {Component, OnInit, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from '@angular/forms';
import {PubSubService} from '@services/pub-sub.service';
import {UserService} from '@services/api/user.service';
import {RoleService} from '@services/api/role.service';
import {UserInfoService} from '../../services/user-info.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {MustMatch} from '../../components/helpers/must-match.validator';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
    superuser: boolean;
    isLoading = false;
    newUser: boolean;
    userId: any;
    user: any;
    title: string;
    userForm: FormGroup;
    submitted = true;
    compagniaId: any;
    compagnie: any[];
    roleId: any;
    authorities = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userInfoService: UserInfoService,
        private userService: UserService,
        private roleService: RoleService,
        private ngxAlertService: NgxAlertService,
        private formBuilder: FormBuilder,
        private pubSubService: PubSubService
    ) {}

    ngOnInit() {
        var me = this;
        this.superuser = this.userInfoService.isSuperuser();

        this.compagniaId = this.route.snapshot.params['compagniaId'];

        if (this.superuser) {
        } else {
            this.compagniaId = this.userInfoService.getOrganizationId();
        }

        this.userForm = this.formBuilder.group(
            {
                organizationId: ['', Validators.required],
                username: ['', Validators.required],
                password: ['', [Validators.required, Validators.minLength(6)]],
                matchingPassword: ['', [Validators.required]],
                firstName: [''],
                lastName: [''],
                email: ['', [Validators.required, Validators.email]],
                active: [true],
                landingPage: [''],
                authorities: new FormArray([])
            },
            {
                validator: MustMatch('password', 'matchingPassword')
            }
        );

        this.title = 'Nuovo Utente';
        this.newUser = true;
        let userId = this.route.snapshot.params['id'];
        if (userId) {
            this.title = 'Aggionamento Utente';
            this.newUser = false;
            this.userId = userId;
            this.userService.getUser(this.userId).subscribe((data) => {
                this.user = data.result;
                this.compagniaId = this.user.organizationId;
                this.userForm.patchValue({
                    id: this.user.id,
                    organizationId: this.user.organizationId,
                    username: this.user.username,
                    password: '0000000000000',
                    matchingPassword: '0000000000000',
                    firstName: this.user.firstName,
                    lastName: this.user.lastName,
                    email: this.user.email,
                    active: this.user.enabled,
                    landingPage: this.user.landingPage,
                    authorities: this.user.role
                });

                this.roleService
                    .getRoles(this.user.organizationId)
                    .subscribe((pagedData) => {
                        this.authorities = pagedData.result;

                        this.authorities.forEach((r) =>
                            this.authoritiesFormArray.push(
                                new FormControl(this.isRolePresent(r))
                            )
                        );
                    });
            });
        } else {
            this.userForm.controls['organizationId'].setValue(this.compagniaId);

            this.roleService.getRoles().subscribe((pagedData) => {
                this.authorities = pagedData.result;

                this.authorities.forEach(() =>
                    this.authoritiesFormArray.push(new FormControl(false))
                );
            });
        }
    }

    isRolePresent(role: any) {
        const result = this.user.role.filter((c) => c === role.authority);
        return result.length > 0 ? true : false;
    }

    get authoritiesFormArray() {
        return this.userForm.controls.authorities as FormArray;
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.userForm.controls;
    }

    onChangeCompagnia(compagniaId) {
        this.compagniaId = compagniaId;
        this.roleService.getRoles(compagniaId).subscribe((pagedData) => {
            this.authorities = pagedData.result;
        });
    }

    saveEntity() {
        this.submitted = true;

        if (this.userForm.invalid) {
            return;
        }

        this.isLoading = true;
        
        const selectedRoleIds = this.userForm.value.authorities.map(
            (checked, i) => (checked ? this.authorities[i].authority : null)
        );

        const selectedRoleIdsNew = selectedRoleIds.filter( 
            element => element != null
        );

        this.user = {
            id: this.userForm.value.id,
            organizationId: this.compagniaId,
            username: this.userForm.value.username,
            password: this.userForm.value.password,
            matchingPassword: this.userForm.value.matchingPassword,
            firstName: this.userForm.value.firstName,
            lastName: this.userForm.value.lastName,
            email: this.userForm.value.email,
            enabled: this.userForm.value.active,
            landingPage: this.userForm.value.landingPage,
            role: selectedRoleIdsNew
        };

        if (this.newUser) {
            this.userService.addUser(this.user).subscribe((data) => {
                if (data.message === 'success') {
                    this.ngxAlertService.success(
                        '',
                        'Registrazione avvenuta con successo'
                    );

                    // publish event so list controller can refresh
                    //this.pubSubService.publish('users-updated');
                    this.router.navigate(['/user']);
                } else {
                    this.ngxAlertService.error('', data.result);
                }
                this.isLoading = false;
            });
        } else {
            this.userService.updateUser(this.user).subscribe((data) => {
                if (data.message === 'success') {
                    this.ngxAlertService.success(
                        '',
                        'Aggionamento avvenuto con successo'
                    );

                    // publish event so list controller can refresh
                    //this.pubSubService.publish('users-updated');
                    this.router.navigate(['/user']);
                } else {
                    this.ngxAlertService.error('', data.result);
                }
                this.isLoading = false;
            });
        }

        // redirect to users view
        //this.router.navigate(['/user']);
    }
}
