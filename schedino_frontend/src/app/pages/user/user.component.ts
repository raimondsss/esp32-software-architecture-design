import {Component, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ConfirmationModalComponent} from 'app/components/dialog/confirmation-modal.component';
import {PubSubService} from '@services/pub-sub.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {UserInfoService} from '../../services/user-info.service';
import {UserService} from '@services/api/user.service';
import {RoleService} from '@services/api/role.service';
import {Page} from 'app/models/paging/page';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent {
    superuser: boolean;
    navigationSubscription;
    subscription: Subscription;
    showSlideOut: boolean;
    isLoading = false;
    columns: any[];
    page = new Page();
    rows: any[];
    bckrows: any[];
    user: any = {};
    isNewUser: boolean;
    filterData = [];
    compagniaId: any;
    compagnie: any[];
    roleId: any;
    roles: any[];
    username: any;

    @ViewChild('userTable') table;

    constructor(
        private router: Router,
        private userInfoService: UserInfoService,
        private userService: UserService,
        private roleService: RoleService,
        private modalService: BsModalService,
        private pubSubService: PubSubService,
        private ngxAlertService: NgxAlertService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 10;
    }

    ngOnInit() {
        var me = this;
        //me.setPage({ offset: 0 });
        //me.getPageData();
        this.superuser = this.userInfoService.isSuperuser();

        /*if (this.superuser) {
            this.operatoreService.getList().subscribe((pagedData) => {
                this.compagnie = pagedData.items;
            });
        } else {*/
        this.compagniaId = this.userInfoService.getOrganizationId();
        this.roleService.getRoles(this.compagniaId).subscribe((pagedData) => {
            this.roles = pagedData.result;
        });
        this.getPageData();
        //}

        // reload users when updated
        this.subscription = this.pubSubService
            .on('users-updated')
            .subscribe(() => this.getPageData());

        this.columns = [
            {prop: 'userId', name: 'ID', width: 100},
            {prop: 'firstName', name: 'First Name', width: 120},
            {prop: 'lastName', name: 'Last Name', width: 120},
            {prop: 'email', name: 'Email', width: 250},
            {prop: 'active', name: 'Attivo', width: 120}
        ];
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();

        // avoid memory leaks here by cleaning up after ourselves. If we
        // don't then we will continue to run our initialiseInvites()
        // method on every navigationEnd event.
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }

    getPageData() {
        var me = this;
        this.isLoading = true;
        this.userService
            .getUsers(this.compagniaId.toString(), this.username, this.roleId)
            .subscribe((data) => {
                this.rows = data.result;
                this.bckrows = data.result;
                this.filterData = [...this.rows];

                this.page.pageNumber = 0;
                this.page.size = data.result.length;
                this.page.totalPages = 1;
                this.page.totalElements = data.result.length;

                this.isLoading = false;
            });
    }

    /**
     * Populate the table with new data based on the page number
     * @param page The page to select
     */
    setPage(pageInfo) {
        this.isLoading = true;
        this.page.pageNumber = pageInfo.offset;

        this.userService
            .getUsers(this.compagniaId.toString(), this.username, this.roleId)
            .subscribe((pagedData) => {
                this.page.pageNumber = pagedData.currentPageNumber;
                this.page.size = pagedData.pageSize;
                this.page.totalPages = pagedData.totalPages;
                this.page.totalElements = pagedData.totalItems;

                this.rows = pagedData.result;
                this.filterData = [...this.rows];
                this.isLoading = false;
            });
    }

    search() {
        if (this.compagniaId === undefined || this.compagniaId == '') return;

        //this.setPage({ offset: 0 });
        this.getPageData();
    }

    onChangeCompagnia(compagniaId) {
        this.compagniaId = compagniaId;
        this.roleService.getRoles(compagniaId).subscribe((pagedData) => {
            this.roles = pagedData.result;
        });

        //this.search();
    }

    onChangeRole(roleId) {
        this.roleId = roleId;

        this.search();
    }

    // filters results
    filterDatatable(event) {
        // get the value of the key pressed and make it lowercase
        let val = event.target.value.toLowerCase();
        // get the amount of columns in the table
        let colsAmt = this.columns.length;
        // get the key names of each column in the dataset
        let keys = Object.keys(this.bckrows[0]);
        // assign filtered matches to the active datatable
        this.rows = this.filterData.filter(function (item) {
            // iterate through each row's column data
            for (let i = 0; i < colsAmt; i++) {
                // check for a match
                if (
                    item[keys[i]].toString().toLowerCase().indexOf(val) !==
                        -1 ||
                    !val
                ) {
                    // found match, return true to add to result set
                    return true;
                }
            }
        });
        // whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    newUser() {
        this.showSlideOut = true;
        this.isNewUser = true;
        this.user.userId = '';
    }

    editUser(user: any) {
        this.showSlideOut = true;
        this.isNewUser = false;
        this.user.userId = user.id;
    }

    deleteEntity(user: any) {
        var me = this;

        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(
            "Confermi la cancellazione dell'utente?",
            ''
        );

        (<ConfirmationModalComponent>modal.content).onClose.subscribe(
            (result) => {
                if (result === true) {
                    this.userService.deleteUser(user.id).subscribe((data) => {
                        if (data.message === 'success') {
                            this.ngxAlertService.success(
                                '',
                                'Eliminazione avvenuta con successo'
                            );
                            me.getPageData();
                        } else {
                            this.ngxAlertService.error(
                                '',
                                data.operationMessage
                            );
                        }
                    });
                } else if (result === false) {
                    // when pressed No
                } else {
                    // When closing the modal without no or yes
                }
            }
        );
    }

    resetPassword(user: any) {
        var me = this;

        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal(
            "Confermi il reset della password dell'utente?",
            ''
        );

        (<ConfirmationModalComponent>modal.content).onClose.subscribe(
            (result) => {
                if (result === true) {
                    const params = {
                        email: user.email
                    };
                    this.isLoading = true;
                    this.userService
                        .resetPassword(user.email)
                        .subscribe((data) => {
                            this.isLoading = false;
                            if (data.message === 'success') {
                                this.ngxAlertService.success(
                                    '',
                                    'Reset password avvenuto con successo, la nuova password è stata mandata via email.'
                                );
                                me.getPageData();
                            } else {
                                this.ngxAlertService.error(
                                    '',
                                    data.operationMessage
                                );
                            }
                        });
                } else if (result === false) {
                    // when pressed No
                } else {
                    // When closing the modal without no or yes
                }
            }
        );
    }
}
