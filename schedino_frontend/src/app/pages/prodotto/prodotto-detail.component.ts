import {Component, OnInit, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from '@angular/forms';
import {PubSubService} from '@services/pub-sub.service';
import {UserInfoService} from '../../services/user-info.service';
import {ProdottoService} from '@services/api/prodotto.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {AppConfig} from '../../app-config';

@Component({
    selector: 'app-prodotto-detail',
    templateUrl: './prodotto-detail.component.html',
    styleUrls: ['./prodotto-detail.component.scss']
})
export class ProdottoDetailComponent implements OnInit {
    superuser: boolean;
    utente: boolean;
    scoutuser: boolean;
    isLoading = false;
    newProdotto: boolean;
    entityId: any;
    prodotto: any;
    title: string;
    prodottoForm: FormGroup;
    submitted = true;
    prodottoId: any;
    navi: any[];
    compagniaId: any;
    compagnie: any[];
    paeseId: any;
    paesi: any[];
    filteredPaesi: any[];
    tipologieProdotto: any[];
    dynamicPositionType: any[];
    entityForm: FormGroup;
    fileUpload = {
        filePath: '',
        file: null
    };
    logo = null;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userInfoService: UserInfoService,
        private theService: ProdottoService,
        private ngxAlertService: NgxAlertService,
        private formBuilder: FormBuilder,
        private pubSubService: PubSubService
    ) {}

    ngOnInit() {
        var me = this;

        this.superuser = this.userInfoService.isSuperuser();
        this.utente = this.userInfoService.isUser();
        /*if (this.superuser) {
            this.theService.getList().subscribe((pagedData) => {
                this.compagnie = pagedData.items;
            });
        } else {*/
        //this.compagniaId = this.userInfoService.getOrganizationId();
        //}

        this.prodottoForm = this.formBuilder.group({
            id: [''],
            compagniaId: [''],
            nome:['', [Validators.required]],
            codice: ['', [Validators.required]],
            calorie: ['', [Validators.required]],
            carboidrati: ['', [Validators.required]],
            grassi: ['', [Validators.required]],
            proteine: ['', [Validators.required]],
            
        });

        this.title = 'Nuovo Prodotto';
        this.newProdotto = true;
        let prodottoId = this.route.snapshot.params['id'];
        if (prodottoId) {
            this.title = 'Aggiona Prodotto';
            this.newProdotto = false;
            this.prodottoId = prodottoId;
            this.theService.getEntity(this.prodottoId).subscribe((data) => {
                this.prodotto = data.data;
                this.prodottoForm.patchValue({
                    id: this.prodotto.id,
                    nome: this.prodotto.nome,
                    codice: this.prodotto.codice,
                    calorie: this.prodotto.calorie,
                    carboidrati: this.prodotto.carboidrati,
                    proteine: this.prodotto.proteine,
                    grassi: this.prodotto.grassi,
                });
            });
        } else {
            this.prodottoForm.controls['compagniaId'].setValue(this.compagniaId);
        }
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.prodottoForm.controls;
    }

    onChangeCompagnia(compagniaId) {
        this.compagniaId = compagniaId;
    }

    selectEvent(item) {
        // do something with selected item
        console.log(item);
        //this.prodottoForm.controls['bandiera'].setValue(item.nome);
    }

    onChangeSearch(search: string) {
        this.filteredPaesi = [];
        for (var paese of this.paesi) {
            if (paese.nome.toLowerCase().indexOf(search.toLowerCase()) === 0) {
                //this.filteredPaesi.push( {
                //    id: paese.id,
                //    nome: paese.nome,
                //    sigla: paese.siglaIso3
                //});
                this.filteredPaesi.push(paese.nome);
            }
        }
    }

    onFocused(e) {
        // do something
    }

    hasImage(): boolean {
        if (this.logo && this.logo.length) return true;

        return false;
    }

    getImageUrl() {
        if (this.logo === null) return '#';
        return (
            AppConfig.settings.apiServer.apiFilestoragePath +
            'compagnia$' +
            this.compagniaId +
            '$' +
            this.logo +
            '/001'
        );
    }

    uploadFileChange(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length == 0) {
            return;
        }

        let file: File = fileList[0];
        let fileSize: number = fileList[0].size;
        if (fileSize > 10485760) {
            this.ngxAlertService.success('', 'File size is exceeded');
            return;
        }

        this.fileUpload.file = file;
        this.entityForm.markAsDirty();
    }

    saveEntity() {
        this.submitted = true;

        if (this.prodottoForm.invalid) {
            return;
        }

        this.isLoading = true;

        this.prodotto = {
            id: this.prodottoForm.value.id,
            compagnia: {
                id: this.prodottoForm.value.compagniaId
            },
            nome: this.prodottoForm.value.nome,
            codice: this.prodottoForm.value.codice,
            calorie: this.prodottoForm.value.calorie,
            carboidrati: this.prodottoForm.value.carboidrati,
            grassi: this.prodottoForm.value.grassi,
            proteine: this.prodottoForm.value.proteine
        };

        if (this.newProdotto) {
            this.theService.addEntity(this.prodotto).subscribe((data) => {
                if (data.operationStatus === 'SUCCESS') {
                    this.ngxAlertService.success(
                        '',
                        'Aggiunta avvenuta con successo'
                    );

                    // publish event so list controller can refresh
                    //this.pubSubService.publish('prodotto-updated');
                    this.router.navigate(['/prodotti']);
                } else {
                    this.ngxAlertService.error('', data.operationMessage);
                }
                this.isLoading = false;
            });
        } else {
            this.theService.updateEntity(this.prodotto).subscribe((data) => {
                if (data.operationStatus === 'SUCCESS') {
                    this.ngxAlertService.success(
                        '',
                        'Aggiornamento avvenuto con successo'
                    );

                    // publish event so list controller can refresh
                    //this.pubSubService.publish('prodotto-updated');
                    this.router.navigate(['/prodotti']);
                } else {
                    this.ngxAlertService.error('', data.operationMessage);
                }
                this.isLoading = false;
            });
        }
    }
}
