import {Component, ViewChild, HostListener} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ConfirmationModalComponent} from 'app/components/dialog/confirmation-modal.component';
import {PubSubService} from '@services/pub-sub.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {UserInfoService} from '../../services/user-info.service';
import {StatisticService} from '@services/api/statistic.service';
import {UserService} from '@services/api/user.service';
import {Page} from 'app/models/paging/page';
import moment from 'moment';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
    view: any[] = [460, 180];
    superuser: boolean;
    companyAdmin: boolean;
    navigationSubscription;
    subscription: Subscription;
    showSlideOut: boolean;
    isLoading = false;
    columns: any[];
    page = new Page();
    rows: any[];
    bckrows: any[];
    filterData = [];
    compagniaId: any;
    compagnie: any[];
    nome: any;
    cognome: any;
    marittimoId: any;
    gradi: any[];
    aree: any[];
    livelli: any[];
    gradoId: any;
    areaId: any;
    livelloId: any;
    rawEvent: any;
    countProgetto: number;
    countProgettoScreening: number;
    countProgettoSviluppo: number;
    countProgettoBocciati: number;

    countUtenti: number;
    countSchedini: number;
    countProdotti: number;
    countCalorieOdierne: number;
    countCalorieRimanenti: number;

    totale: number;
    legendPosition: string = 'below';
    ranksData: any[] = [];
    levelsData: any[] = [];
    risks: any[];
    column:any;
    widthScreen:any;
    stato: any;
    scoutId: any;
    operatoreId: any;
    potenzaRichiesta: any;
    potenzeRichieste: any[];
    user: any;
    userId: any;
    userForm: FormGroup;
    dataOdierna = new Date();
    dataTest: any;
    colorScheme = {
        domain: ['#61c673', '#EE4B2B' ]
    };

    // options
    single: any[];
    gradient: boolean = true;
    showLegend: boolean = true;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    
    public utente: boolean;
    public scoutuser: boolean;
    
    public contextmenuX: any;
    public contextmenuY: any;
    public contextmenu = false;
    public selected: any[] = [];
    public sidebarMenuOpened = true;

    @ViewChild('marittimiTable') table;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private modalService: BsModalService,
        private userInfoService: UserInfoService,
        private userService: UserService,
        private statisticService: StatisticService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.column=10;
        this.widthScreen=window.innerWidth-260;
        this.countProgetto = 0;
        this.countProgettoScreening = 0;
        this.countProgettoSviluppo = 0;
        this.countProgettoBocciati = 0;
        this.livelloId = '';
        this.totale=0;

        console.log("potenza",this.potenzaRichiesta);
    }

    ngOnInit() {
        var me = this;

        this.compagniaId = null;
        this.superuser = this.userInfoService.isSuperuser();
        this.companyAdmin = this.userInfoService.isCompanyAdmin();
        this.utente = this.userInfoService.isUser();
        this.userId = this.userInfoService.getUserId();

        let endDate = new Date();
        endDate.setDate(this.dataOdierna.getDate()-7);

        this.potenzeRichieste=[
            {id:"0-1" ,descrizione:"0-1 M"},
            {id:"1-10" ,descrizione:"1-10 M"},
            {id:"11-" ,descrizione:">10 M"},
        ]


        this.userService.getUser(this.userId).subscribe((data) => {
            this.user = data.result;
            console.log('user',this.user);

            if(this.utente){

                this.statisticService.getWeekStats(this.user.id, moment(endDate).format('YYYY-MM-DD'), moment(this.dataOdierna).format('YYYY-MM-DD')).subscribe(pagedData => {          
        
                    console.log('page:',pagedData.items);

                    this.page.pageNumber = 1;
                    this.page.size = pagedData.items.length;
                    this.page.totalPages = 1;
                    this.page.totalElements = pagedData.items.length;
    
                    this.rows = pagedData.items;
                    this.columns = [];
                    if(this.rows.length > 0){
                        let row = this.rows[0];
                        for(const key in row){
                            console.log(key, row[key]);
                            this.columns.push({
                                name: key,
                                prop: key,
                                flexGrow: key == "PRODOTTO" ? 1 : 0.5,
                            })
                        }
                    }             
                });
            }
            me.setPage({offset: 0});
        });

        if(this.superuser){

            this.statisticService.getSchediniAssociati().subscribe(pagedData => {          
    
                console.log('page stats:',pagedData.items);

                this.page.pageNumber = 1;
                this.page.size = pagedData.items.length;
                this.page.totalPages = 1;
                this.page.totalElements = pagedData.items.length;

                this.rows = pagedData.items;    
            });
        }


    }

    ngOnDestroy() {
        // avoid memory leaks here by cleaning up after ourselves. If we
        // don't then we will continue to run our initialiseInvites()
        // method on every navigationEnd event.
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }

    /**
     * Populate the table with new data based on the page number
     * @param page The page to select
     */
    setPage(pageInfo) {
        this.isLoading = true;
        this.page.pageNumber = pageInfo.offset;

        this.updateStats()
        this.isLoading = false;
    }

    updateStats(){
        this.statisticService.getGeneralStats(moment(new Date()).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), this.user.id).subscribe(pagedData => {
        
            this.countUtenti = pagedData.data.numUtenti;
            this.countSchedini = pagedData.data.numSchedini;
            this.countProdotti = pagedData.data.numProdotti;
            this.countCalorieOdierne = pagedData.data.calorieTotali;


            this.countCalorieRimanenti = this.user.obbiettivoGiornaliero - this.countCalorieOdierne;
            if(this.countCalorieRimanenti<0)
                this.countCalorieRimanenti = 0;

            this.single = [{
                    "name": "Kcal Rimanenti",
                    "value": this.countCalorieRimanenti
                    
                },
                {
                    "name": "Kcal Odierne",
                    "value": this.countCalorieOdierne
                    
                }
            ];
            
            console.log('page:',pagedData.data);
        });
    }

    onChangePotenza(id) {
        this.potenzaRichiesta = id;
        
        this.updateStats();
    }

    getStatoFase(row){
       
        if(row.stato == 0){
            if(row.screening[0].stato == 0 && !this.scoutuser){
                return 'Da verificare'
            }
            else if(row.screening[0].stato == 0 && this.scoutuser){
                return 'In attesa di verifica'
            }
            else if(row.screening[0].stato === 1 || (this.scoutuser && row.screening[0].stato === 5) || (this.scoutuser && row.screening[0].stato === 7) || (this.scoutuser && row.screening[0].stato === 8)){
                return 'In attesa del report di valutazione'
            }
            else if(row.screening[0].stato === 2){
                return 'Rigettato'
            }
            else if(row.screening[0].stato === 3){
                return 'Richiesta chiarimenti'
            }
            else if(row.screening[0].stato === 4 && this.scoutuser){
                return 'In attesa di verifica'
            }
            else if(row.screening[0].stato === 4 && !this.scoutuser){
                return 'Da verificare'
            }
            else if(row.screening[0].stato === 5 && !this.scoutuser){
                return 'Caricamento Report'
            }
            else if(row.screening[0].stato === 6 && !this.scoutuser && row.screening[0].tipologiaPagamento != "asta"){
                return 'Inserisci contratto preliminare'
            }
            else if(row.screening[0].stato >= 6 && this.scoutuser && row.screening[0].tipologiaPagamento != "asta"){
                return 'In attesa del contratto preliminare'
            }
            else if(row.screening[0].stato >= 6 && row.screening[0].tipologiaPagamento == "asta"){
                return "In attesa del verbale dell'asta"
            }
            else if(row.screening[0].stato === 7 && !this.scoutuser){
                return 'Chiarimenti Report richiesti'
            }
            else if(row.screening[0].stato === 8 && !this.scoutuser){
                return 'Chiarimenti Report'
            }
            else if(row.screening[0].stato === 9 && !this.scoutuser){
                return 'Contratto Preliminare'
            }
            else if(row.screening[0].stato === 10 && !this.scoutuser){
                return 'Contratto Definitivo'
            }
        }
        else if(row.stato == 1 && !this.scoutuser){
            if(row.sviluppo[0].stato == 0){
                return 'Creato'
            }
            else if(row.sviluppo[0].stato == 1){
                return 'In attesa del bonifico'
            }
            else if(row.sviluppo[0].stato == 2){
                return 'Bonifico Caricato'
            }
            else if(row.sviluppo[0].stato == 3){
                return 'In attesa di preventivo'
            }
            else if(row.sviluppo[0].stato == 4){
                return 'Preventivo Caricato'
            }
            else if(row.sviluppo[0].stato == 5){
                return 'Preventivo Accettato'
            }
            else if(row.sviluppo[0].stato == 6){
                return 'Richiesta Modifica'
            }
            else if(row.sviluppo[0].stato == 7){
                return 'Modifica Approvata'
            }
        }else if(row.stato == 1 && this.scoutuser){
            return 'Concluso'
        }
        
    }

    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }
    
    onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    onTableContextMenu(contextMenuEvent) {
        this.contextmenuX = contextMenuEvent.event.pageX;
        this.contextmenuY = contextMenuEvent.event.pageY;
        this.marittimoId = contextMenuEvent.content.id;
        this.contextmenu = true;
        contextMenuEvent.event.preventDefault();
        contextMenuEvent.event.stopPropagation();
    }

    onTreeAction(event: any) {
        const index = event.rowIndex;
        const row = event.row;
        if (row.treeStatus === 'collapsed') {
            row.treeStatus = 'expanded';
        } else {
            row.treeStatus = 'collapsed';
        }
        this.rows = [...this.rows];
    }

    percentageFormatting(c) {
        return Math.round(c);
    }

    getRowClass = (row) => {
        //console.log('rowClass');
        return {
            'row-color': row.father === null
        };
    };

    @HostListener('document:click', ['$event'])
    clickedOutside($event) {
        if ($event.target.className !== 'dropdown-item context-menu') {
            this.contextmenu = false;
        }
    }

    search() {
        this.setPage({offset: 0});
        //this.getPageData();
    }

    // filters results
    filterDatatable(event) {
        // get the value of the key pressed and make it lowercase
        let val = event.target.value.toLowerCase();
        // get the amount of columns in the table
        let colsAmt = this.columns.length;
        // get the key names of each column in the dataset
        let keys = Object.keys(this.bckrows[0]);
        // assign filtered matches to the active datatable
        this.rows = this.filterData.filter(function (item) {
            // iterate through each row's column data
            for (let i = 0; i < colsAmt; i++) {
                // check for a match
                if (
                    item[keys[i]].toString().toLowerCase().indexOf(val) !==
                        -1 ||
                    !val
                ) {
                    // found match, return true to add to result set
                    return true;
                }
            }
        });
        // whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

}
