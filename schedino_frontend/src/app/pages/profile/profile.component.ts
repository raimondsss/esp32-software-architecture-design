import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from '@angular/forms';
import {UserInfoService, UserInStorage} from '../../services/user-info.service';
import {UserService} from '@services/api/user.service';
import {NgxAlertService} from '@services/ngxalert.service';
import {MustMatch} from '../../components/helpers/must-match.validator';
import {SchedinoService} from '@services/api/schedino.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    isLoading = false;
    submitted = true;
    public utente: boolean;
    public superuser: boolean;
    userForm: FormGroup;
    passwordForm: FormGroup;
    user: any;
    password: any;
    userId: any;
    compagniaId: any;
    schedini: any[];


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userInfoService: UserInfoService,
        private schedinoService: SchedinoService,
        private userService: UserService,
        private ngxAlertService: NgxAlertService,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        var me = this;
        this.userId = this.userInfoService.getUserId();
        this.superuser = this.userInfoService.isSuperuser();
        this.utente = this.userInfoService.isUser();


        console.log('userId', this.userId);
        this.user = this.userInfoService.getUserInfo();

        this.schedinoService.getList().subscribe((pagedData) => {
            this.schedini = pagedData.items;
            console.log(this.schedini);
        });

        if(this.superuser){
            this.userForm = this.formBuilder.group(
                {
                    organizationId: ['', Validators.required],
                    username: ['', Validators.required],
                    firstName: [''],
                    lastName: [''],
                    email: ['', [Validators.required, Validators.email]],
                    active: [true],
                    landingPage: [''],
                    authorities: new FormArray([]),
                }
            );
        }else{
            this.userForm = this.formBuilder.group(
                {
                    organizationId: ['', Validators.required],
                    username: ['', Validators.required],
                    firstName: [''],
                    lastName: [''],
                    email: ['', [Validators.required, Validators.email]],
                    active: [true],
                    landingPage: [''],
                    authorities: new FormArray([]),
                    obbiettivoGiornaliero: [''],
                    schedino: [''],
                }
            );
        }


        this.passwordForm = this.formBuilder.group(
            {
                password: ['', [Validators.required, Validators.minLength(6)]],
                matchingPassword: ['', [Validators.required]]
            },
            {
                validator: MustMatch('password', 'matchingPassword')
            }
        );

        this.userService.getUser(this.userId).subscribe((data) => {
            this.user = data.result;
            this.compagniaId = this.user.organizationId,
            console.log('user',this.user);
            this.userForm.patchValue({
                id: this.user.id,
                organizationId: this.user.organizationId,
                username: this.user.username,
                password: '0000000000000',
                matchingPassword: '0000000000000',
                firstName: this.user.firstName,
                lastName: this.user.lastName,
                email: this.user.email,
                active: this.user.enabled,
                landingPage: this.user.landingPage,
                authorities: this.user.role,
                obbiettivoGiornaliero: this.user.obbiettivoGiornaliero,
                schedino: this.user.schedino
            });

        });
    }

    get f() {
        return this.userForm.controls;
    }

    get f2() {
        return this.passwordForm.controls;
    }

    onChangeSchedino(id) {    
        let schedino = this.schedini.find(c => c.codice.toString().toUpperCase() === id.toUpperCase());

        if(schedino.utenti > 0){
            alert('schedino già associato');
            this.userForm.controls['schedinoId'].setValue(this.user.schedinoId);
        }else{
            this.userForm.controls['schedinoId'].setValue(id.toUpperCase());
        }
            
    }

    saveEntity() {
        this.submitted = true;

        if (this.userForm.invalid) {
            return;
        }

        this.isLoading = true;
        this.user = {
            id: this.userForm.value.id,
            organizationId: this.compagniaId,
            username: this.userForm.value.username,
            firstName: this.userForm.value.firstName,
            lastName: this.userForm.value.lastName,
            email: this.userForm.value.email,
            enabled: this.userForm.value.active,
            landingPage: this.userForm.value.landingPage,
            role: this.user.role,
            obbiettivoGiornaliero: this.userForm.value.obbiettivoGiornaliero,
            schedino: this.userForm.value.schedino.toUpperCase(),
            password: '0000000000000',
            matchingPassword: '0000000000000',
        };
        console.log('user dopo save',this.user);

        this.userService.updateUser(this.user).subscribe((data) => {
            if (data.message === 'success') {
                this.ngxAlertService.success('', 'Update successful');
                const userObj: UserInStorage = this.userInfoService.getUserInfo();
                userObj.displayName = this.user.firstName+ " "+ this.user.lastName;
                this.userInfoService.storeUserInfo(JSON.stringify(userObj));
            } else {
                this.ngxAlertService.error('', data.result);
            }

            this.isLoading = false;
        });
    }

    changePassword() {
        this.submitted = true;

        if (this.passwordForm.invalid) {
            return;
        }

        this.isLoading = true;
        this.password = {
            username: this.user.username,
            email: this.user.email,
            newPassword: this.passwordForm.value.password,
        };

        this.userService.updatePassword(this.password).subscribe((data) => {
            if (data.message === 'success') {
                this.ngxAlertService.success('', 'Update successful');

            } else {
                this.ngxAlertService.error('', data.result);
            }

            this.isLoading = false;
        });
    }
}
