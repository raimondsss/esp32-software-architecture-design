import { Component, Input, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent implements OnInit {
  @Input() x = 0;
  @Input() y = 0;
  @Input() isShow: boolean;
  @Input() id = 0;
  constructor(private router: Router) { }

  ngOnInit() {
    console.log(this.x);
    console.log(this.y);
  }

  public onEdit(e): void {
    console.log('edit '+this.id);
    this.router.navigate(['/editmarittimo', this.id]);
  }

  public onRemove(e): void {
    console.log('remove '+this.id);
  }

  public onAssessment(e): void {
    this.router.navigate(['/corsimarittimo', this.id]);
  }
}