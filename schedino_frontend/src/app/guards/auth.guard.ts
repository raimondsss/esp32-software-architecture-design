import {Injectable} from '@angular/core';
import {
    CanActivate,
    CanActivateChild,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    Router
} from '@angular/router';
import {Observable} from 'rxjs';
import {AppService} from '@services/app.service';
import {UserInfoService} from '../services/user-info.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(
        private router: Router,
        private appService: AppService,
        private userInfoService: UserInfoService
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        //return this.getProfile();
        return this.checkLogin();
    }

    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        return this.canActivate(next, state);
    }

    async getProfile() {
        if (this.appService.user) {
            return true;
        }

        try {
            await this.appService.getProfile();
            return true;
        } catch (error) {
            return false;
        }
    }

    checkLogin(): boolean {
        if (this.userInfoService.isLoggedIn()) {
            // check the token
            const expiresIn = this.userInfoService.getOauthInfo().expiresIn;
            const lastAccess = this.userInfoService.getOauthInfo().lastAccess;
            if (new Date().getTime() - lastAccess < expiresIn * 1000)
                return true;
            //if (this.checkAccessToken())
            //    return true;
        }

        console.log(
            'User is not logged - This routing guard prevents redirection to any routes that needs logging.'
        );
        //Store the original url in login service and then redirect to login page
        //this.loginService.landingPage = url;
        this.router.navigate(['/login']);
        return false;
    }
}
