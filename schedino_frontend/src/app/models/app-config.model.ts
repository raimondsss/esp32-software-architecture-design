export interface IAppConfig {
    env: {
        name: string;
    };
    application: {
        version: string;
        locale: string;
        currencyFormat: {
            style: string;
            currency: string;
        };
        dateFormat: {
            year: string;
            month: string;
            day: string;
        };
    };
    appInsights: {
        instrumentationKey: string;
    };
    logging: {
        console: boolean;
        appInsights: boolean;
    };
    aad: {
        requireAuth: boolean;
        tenant: string;
        clientId: string;
    };
    apiServer: {
        metadata: string;
        rules: string;
        clientId: string;
        clientSecret: string;
        webRtcServerUrl: string;
        baseApiPath: string;
        baseResPath: string;
        websocketPath: string;
        baseFacePath: string;
        queueStatusTopic: string;
        queueUpdateTopic: string;
        faceDetectTopic: string;
        apiFilestoragePath: string;
        apiImagestoragePath: string;
        rtspWebCamUrl: string;
    };
    moodle: {
        baseApiPath: string;
        wstoken: string;
    };
    recognition: {
        resetTimeout: number;
        webcam: boolean;
        termoscanner: boolean;
        maxTemperature: number;
    };
    toastConfig: {
        positionClass: string;
        animation: string;
        showCloseButton: boolean;
        tapToDismiss: boolean;
        timeOut: number;
    };
}
