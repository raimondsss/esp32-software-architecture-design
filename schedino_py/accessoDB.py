import mysql.connector
from mysql.connector import errorcode

try:
  cnx = mysql.connector.connect(user='root',
                                password='admin2018',
                                database='schedino')
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)

##############################################################################################################################

from http import client
import paho.mqtt.client as mqtt

def sistemoLaStringa(s):
    s = s.replace("b", "")
    s = s.replace("\\r", "")
    s = s.replace("'", "")
    s = s.replace("\\n", "")
    return s

def on_message(client, userdata, msg):
   #Funzione che legge il codice a barre ricevuto e fa la query al dbms
    rcv_msg = sistemoLaStringa(str(msg.payload))
    
    print(rcv_msg)

    token = rcv_msg.split(",")

    ID = token[0]

    codice = " \""+ token[1] + "\" "

    cursor = cnx.cursor()

    query = ("SELECT * FROM prodotti WHERE codice=" + codice)

    cursor.execute(query)

    result = cursor.fetchall()

    if(len(result) > 0):

        for x in result:
            nome = x[2]
            calorie = x[3]
            carboidrati = x[4]
            grassi = x[5]
            proteine = x[6]

        messaggio = ID+"-"+ nome + " " + str(calorie) + " " +  str(carboidrati) + " " +  str(grassi) + " "  +  str(proteine) 

        client.publish("Bilancia/Valori",messaggio)
    
    else:
        client.publish("Bilancia/Valori", ID+"-"+"Prodotto_inesistente")

    cursor.close()

def on_connect(client, userdata, flags, rc):
    #Funzione per connettersi al broker mqtt ed iscriversi al topic
    print("Mi connetto al broker")
 

try:
    client = mqtt.Client("App", True)
    client.on_message = on_message
    client.on_connect = on_connect
    client.connect("broker.hivemq.com", 1883, 60)
    client.subscribe("Bilancia/Codice",0)
    client.loop_start()

    input("Premere ENTER per terminare...\n")
    
except Exception as e:
    print('exception ', e)
