package com.ruubel.service;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.stringtemplate.v4.ST;

import com.ruubel.dto.EmailMessageDto;
import com.ruubel.dto.UserDto;
import com.ruubel.enums.EmailTemplateCode;
import com.ruubel.event.OnGenerateAccountEvent;
import com.ruubel.event.OnSendEmailEvent;
import com.ruubel.model.EmailQueue;
import com.ruubel.model.EmailTemplate;
import com.ruubel.repository.EmailQueueRepo;
import com.ruubel.repository.EmailTemplateRepo;
import com.ruubel.tools.MD5Tools;

/**
 * Service class is to configure and create email template
 * 
 * @author
 *
 */
@Service
@Configuration
public class EmailServiceProducer {

    private final static Logger logger = LoggerFactory.getLogger(EmailServiceProducer.class);

    @Value("${app.email-template.path}")
    private String emailTemplatePath;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private EmailTemplateRepo emailTemplateRepository;

    @Autowired
    private EmailQueueRepo emailQueueRepository;

    /**
	 * sendAccountRegistrationEmail - 
	 * 
	 * @param UserDto
	 * @return void
	 */
	public void sendEmail(EmailMessageDto emailMessage, Locale locale) {
		
		try {

			String authToken = MD5Tools.generateActivationToken("PROVA" + new Date());

			EmailQueue emailQueue = new EmailQueue();
			emailQueue.setRenderedSubject(emailMessage.getSubject());
			emailQueue.setRenderedTemplate(emailMessage.getText());
			emailQueue.setToAddress(emailMessage.getTo());
			emailQueue.setFromAddress(emailMessage.getFrom());
			emailQueue.setAuthToken(authToken);
			emailQueue.setTemplateId(0);
			emailQueue.setInsertDate(new Date());
			EmailQueue emailQueuesaved = emailQueueRepository.save(emailQueue);

			eventPublisher.publishEvent(new OnSendEmailEvent(emailQueuesaved));

		} catch (Exception e) {
			logger.error("Exception: ", e);
		}

	}

    /**
	 * sendAccountRegistrationEmail - 
	 * 
	 * @param UserDto
	 * @return void
	 */
	public void sendAccountRegistrationEmail(UserDto user, Locale locale) {
		
		try {
			String lang = locale.getLanguage().toUpperCase();
			EmailTemplate emailTemplate = emailTemplateRepository.findOneByCodeAndLanguage(EmailTemplateCode.ACCOUNT_REGISTRATION.toString(), locale.getLanguage().toUpperCase());
			if (emailTemplate == null || emailTemplate.getId() == null) {
				logger.error("Fatal error: email template "+EmailTemplateCode.ACCOUNT_REGISTRATION.toString() + " not found");
				return;
			}

			String authToken = MD5Tools.generateActivationToken(user.getUsername() + new Date());

			File file = new File(emailTemplatePath+"/"+emailTemplate.getText());
		    String emailData = FileUtils.readFileToString(file, "UTF-8");
		    
			ST templateToProcess = new ST(emailData, '$', '$');
			//templateToProcess.add("IMAGE_PATH", emailTemplatePath);
			templateToProcess.add("NOME_UTENTE", user.getFirstName() + " " + user.getLastName());
			templateToProcess.add("USERNAME", user.getUsername());
			templateToProcess.add("PASSWORD", user.getPassword());

			EmailQueue emailQueue = new EmailQueue();
			emailQueue.setRenderedSubject(emailTemplate.getSubject());
			emailQueue.setRenderedTemplate(templateToProcess.render());
			emailQueue.setToAddress(user.getEmail());
			emailQueue.setFromAddress(emailTemplate.getFrom());
			emailQueue.setAuthToken(authToken);
			emailQueue.setTemplateId(emailTemplate.getId());
			emailQueue.setInsertDate(new Date());
			EmailQueue emailQueuesaved = emailQueueRepository.save(emailQueue);

			eventPublisher.publishEvent(new OnGenerateAccountEvent(emailQueuesaved));

		} catch (Exception e) {
			logger.error("Exception: ", e);
		}

	}
	
	public void sendResetPasswordEmail(UserDto user, Locale locale) {
		
		try {

			EmailTemplate emailTemplate = emailTemplateRepository.findOneByCodeAndLanguage(EmailTemplateCode.FORGOT_PASSWORD_REQUEST.toString(), locale.getLanguage().toUpperCase());
			if (emailTemplate == null || emailTemplate.getId() == null) {
				logger.error("Fatal error: email template "+EmailTemplateCode.FORGOT_PASSWORD_REQUEST.toString() + " not found");
				return;
			}

			String authToken = MD5Tools.generateActivationToken(user.getUsername() + new Date());

			File file = new File(emailTemplatePath+"/"+emailTemplate.getText());
		    String emailData = FileUtils.readFileToString(file, "UTF-8");
		    
			ST templateToProcess = new ST(emailData, '$', '$');
			//templateToProcess.add("IMAGE_PATH", emailTemplatePath);
			templateToProcess.add("NOME_UTENTE", user.getFirstName() + " " + user.getLastName());
			//templateToProcess.add("USERNAME", user.getUsername());
			templateToProcess.add("PASSWORD", user.getPassword());

			EmailQueue emailQueue = new EmailQueue();
			emailQueue.setRenderedSubject(emailTemplate.getSubject());
			emailQueue.setRenderedTemplate(templateToProcess.render());
			emailQueue.setToAddress(user.getEmail());
			emailQueue.setFromAddress(emailTemplate.getFrom());
			emailQueue.setAuthToken(authToken);
			emailQueue.setTemplateId(emailTemplate.getId());
			emailQueue.setInsertDate(new Date());
			EmailQueue emailQueuesaved = emailQueueRepository.save(emailQueue);

			eventPublisher.publishEvent(new OnGenerateAccountEvent(emailQueuesaved));

		} catch (Exception e) {
			logger.error("Exception: ", e);
		}

	}
	
}
