package com.ruubel.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.ruubel.dto.EmailDto;
import com.ruubel.event.listener.GenerateAccountListener;
import com.ruubel.exception.ApplicationServiceException;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

/**
 * Service class is to configure and send email using SendGrid API
 * 
 * @author Moniruzzaman Md
 *
 */
@Service
@Configuration
public class SendGridService {

	private final static Logger logger = LoggerFactory.getLogger(SendGridService.class);

	@Value("${spring.sendgrid.api-key}")
    private String sendGridApi;
	//private String sendGridApi = "Your_SendGripd_API_Key";

	/**
	 * PersonalizeEmail - details setting for each email. For the complete example:
	 * https://github.com/sendgrid/sendgrid-java/blob/master/examples/helpers/mail/Example.java
	 * 
	 * @param emailPojo
	 * @return Mail
	 */
	private Mail PersonalizeEmail(EmailDto emailPojo) {

		Mail mail = new Mail();

		/* From information setting */
		Email fromEmail = new Email();
		fromEmail.setName(emailPojo.getFromName());
		fromEmail.setEmail(emailPojo.getFromEmail());

		mail.setFrom(fromEmail);
		mail.setSubject(emailPojo.getEmailSubject());

		/*
		 * Personalization setting, we only add recipient info for this particular
		 * example
		 */
		Personalization personalization = new Personalization();
		Email to = new Email();
		to.setName(emailPojo.getToName());
		to.setEmail(emailPojo.getToEmail());
		personalization.addTo(to);

		personalization.addHeader("X-Test", "test");
		personalization.addHeader("X-Mock", "true");

		/* Substitution value settings */
		//personalization.addSubstitution("%name%", emailPojo.getToName());
		//personalization.addSubstitution("%from%", emailPojo.getFromName());

		mail.addPersonalization(personalization);

		/* Set template id */
		mail.setTemplateId(emailPojo.getTemplateId());

		/* Reply to setting */
		Email replyTo = new Email();
		replyTo.setName(emailPojo.getFromName());
		replyTo.setEmail(emailPojo.getFromEmail());
		mail.setReplyTo(replyTo);

		/* Adding Content of the email */
		Content content = new Content();

		/* Adding email message/body */
		//content.setType("text/plain");
		content.setType("text/html");
		content.setValue(emailPojo.getMessage());
		mail.addContent(content);

		return mail;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.monirthougth.sendgrid.service.SendGridService#sendMail(com.monirthought.
	 * sendgrid.pojo.EmailPojo)
	 */
	public void sendMail(EmailDto emailPojo) throws ApplicationServiceException {

		SendGrid sg = new SendGrid(sendGridApi);
		sg.addRequestHeader("X-Mock", "true");

		Request request = new Request();
		Mail mail = PersonalizeEmail(emailPojo);
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
		} catch (IOException ex) {
			logger.error("Exception: ", ex);
			//ex.printStackTrace();
			throw new ApplicationServiceException("C'è stato un errore nell'invio della mail! " + ex.getMessage());
			//return "Failed to send mail! " + ex.getMessage();
		}
		
		//return "Email is sent Successfully!!";
	}

}
