package com.ruubel.service;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class FileStorageProperties {

	public static final String RELPATH="filestorage";
	public static final String BUST="bust=002";
	
    /**
     * Folder location for storing files
     */
    private String location = "filestorage";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}