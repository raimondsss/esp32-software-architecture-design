package com.ruubel.service;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.Mockito.CALLS_REAL_METHODS;

import java.io.*;
import java.text.SimpleDateFormat;
import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import com.ruubel.repository.*;
import com.ruubel.utils.DataRow;
import com.ruubel.utils.DataTable;
import com.ruubel.utils.ExcelTable;
import com.ruubel.model.*;


@Service
public class ProdottoService {

    private final static Logger logger = LoggerFactory.getLogger(ProdottoService.class);

    @Autowired
    private ProdottoRepo prodottoRepository;   
    
    @Autowired private EntityManager entityManager;

    
    public Page<Prodotto> findAll(String codice, String nome, Pageable pageable){
        return this.prodottoRepository.findAll(new Specification<Prodotto>() {
            @Override
            public Predicate toPredicate(Root<Prodotto> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                try {
                    if (codice != null) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("codice")), codice.toLowerCase()));
                    }    
                    if (nome != null) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("nome")), nome.toLowerCase()));
                    }   
                } catch (Exception ex) {
              	  logger.error(ex.toString());
                }
                
                query.orderBy(criteriaBuilder.desc(root.get("nome")));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
  	}

    public Prodotto save(Prodotto prodotto) throws Exception {
    
    	Prodotto prodottoSaved = prodottoRepository.save(prodotto);

		return prodottoSaved;
    }
    

    public void delete(Integer id) throws Exception {
    	Prodotto prodotto = prodottoRepository.findOneById(id);
		
		prodottoRepository.deleteById(id);
    }

    public void saveFromFile(String filepath, String token) throws Exception {
    }

    public ProdottoRepo getRepository() {
        return prodottoRepository;
    }

}