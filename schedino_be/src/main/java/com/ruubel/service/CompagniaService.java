package com.ruubel.service;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.io.*;
import java.text.SimpleDateFormat;
import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import com.ruubel.repository.*;
import com.ruubel.utils.DataRow;
import com.ruubel.utils.DataTable;
import com.ruubel.utils.ExcelTable;
import com.ruubel.model.*;


@Service
public class CompagniaService {

    private final static Logger logger = LoggerFactory.getLogger(CompagniaService.class);

    @Autowired
    private CompagniaRepo compagniaRepository;
	
    
    public Page<Compagnia> findAll(String ragioneSociale, Pageable pageable){
        return this.compagniaRepository.findAll(new Specification<Compagnia>() {
            @Override
            public Predicate toPredicate(Root<Compagnia> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                try {
                    if (ragioneSociale != null) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("ragioneSociale")), ragioneSociale.toLowerCase()));
                    }       
                } catch (Exception ex) {
              	  logger.error(ex.toString());
                }
                
                query.orderBy(criteriaBuilder.desc(root.get("ragioneSociale")));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
  	}

    public Compagnia save(Compagnia compagnia) throws Exception {
    	// update the company
    	Compagnia compagniaSaved = compagniaRepository.save(compagnia);

 

		return compagniaSaved;
    }

    public void delete(Integer id) throws Exception {
    	Compagnia compagnia = compagniaRepository.findOneById(id);
    
				
		//compagniaSettingRepository.deleteById(id);
		
		compagniaRepository.deleteById(id);
    }

    public void saveFromFile(String filepath, String token) throws Exception {
    }

    public CompagniaRepo getRepository() {
        return compagniaRepository;
    }

}