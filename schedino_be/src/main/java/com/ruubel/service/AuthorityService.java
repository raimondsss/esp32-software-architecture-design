package com.ruubel.service;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruubel.model.Authority;
import com.ruubel.model.PasswordResetToken;
import com.ruubel.model.UserLocation;
import com.ruubel.model.VerificationToken;
import com.ruubel.dto.RoleDto;
import com.ruubel.error.UserAlreadyExistException;
import com.ruubel.repository.AuthorityRepository;
import com.ruubel.repository.PasswordResetTokenRepository;
import com.ruubel.repository.UserLocationRepository;
import com.ruubel.repository.VerificationTokenRepository;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;


@Transactional
@Service(value = "authorityService")
public class AuthorityService {

	private static final Logger log = LoggerFactory.getLogger(AuthorityService.class);

    @Autowired
    private AuthorityRepository authorityRepository;


	public List<Authority> findAll() {
        List<Authority> authorities = new ArrayList<>();
        authorityRepository.findAll().iterator().forEachRemaining(authority -> authorities.add(authority));
        return authorities;
    }

    public Authority findOne(long id) {
        return authorityRepository.findById(id).get();
    }

    public void delete(long id) {
    	authorityRepository.deleteById(id);
    }

    public RoleDto find(String name) {
    	Authority authority = authorityRepository.findByAuthority(name);
    	
    	RoleDto role = new RoleDto();
    	role.setId(authority.getId());
    	role.setName(authority.getAuthority());
        return role;
    }

    public RoleDto save(RoleDto roleDto) {
    	Authority authority = authorityRepository.findByAuthority(roleDto.getName());
        if(authority != null) {
            log.error(String.format("Duplicate role %s", roleDto.getName()));
            throw new RuntimeException("Duplicate role.");
        }
        
        Authority auth = new Authority();
        auth.setAuthority(roleDto.getName());        
        authorityRepository.save(auth);

        return roleDto;
    }
    
    public RoleDto update(RoleDto roleDto) {
    	Authority authority = authorityRepository.findByAuthority(roleDto.getName());
        if(authority == null) {
            log.error(String.format("Role not fond %s", roleDto.getName()));
            throw new RuntimeException("Role not found.");
        }
        
        authority.setId(roleDto.getId());
        authority.setAuthority(roleDto.getName());        
        authorityRepository.save(authority);
        
        return roleDto;
    }
    

}
