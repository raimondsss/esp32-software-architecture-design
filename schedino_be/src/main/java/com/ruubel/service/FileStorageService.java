package com.ruubel.service;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
 
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class FileStorageService {
	
	Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private final Path rootLocation = Paths.get("filestorage");
    //private final Path rootLocation;
 
    /*
    @Autowired
    public FileStorageService(FileStorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());

        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }
    */

    public FileStorageService() {
        try {
            if (!Files.exists(rootLocation)) {
                Files.createDirectory(rootLocation);
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }
    
	private Path getRootLocation(String prefix, String subPrefix) {
		String thePath = this.rootLocation.toString();

        try {
        	if (!prefix.equals("")) {
        		thePath += File.separator + prefix;

        		if (!Files.exists(Paths.get(thePath)))
                    Files.createDirectory(Paths.get(thePath));
        	}
        	if (!subPrefix.equals("")) {        		
        		thePath += File.separator + subPrefix;

        		if (!Files.exists(Paths.get(thePath)))
                    Files.createDirectory(Paths.get(thePath));        		
        	}
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize file storage!");
        }
        
		return Paths.get(thePath);
	}

    public void store(MultipartFile file){
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
            log.info("*********** Storing file {}", fileName);
            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
        	throw new RuntimeException("FAIL! -> message = " + e.getMessage());
        }
	}
	
    public Path store(MultipartFile file, String dir, String subDir){
    	Path storedPath;
    	
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
            log.info("*********** Storing file {}", fileName);
            storedPath = getRootLocation(dir, subDir).resolve(file.getOriginalFilename());
            Files.copy(file.getInputStream(), storedPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
        	throw new RuntimeException("FAIL! -> message = " + e.getMessage());
        }
		
		return storedPath;
	}
	
    public void store(byte[] data, String fileName){
		try {
            log.info("*********** Storing file {}", fileName);
            OutputStream out = new FileOutputStream(this.rootLocation.resolve(fileName).toString());
            out.write(data);
            out.close();
        } catch (Exception e) {
        	throw new RuntimeException("FAIL! -> message = " + e.getMessage());
        }
	}

    public Path loadFile(String filename) {
        return rootLocation.resolve(filename);
    }

    public Resource loadFileAsResource(String rootPath, String filename) {
        try {
            Path rootLocation = Paths.get(rootPath);
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }else{
            	throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
        	throw new RuntimeException("Error! -> message = " + e.getMessage());
        }
    }
    
    public Resource loadFileAsResource(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }else{
            	throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
        	throw new RuntimeException("Error! -> message = " + e.getMessage());
        }
    }
    
    public Resource loadFileAsResource(String filename, String dir, String subDir) {
        try {
            Path file = getRootLocation(dir, subDir).resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }else{
            	throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
        	throw new RuntimeException("Error! -> message = " + e.getMessage());
        }
    }
    
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }
 
    public void delete(String filename, String dir, String subDir) {
        Path filePath = getRootLocation(dir, subDir).resolve(filename);
    	
        filePath.toFile().delete();
    }
 
    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }
 
	public Stream<Path> loadFiles() {
        try {
            return Files.walk(this.rootLocation, 1)
                .filter(path -> !path.equals(this.rootLocation))
                .map(this.rootLocation::relativize);
        }
        catch (IOException e) {
        	throw new RuntimeException("\"Failed to read stored file");
        }
	}
	
	public String getRelPath(Path thePath) {
		return thePath.toString().substring(thePath.toString().indexOf(rootLocation.toString())+rootLocation.toString().length());
	}
}