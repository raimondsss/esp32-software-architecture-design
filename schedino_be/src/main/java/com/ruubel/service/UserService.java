package com.ruubel.service;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruubel.model.Authority;
import com.ruubel.model.Credentials;
import com.ruubel.model.PasswordResetToken;
import com.ruubel.model.Schedino;
import com.ruubel.model.User;
import com.ruubel.model.VerificationToken;
import com.ruubel.dto.UserDto;
import com.ruubel.error.UserAlreadyExistException;
import com.ruubel.repository.AuthorityRepository;
import com.ruubel.repository.CredentialRepository;
import com.ruubel.repository.PasswordResetTokenRepository;
import com.ruubel.repository.SchedinoRepo;
import com.ruubel.repository.UserLocationRepository;
import com.ruubel.repository.VerificationTokenRepository;
import com.ruubel.service.UserService;
import com.ruubel.utils.DataRow;
import com.ruubel.utils.DataTable;
import com.ruubel.utils.ExcelTable;


@Transactional
@Service(value = "userService")
public class UserService {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);

    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";

    @Autowired
    private CredentialRepository credentialRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private VerificationTokenRepository tokenRepository;
    
    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Autowired
    private UserLocationRepository userLocationRepository;

    @Autowired
    private EmailServiceProducer emailServiceProducer;
    
    @Autowired
    private SchedinoRepo schedinoRepository;
    
    @Autowired
    private SchedinoService schedinoService;

    
	@Value("${qr.prefix}")
    private String QRPrefix;

	@Value("${app.name}")
    private String AppName;

	
	public List<Credentials> findAll() {
        List<Credentials> users = new ArrayList<>();
        credentialRepository.findAll().iterator().forEachRemaining(user -> users.add(user));
        return users;
    }

	public List<Credentials> findAllByOrganizationId(Integer organizationId) {
        List<Credentials> users = new ArrayList<>();
        credentialRepository.findAllByOrganizationId(organizationId).iterator().forEachRemaining(user -> users.add(user));
        return users;
    }

	public List<Credentials> findAllByOrganizationIdAndUsername(Integer organizationId, String username) {
        List<Credentials> users = new ArrayList<>();
        credentialRepository.findAllByOrganizationIdAndUsername(organizationId, username).iterator().forEachRemaining(user -> users.add(user));
        return users;
    }

	public List<Credentials> findAllByOrganizationIdAndRole(Integer organizationId, String role) {
        return credentialRepository.findAll(new Specification<Credentials>() {
            @Override
            public Predicate toPredicate(Root<Credentials> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                try {
                    if (organizationId > 0) {
                        predicates.add(criteriaBuilder.equal(root.get("organizationId"), organizationId));
                    }
                    if (role != null) {
                        predicates.add(criteriaBuilder.equal(root.join("authorities").get("authority"), role));
                    }       
                } catch (Exception ex) {
                	log.error(ex.toString());
                }
                
                List<Order> orderList = new ArrayList();
                orderList.add(criteriaBuilder.desc(root.get("username")));
                query.orderBy(orderList);

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });		
    }
	
	public List<Credentials> findAllAssociati() {
        return credentialRepository.findAll(new Specification<Credentials>() {
            @Override
            public Predicate toPredicate(Root<Credentials> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                try {
                	predicates.add(criteriaBuilder.isNotNull(root.get("schedino").get("id")));                          
                } catch (Exception ex) {
                	log.error(ex.toString());
                }
                
                List<Order> orderList = new ArrayList();
                orderList.add(criteriaBuilder.desc(root.get("schedino").get("id")));
                query.orderBy(orderList);

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });		
    }

    public Credentials findByUsername(String username) {
        return credentialRepository.findByUsername(username);
    }

    public UserDto findOne(long id) {
		Credentials credential = credentialRepository.findOneById(id);

		UserDto user = new UserDto();
        user.setId(credential.getId());
        user.setUsername(credential.getUsername());
        user.setPassword(credential.getPassword());
        user.setEmail(credential.getEmail());
        user.setFirstName(credential.getFirstName());
        user.setLastName(credential.getLastName());
        user.setOrganizationId(credential.getOrganizationId());
        user.setChangePassword(credential.isChangePassword());
        user.setPrivacy(credential.isPrivacy());
        user.setEnabled(credential.isEnabled());
        user.setLandingPage(credential.getLandingPage());
        user.setRole(new ArrayList<String>());
        for (Authority authority : credential.getAuthorities()) {
        	user.getRole().add(authority.getAuthority());
        }
        user.setObbiettivoGiornaliero(credential.getObbiettivoGiornaliero());
        if(user.getRole().get(0).equals("ROLE_USER")) {
        	user.setSchedino(credential.getSchedino().getCodice());
        }
        	
        return user;
    }

    public boolean existsById(long id) {
    	return credentialRepository.existsById(id);
    }

    public void delete(long id) {
    	credentialRepository.deleteById(id);
    }

    public UserDto find(String username) {
        Credentials credential = credentialRepository.findByUsername(username);
    	
        UserDto user = new UserDto();
        user.setId(credential.getId());
        user.setUsername(credential.getUsername());
        user.setPassword(credential.getPassword());
        user.setEmail(credential.getEmail());
        user.setFirstName(credential.getFirstName());
        user.setLastName(credential.getLastName());
        user.setOrganizationId(credential.getOrganizationId());
        user.setChangePassword(credential.isChangePassword());
        user.setPrivacy(credential.isPrivacy());
        user.setEnabled(credential.isEnabled());
        user.setAvatar(credential.getAvatar());
        user.setLandingPage(credential.getLandingPage());
        user.setRole(new ArrayList<String>());
        for (Authority authority : credential.getAuthorities()) {
        	user.getRole().add(authority.getAuthority());
        	user.setRoleDescription(authority.getDescription());
        }
        	
        return user;
    }

    public UserDto save(UserDto userDto, boolean update, Locale locale) {
    	if (!update) {
            Credentials userWithDuplicateUsername = credentialRepository.findByUsername(userDto.getUsername());
            if(userWithDuplicateUsername != null && userDto.getId() != userWithDuplicateUsername.getId()) {
                log.error(String.format("Duplicate username %s", userDto.getUsername()));
                throw new RuntimeException("Duplicate username.");
            }
            //Credentials userWithDuplicateEmail = credentialRepository.findByEmail(userDto.getEmail());
            //if(userWithDuplicateEmail != null && userDto.getId() != userWithDuplicateEmail.getId()) {
            List<Credentials> userWithDuplicateEmail = credentialRepository.findAllByEmail(userDto.getEmail());
            if(!userWithDuplicateEmail.isEmpty()) {
                log.error(String.format("Duplicate email %s", userDto.getEmail()));
                throw new RuntimeException("Duplicate email.");
            }    		
    	} else {
            Credentials userWithUsername = credentialRepository.findByUsername(userDto.getUsername());
            if(userWithUsername == null) {
                log.error(String.format("Username %s not found", userDto.getUsername()));
                throw new RuntimeException("Username not found.");
            }
    		
    	}
        if(userDto.getRole() == null || userDto.getRole().isEmpty()) {
            log.error(String.format("No role"));
            throw new RuntimeException("No role.");
        }

        Credentials user = new Credentials();
        if (update) {
        	user = credentialRepository.findByUsername(userDto.getUsername());        	
        }
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setObbiettivoGiornaliero(userDto.getObbiettivoGiornaliero());
        if(userDto.getRole().get(0).equals("ROLE_USER")) {
        	Schedino schedino = schedinoRepository.findOneById(user.getSchedino().getId());
        	schedino.setCodice(userDto.getSchedino());
        	schedinoRepository.save(schedino);
        }
        
        user.setOrganizationId(userDto.getOrganizationId());
        if (!update)
        	user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setLandingPage(userDto.getLandingPage());
        user.setEnabled(userDto.isEnabled());
        
        List<String> roleTypes = new ArrayList<String>();
        for (String role : userDto.getRole()) {
        	if (role != null) roleTypes.add(role);
        }
        //userDto.getRole().stream().map(role -> roleTypes.add(role));
        user.setAuthorities(authorityRepository.find(roleTypes));
        
        credentialRepository.save(user);
        
        // send credential via email
       /* if (user.getEmail() != null && !user.getEmail().equals("") && !update) {
        	log.info("Sending credential to email {}", user.getEmail());
        	
    		emailServiceProducer.sendAccountRegistrationEmail(userDto, locale);
        }*/

        return userDto;
    }
    
    public UserDto update(UserDto userDto) {
        Credentials user = credentialRepository.findByUsername(userDto.getUsername());
        if(user == null) {
            log.error(String.format("User not fond %s", userDto.getUsername()));
            throw new RuntimeException("User not found.");
        }
        
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setAvatar(userDto.getAvatar());
        user.setPrivacy(userDto.isPrivacy());
        user.setPrivacyDate(userDto.getPrivacyDate());
        
        credentialRepository.save(user);
        return userDto;
    }
    
    public Credentials registerNewAccount(final UserDto accountDto) throws UserAlreadyExistException {
        if (emailExists(accountDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: " + accountDto.getEmail());
        }
        
        final Credentials user = new Credentials();
        user.setUsername(accountDto.getUsername());
        user.setFirstName(accountDto.getFirstName());
        user.setLastName(accountDto.getLastName());
        user.setOrganizationId(accountDto.getOrganizationId());
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        user.setUsing2FA(accountDto.isUsing2FA());
        user.setEnabled(accountDto.isEnabled());
        if (accountDto.getRole() != null && !accountDto.getRole().isEmpty())
        	user.setAuthorities(Arrays.asList(authorityRepository.findByAuthority(accountDto.getRole().get(0))));
        else
            user.setAuthorities(Arrays.asList(authorityRepository.findByAuthority("ROLE_CMS_ADMIN")));
        user.setLoginType(Credentials.LOGIN_EMAIL);
        
        return credentialRepository.save(user);
    }

    public Credentials registerNewUserAccount(final UserDto accountDto) throws UserAlreadyExistException {
        if (emailExists(accountDto.getEmail())) {
            throw new UserAlreadyExistException("Registrazione impossibile: email " + accountDto.getEmail() + " già presente!");
        }
        
        final Credentials user = new Credentials();
        user.setUsername(accountDto.getUsername());
        user.setFirstName(accountDto.getFirstName());
        user.setLastName(accountDto.getLastName());
        user.setObbiettivoGiornaliero(accountDto.getObbiettivoGiornaliero());
        //Schedino schedinoLast = schedinoRepository.findOneByMaxId();
        Schedino schedino = new Schedino();
        schedino.setCodice(accountDto.getSchedino());
        schedino.setDescrizione("bilancia");
        Schedino schedinoSaved = schedinoService.save(schedino);
        schedinoSaved.setNome("Smart Scale "+schedinoSaved.getId());
        schedinoService.save(schedinoSaved);
        user.setSchedino(schedinoSaved);
        user.setOrganizationId(1);
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        user.setEnabled(true);
        user.setUsing2FA(accountDto.isUsing2FA());
        user.setAuthorities(Arrays.asList(authorityRepository.findByAuthority("ROLE_USER")));
        user.setLoginType(Credentials.LOGIN_EMAIL);
        
        return credentialRepository.save(user);
    }

    public Credentials getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = null;
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        Credentials user = null;
        if (currentUserName != null) {
            user = findByUsername(currentUserName);
        }
        return user;
    }

    public Credentials getUser(final String verificationToken) {
        final VerificationToken token = tokenRepository.findByToken(verificationToken);
        if (token != null) {
            return token.getUser();
        }
        return null;
    }

    public VerificationToken getVerificationToken(final String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    public void saveRegisteredUser(final Credentials user) {
    	credentialRepository.save(user);
    }

    public void deleteUser(final Credentials user) {
        final VerificationToken verificationToken = tokenRepository.findByUser(user);

        if (verificationToken != null) {
            tokenRepository.delete(verificationToken);
        }

        final PasswordResetToken passwordToken = passwordTokenRepository.findByUser(user);

        if (passwordToken != null) {
            passwordTokenRepository.delete(passwordToken);
        }

        credentialRepository.delete(user);
    }
    
    public void createVerificationTokenForUser(final Credentials user, final String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }    
    
    public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
        VerificationToken vToken = tokenRepository.findByToken(existingVerificationToken);
        vToken.updateToken(UUID.randomUUID().toString());
        vToken = tokenRepository.save(vToken);
        return vToken;
    }
    
    public void createPasswordResetTokenForUser(final Credentials user, final String token) {
        final PasswordResetToken myToken = new PasswordResetToken(token, user);
        passwordTokenRepository.save(myToken);
        user.setChangePassword(true);
        credentialRepository.save(user);
    }

    public Credentials findUserByEmail(final String email) {
        return credentialRepository.findByEmail(email);
    }

    public Credentials findUserByUsername(final String username) {
        return credentialRepository.findByUsername(username);
    }

    public PasswordResetToken getPasswordResetToken(final String token) {
        return passwordTokenRepository.findByToken(token);
    }

    public Optional<Credentials> getUserByPasswordResetToken(final String token) {
        return Optional.ofNullable(passwordTokenRepository.findByToken(token) .getUser());
    }

    public Optional<Credentials> getUserByID(final long id) {
        return credentialRepository.findById(id);
    }

    public void resetUserPassword(final Credentials user, final String password) {
        user.setPassword(passwordEncoder.encode(password));
        user.setChangePassword(true);
        user.setPrivacy(false);
        credentialRepository.save(user);
    }

    public void changeUserPassword(final Credentials user, final String password) {
        user.setPassword(passwordEncoder.encode(password));
        user.setChangePassword(false);
        user.setPrivacy(true);
        credentialRepository.save(user);
    }

    public boolean checkIfValidOldPassword(final Credentials user, final String oldPassword) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }

    public String validateVerificationToken(String token) {
        final VerificationToken verificationToken = tokenRepository.findByToken(token);
        if (verificationToken == null) {
            return TOKEN_INVALID;
        }

        final Credentials user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            tokenRepository.delete(verificationToken);
            return TOKEN_EXPIRED;
        }

        user.setEnabled(true);
        // tokenRepository.delete(verificationToken);
        credentialRepository.save(user);
        return TOKEN_VALID;
    }
    
    public String generateQRUrl(Credentials user) throws UnsupportedEncodingException {
        return QRPrefix + URLEncoder.encode(String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", AppName, user.getEmail(), user.getSecret(), AppName), "UTF-8");
    }

    public Credentials updateUser2FA(boolean use2FA) {
        final Authentication curAuth = SecurityContextHolder.getContext().getAuthentication();
        Credentials currentUser = (Credentials) curAuth.getPrincipal();
        currentUser.setUsing2FA(use2FA);
        currentUser = credentialRepository.save(currentUser);
        final Authentication auth = new UsernamePasswordAuthenticationToken(currentUser, currentUser.getPassword(), curAuth.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return currentUser;
    }

    public List<String> getUsersFromSessionRegistry() {
        return sessionRegistry.getAllPrincipals()
            .stream()
            .filter((u) -> !sessionRegistry.getAllSessions(u, false)
                .isEmpty())
            .map(o -> {
                if (o instanceof Credentials) {
                    return ((Credentials) o).getEmail();
                } else {
                    return o.toString()
            ;
                }
            }).collect(Collectors.toList());
    }

    public void saveFromFile(String filepath, String token) throws Exception {
        final FileInputStream inputStream = new FileInputStream(filepath);
        DataTable table = ExcelTable.load(() -> inputStream);

        int rowCount = table.rowCount();

        for(int i=0; i < rowCount; ++i) {
            DataRow row = table.row(i);

            Credentials user = new Credentials();
            user.setUsername(row.cell("username"));
            user.setEmail(row.cell("email"));
            user.setFirstName(row.cell("firstname"));
            user.setLastName(row.cell("lastname"));
            user.setOrganizationId((int)Double.parseDouble(row.cell("organizationid")));
            user.setPassword(passwordEncoder.encode(row.cell("password")));
            
            List<String> roleTypes = new ArrayList<>();
            roleTypes.add(row.cell("role"));
            user.setAuthorities(authorityRepository.find(roleTypes));
            
            log.info("Saving user: {}", user.getUsername());
            credentialRepository.save(user);

        }

    }
 
    private boolean emailExists(final String email) {
        return credentialRepository.findByEmail(email) != null;
    }

	@Autowired
	private DefaultOAuth2RequestFactory defaultOAuth2RequestFactory;
	
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthorizationServerTokenServices tokenServices;

    public OAuth2AccessToken getToken(String username, String password) {
	    HashMap<String, String> parameters = new HashMap<String, String>();
	    parameters.put("client_id", "app_client");
	    parameters.put("client_secret", "user");
	    parameters.put("scope", "read,write");
	    parameters.put("grant_type", "password");
	    parameters.put("password", username);
	    parameters.put("username", password);

	    AuthorizationRequest authorizationRequest = defaultOAuth2RequestFactory.createAuthorizationRequest(parameters);
	    authorizationRequest.setApproved(true);

	    OAuth2Request oauth2Request = defaultOAuth2RequestFactory.createOAuth2Request(authorizationRequest);
	    // Create principal and auth token
	    final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(
	            username, password);
	    Authentication authentication = authenticationManager.authenticate(loginToken);

	    OAuth2Authentication authenticationRequest = new OAuth2Authentication(oauth2Request, authentication);
	    authenticationRequest.setAuthenticated(true);

	    OAuth2AccessToken accessToken = tokenServices.createAccessToken(authenticationRequest);

	    return accessToken;
	}

}
