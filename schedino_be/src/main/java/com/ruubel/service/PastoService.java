package com.ruubel.service;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.Mockito.CALLS_REAL_METHODS;

import java.io.*;
import java.text.SimpleDateFormat;
import com.google.common.base.Strings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import com.ruubel.repository.*;
import com.ruubel.utils.DataRow;
import com.ruubel.utils.DataTable;
import com.ruubel.utils.ExcelTable;
import com.ruubel.model.*;


@Service
public class PastoService {

    private final static Logger logger = LoggerFactory.getLogger(PastoService.class);

    @Autowired
    private PastoRepo pastoRepository;   
    
    @Autowired private EntityManager entityManager;

    
    public Page<Pasto> findAll(String codice, String nome, String data, Integer id, Pageable pageable){
        return this.pastoRepository.findAll(new Specification<Pasto>() {
            @Override
            public Predicate toPredicate(Root<Pasto> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                try {
                    if (codice != null) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("codice")), codice.toLowerCase()));
                    }    
                    if (nome != null) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("nome")), nome.toLowerCase()));
                    }    
                    if (data != null) {
                    	   Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data+" 00:00:00");
                           predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("data").as(Date.class), startTime));
                           
                           Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data+" 23:59:59");
                           predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("data").as(Date.class), endTime));
                    }
                    if (id != null) {
                        predicates.add(criteriaBuilder.equal(root.get("credential").get("id"), id));
                    }
                } catch (Exception ex) {
              	  logger.error(ex.toString());
                }
                
                query.orderBy(criteriaBuilder.desc(root.get("data")));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
  	}
    
    public List<Pasto> findAll(Integer userId, String startDate, String endDate){
        return this.pastoRepository.findAll(new Specification<Pasto>() {
            @Override
            public Predicate toPredicate(Root<Pasto> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                try { 
                    if (startDate != null) {
                    	   Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate+" 00:00:00");
                           predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("data").as(Date.class), startTime));
                                          
                    }
                    if (endDate != null) {
                        
                        Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate+" 23:59:59");
                        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("data").as(Date.class), endTime));
                 }
                    if (userId != null) {
                        predicates.add(criteriaBuilder.equal(root.get("credential").get("id"), userId));
                    }
                } catch (Exception ex) {
              	  logger.error(ex.toString());
                }
                
                query.orderBy(criteriaBuilder.desc(root.get("nome")));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });
  	}

    public Pasto save(Pasto pasto) throws Exception {
    
    	Pasto pastoSaved = pastoRepository.save(pasto);

		return pastoSaved;
    }
    

    public void delete(Integer id) throws Exception {
    	Pasto pasto = pastoRepository.findOneById(id);
		
		pastoRepository.deleteById(id);
    }
    
    
    /*public Double sumAll(Date data, Date startDate, Date endDate, Integer scoutId, Integer[] stato) {
		
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery query = criteriaBuilder.createQuery(Double.class);
		Root<Pasto> root = query.from(Pasto.class);
		
        List<Predicate> predicates = new ArrayList<>();
        try {
            if (scoutId != null) {
            	predicates.add(criteriaBuilder.equal(root.get("scout").get("id"), scoutId));
            }
            predicates.add(criteriaBuilder.equal(root.get("stato"), stato));
            if (data != null) {
          	  Date startTime = new SimpleDateFormat("ddMMyyyyHHmmss").parse(new SimpleDateFormat("ddMMyyyy").format(data)+"000000");
                Date endTime = new SimpleDateFormat("ddMMyyyyHHmmss").parse(new SimpleDateFormat("ddMMyyyy").format(data)+"235959");
                predicates.add(criteriaBuilder.between(root.get("data"), startTime, endTime));
            }
            if (startDate != null) {
          	  Date startTime = new SimpleDateFormat("ddMMyyyyHHmmss").parse(new SimpleDateFormat("ddMMyyyy").format(startDate)+"000000");
                predicates.add(criteriaBuilder.greaterThan(root.get("data"), startTime));
            }
            if (endDate != null) {
                Date endTime = new SimpleDateFormat("ddMMyyyyHHmmss").parse(new SimpleDateFormat("ddMMyyyy").format(endDate)+"235959");
                predicates.add(criteriaBuilder.lessThan(root.get("data"), endTime));
            }
      	  
            return ((Double) entityManager.createQuery(
            		query.where(predicates.toArray(new Predicate[predicates.size()]))
            		.select(criteriaBuilder.count(query.from(Pasto.class)))).getSingleResult());
        } catch (Exception ex) {
      	  logger.error(ex.toString());
        }
        
        return 0.0;
	}*/

    public void saveFromFile(String filepath, String token) throws Exception {
    }

    public PastoRepo getRepository() {
        return pastoRepository;
    }

}