package com.ruubel.service;

import java.util.Date;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruubel.model.Credentials;
import com.ruubel.model.Pasto;
import com.ruubel.model.Prodotto;
import com.ruubel.model.Schedino;
import com.ruubel.repository.CredentialRepository;
import com.ruubel.repository.PastoRepo;
import com.ruubel.repository.ProdottoRepo;
import com.ruubel.repository.SchedinoRepo;

@Service
public class MessageService {

	@Autowired
	private IMqttClient mqttClient;
	
    @Autowired
    private PastoRepo pastoRepository;   
    
    @Autowired
    private PastoService pastoService;
        
    @Autowired
    private ProdottoRepo prodottoRepository;
    
    @Autowired
    private SchedinoRepo schedinoRepository;
    
    @Autowired
    private CredentialRepository credentialRepository;  

	public void publish(final String topic, final String payload, int qos, boolean retained)
			throws MqttPersistenceException, MqttException {
		MqttMessage mqttMessage = new MqttMessage();
		mqttMessage.setPayload(payload.getBytes());
		mqttMessage.setQos(qos);
		mqttMessage.setRetained(retained);

		mqttClient.publish(topic, mqttMessage);
		
		//mqttClient.publish(topic, payload.getBytes(), qos, retained);

		mqttClient.disconnect();
	}

	public void mqttMessageRcvd(final String topic) throws MqttException, InterruptedException {
		System.out.println("Messages received:");

		mqttClient.subscribeWithResponse(topic, (tpic, msg) -> {
			System.out.println(msg.getId() + " -> " + new String(msg.getPayload()));
			
			//Aggiunta nella tabella mangiati
			
			String message = new String(msg.getPayload());
			String[] parts = message.split(",");
			String idSchedino = parts[0];
			String peso = parts[1];
			String calorie = parts[2];
			String carboidrati = parts[3];
			String grassi = parts[4];
			String proteine = parts[5];
			String nome = parts[6];
			
			System.out.println(nome + " proteine: " + proteine);
			 
			Prodotto prodotto = prodottoRepository.findOneByNome(nome);
			
			Schedino schedino = schedinoRepository.findOneByCodice(idSchedino);
			
			Credentials credential = credentialRepository.findOneBySchedinoId(schedino.getId());
			
			Pasto pasto = new Pasto();
			pasto.setNome(nome);
			pasto.setCalorie(Double.parseDouble(calorie));
			pasto.setCarboidrati(carboidrati);
			pasto.setGrassi(grassi);
			pasto.setProteine(proteine);
			pasto.setCodice(prodotto.getCodice());
			pasto.setPeso(peso);
			pasto.setProdotti(prodotto);
			pasto.setCredential(credential);
			pasto.setData(new Date());
			
			this.pastoService.save(pasto);
			
			System.out.println(nome);
			
		});
	}

}