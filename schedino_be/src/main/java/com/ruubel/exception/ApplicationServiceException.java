package com.ruubel.exception;

public class ApplicationServiceException extends Exception {

	private static final long serialVersionUID = 5899373342506604713L;

	public ApplicationServiceException() {
		super();
	}

	public ApplicationServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ApplicationServiceException(String arg0) {
		super(arg0);
	}

	public ApplicationServiceException(Throwable arg0) {
		super(arg0);
	}

}
