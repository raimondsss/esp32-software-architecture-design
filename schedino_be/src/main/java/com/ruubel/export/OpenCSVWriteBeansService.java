package com.ruubel.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVWriter;
import com.opencsv.bean.BeanToCsv;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;


public class OpenCSVWriteBeansService {

	static final Logger logger = LoggerFactory.getLogger(OpenCSVWriteBeansService.class);
	
	/*
	public static void exportTreatment(List<TreatmentBean> beansToExport, PrintWriter writer) throws RuntimeServiceException {
	
		try {
			//ColumnPositionMappingStrategy<TreatmentBean> mappingStrategy = 
			//		new ColumnPositionMappingStrategy<TreatmentBean>();

			ColumnPositionWithHeaderMappingStrategy<TreatmentBean> mappingStrategy = 
					new ColumnPositionWithHeaderMappingStrategy<TreatmentBean>(TreatmentBean.CSV_HEADER);			 
			mappingStrategy.setType(TreatmentBean.class);
			mappingStrategy.setColumnMapping(TreatmentBean.CSV_HEADER);
			
            StatefulBeanToCsv<TreatmentBean> beanToCsv = new StatefulBeanToCsvBuilder(writer)
            		.withMappingStrategy(mappingStrategy)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
//                    .withSeparator(',')
                    .build();
                    
            beanToCsv.write(beansToExport);

        } catch (Exception ex) {
			LogTools.logException(logger, "export", ex);
			throw new ApplicationServiceException("A problem occurred while export beans list list.");
        	
        }
	}

	public static void exportDataBreach(List<DataBreachBean> beansToExport, PrintWriter writer) throws RuntimeServiceException {
		
		try {
			ColumnPositionWithHeaderMappingStrategy<DataBreachBean> mappingStrategy = 
					new ColumnPositionWithHeaderMappingStrategy<DataBreachBean>(DataBreachBean.CSV_HEADER);			 
			mappingStrategy.setType(DataBreachBean.class);
			mappingStrategy.setColumnMapping(TreatmentBean.CSV_HEADER);
			
            StatefulBeanToCsv<DataBreachBean> beanToCsv = new StatefulBeanToCsvBuilder(writer)
            		.withMappingStrategy(mappingStrategy)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(beansToExport);

        } catch (Exception ex) {
			LogTools.logException(logger, "export", ex);
			throw new ApplicationServiceException("A problem occurred while export beans list list.");
        	
        }
	}
	*/
}
