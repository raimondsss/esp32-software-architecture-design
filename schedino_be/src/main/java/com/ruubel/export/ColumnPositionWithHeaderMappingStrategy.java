package com.ruubel.export;

import com.opencsv.bean.ColumnPositionMappingStrategy;

class ColumnPositionWithHeaderMappingStrategy<T> extends ColumnPositionMappingStrategy<T> {

	String[] header;
	
	public ColumnPositionWithHeaderMappingStrategy(String[] header) {
		//this.header = header;
		this.header = new String[header.length];
		for (int i = 0; i < header.length; i++) {
            this.header[i] = header[i].toUpperCase();
        }
	}
 
	@Override
    public String[] generateHeader() {
		return header;
    }

}
