package com.ruubel.model;

import lombok.*;
import java.util.*;
import javax.persistence.*;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.JoinFormula;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.swagger.annotations.ApiModelProperty;

@Data
@Entity
@Table(name = "pasti")
public class Pasto{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
    private String nome;
    private String codice;
    private Double calorie;
    private String carboidrati;
    private String grassi;
    private String proteine;
    private String peso;
    private Date data;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "prodotto_id", nullable = false)
    private Prodotto prodotti;
       
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "credentials_id", nullable = false)
    private Credentials credential;
      
	
    public Pasto(){

    }

    public Pasto(Integer id){
    	this.id = id;
    }
    
    
}
