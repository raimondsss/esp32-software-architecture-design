package com.ruubel.model;

import lombok.*;
import java.util.*;
import javax.persistence.*;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.JoinFormula;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.swagger.annotations.ApiModelProperty;

@Data
@Entity
@Table(name = "schedini")
public class Schedino{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
    private String nome;
    private String codice;
    private String descrizione;
      
    @Formula("(select count(p.id) from credentials p where p.schedino_id = id)")
    private Long utenti;
	
    public Schedino(){

    }

    public Schedino(Integer id){
    	this.id = id;
    }
    
    
}
