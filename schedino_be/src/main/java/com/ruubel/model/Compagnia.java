package com.ruubel.model;

import lombok.*;
import java.util.*;
import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.swagger.annotations.ApiModelProperty;

@Data
@Entity
@Table(name = "compagnie")
public class Compagnia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id; 
    private String ragioneSociale;
    private String indirizzo;
    private String localita;
    private String cap;
    private String provincia;
    private String nazione;
    private String partitaIva;
    private String logo;

    public Compagnia(){
    
    }

    public Compagnia(Integer id){
        this.id = id;
    }
}
