package com.ruubel.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.jboss.aerogear.security.otp.api.Base32;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by ahmed on 21.5.18.
 */

@Entity
@Table(name ="credentials")
public class Credentials implements Serializable {

	public static final String LOGIN_EMAIL="email";
	public static final String LOGIN_GOOGLE="google";
	public static final String LOGIN_FACEBOOK="facebook";
	public static final String LOGIN_TWITTER="twitter";
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @NotEmpty
    private String username;
    
    @NotEmpty
    private String password;

    private String email;
    
    private String firstName;

    private String lastName;
    
    private String address;
    
    private String city;
    
    private Date   birthDate;
    
    private String birthLoc;

    private Integer organizationId;
    
    private boolean enabled;

    private boolean isUsing2FA;

    private String secret;
    
    private String loginType;
    
    private boolean changePassword;
    
    private String avatar;

    private String userId;

    private String token;

    private String landingPage;
    
    private boolean privacy;
    
    private Date   	privacyDate;
    
    private Integer obbiettivoGiornaliero;

    
    @ManyToMany(fetch = FetchType.EAGER)
    //@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<Authority> authorities;

    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "schedino_id", nullable = false)
    private Schedino schedino;
    
    public Credentials() {
        super();
        this.secret = Base32.random();
        this.enabled = false;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isUsing2FA() {
        return isUsing2FA;
    }

    public void setUsing2FA(boolean isUsing2FA) {
        this.isUsing2FA = isUsing2FA;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
    
    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthLoc() {
		return birthLoc;
	}

	public void setBirthLoc(String birthLoc) {
		this.birthLoc = birthLoc;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public boolean isChangePassword() {
		return changePassword;
	}

	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	public boolean isPrivacy() {
		return privacy;
	}

	public void setPrivacy(boolean privacy) {
		this.privacy = privacy;
	}

	public Date getPrivacyDate() {
		return privacyDate;
	}

	public void setPrivacyDate(Date privacyDate) {
		this.privacyDate = privacyDate;
	}
	
	public Integer getObbiettivoGiornaliero() {
		return obbiettivoGiornaliero;
	}

	public void setObbiettivoGiornaliero(Integer obbiettivoGiornaliero) {
		this.obbiettivoGiornaliero = obbiettivoGiornaliero;
	}

	public Schedino getSchedino() {
		return schedino;
	}

	public void setSchedino(Schedino schedino) {
		this.schedino = schedino;
	}

}
