package com.ruubel.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "email_queue")
public class EmailQueue {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	private Integer templateId;

	private String renderedSubject;

	private String renderedTemplate;

	private String toAddress;

	private String toList;

	private String fromAddress;

	private String replyTo;

	private String ccs;

	private String bccs;

	private String authToken;

	private boolean sent;

	private Date sentDate;

	private int retry;

	private Date lastRetryDate;

	private Date insertDate;

	private int priority;

	private String attachments;
}
