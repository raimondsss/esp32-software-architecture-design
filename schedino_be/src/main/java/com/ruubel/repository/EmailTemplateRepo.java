package com.ruubel.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.*;

import com.ruubel.model.*;

public interface EmailTemplateRepo extends JpaRepository<EmailTemplate, Integer> {
    public List<EmailTemplate> findAll();
    EmailTemplate save(EmailTemplate c);
    EmailTemplate findOneById(Integer id);
    EmailTemplate findOneByCodeAndLanguage(String code, String language);
    void deleteById(Integer id);
    boolean existsById(Integer id);

}

