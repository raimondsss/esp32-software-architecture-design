package com.ruubel.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.*;

import com.ruubel.model.*;

public interface EmailQueueRepo extends JpaRepository<EmailQueue, Integer> {
    public List<EmailQueue> findAll();
    EmailQueue save(EmailQueue c);
    EmailQueue findOneById(Integer id);
    void deleteById(Integer id);
    boolean existsById(Integer id);

}

