package com.ruubel.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.*;

import com.ruubel.model.*;

public interface SchedinoRepo extends JpaRepository<Schedino, Integer>, JpaSpecificationExecutor<Schedino> {
    public List<Schedino> findAll();
    public Page<Schedino> findAll(Pageable p);
    Schedino save(Schedino s);
    Schedino findOneById(Integer id);
    Schedino findOneByCodice(String codice);
    void deleteById(Integer id);
    boolean existsById(Integer id);

}

