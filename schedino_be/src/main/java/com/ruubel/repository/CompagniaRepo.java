package com.ruubel.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.*;

import com.ruubel.model.*;

public interface CompagniaRepo extends JpaRepository<Compagnia, Integer>, JpaSpecificationExecutor<Compagnia> {
    public List<Compagnia> findAll();
    public Page<Compagnia> findAll(Pageable p);
    Compagnia save(Compagnia c);
    Compagnia findOneById(Integer id);
    void deleteById(Integer id);
    boolean existsById(Integer id);

}

