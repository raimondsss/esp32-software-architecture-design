package com.ruubel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ruubel.model.Credentials;
import com.ruubel.model.UserLocation;

public interface UserLocationRepository extends JpaRepository<UserLocation, Long> {
    UserLocation findByCountryAndUser(String country, Credentials user);

}
