package com.ruubel.repository;

import com.ruubel.model.Credentials;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by ahmed on 21.5.18.
 */
public interface CredentialRepository extends JpaRepository<Credentials,Long>, JpaSpecificationExecutor<Credentials> {
	List<Credentials> findAllByOrganizationId(Integer organizationId);
	List<Credentials> findAllByOrganizationIdAndUsername(Integer organizationId, String username);
	List<Credentials> findAllByEmail(String email);
    Credentials findByUsername(String name);
    Credentials findByEmail(String email);
    Credentials findOneBySchedinoId(Integer schedinoId);
    Credentials findOneById(Long id);
}
