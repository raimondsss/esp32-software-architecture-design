package com.ruubel.repository;

import com.ruubel.model.Authority;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by ahmed on 21.5.18.
 */
public interface AuthorityRepository extends JpaRepository<Authority,Long> {
	Authority findByAuthority(String authority);

	@Query(value = "SELECT * FROM authority where authority IN (:roles)", nativeQuery = true)
    List<Authority> find(@Param("roles") List<String> roles);

    @Override
    void delete(Authority role);

}
