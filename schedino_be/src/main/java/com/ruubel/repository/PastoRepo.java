package com.ruubel.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.*;

import com.ruubel.model.*;

public interface PastoRepo extends JpaRepository<Pasto, Integer>, JpaSpecificationExecutor<Pasto> {
    public List<Pasto> findAll();
    public Page<Pasto> findAll(Pageable p);
    Pasto save(Pasto p);
    Pasto findOneById(Integer id);
    void deleteById(Integer id);
    boolean existsById(Integer id);

}

