package com.ruubel.repository;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;
import java.util.*;

import com.ruubel.model.*;

public interface ProdottoRepo extends JpaRepository<Prodotto, Integer>, JpaSpecificationExecutor<Prodotto> {
    public List<Prodotto> findAll();
    public Page<Prodotto> findAll(Pageable p);
    Prodotto save(Prodotto p);
    Prodotto findOneById(Integer id);
    Prodotto findOneByNome(String nome);
    Prodotto findOneByCodice(String codice);
    void deleteById(Integer id);
    boolean existsById(Integer id);

}

