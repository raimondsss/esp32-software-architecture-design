package com.ruubel.data;

import lombok.Data;

@Data
public class LongSingleSeries  {
    private String name;
    private Long value;

    
    public LongSingleSeries( String name, Long value){
        this.name  = name;
        this.value = value;
        
    }
}
