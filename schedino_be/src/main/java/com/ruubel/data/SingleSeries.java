package com.ruubel.data;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class SingleSeries  {
    private String name;
    private BigDecimal value;
    private Long longValue;

    public SingleSeries( String name, BigDecimal value){
        this.name  = name;
        this.value = value;
        
    }
    
    public SingleSeries( String name, Long value){
        this.name  = name;
        this.longValue = value;
        
    }
}
