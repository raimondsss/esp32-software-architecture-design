package com.ruubel.data;

import java.util.List;

import lombok.Data;

@Data
public class MultiSeries  {
    private String name;
    private List<SingleSeries> series;

    public MultiSeries(String name, List<SingleSeries> series){
        this.name  = name;
        this.series = series;
    }
}
