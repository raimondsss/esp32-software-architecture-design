package com.ruubel.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.web.client.RestTemplate;
import com.google.gson.*;

public class TimeUtils {

	public static int hourFromDecimal(double time) {
		return (int) time;
	}

	public static int minuteFromDecimal(double time) {
		return (int) (time * 60) % 60;
	}

	public static int secondFromDecimal(double time) {
		return (int) (time * (60*60)) % 60;
	}
	
	public static long daysBetween(Date startDate, Date endDate) {
		long startTime = startDate.getTime();
		long endTime = endDate.getTime();
		long diffTime = endTime - startTime;
		long diffDays = diffTime / (1000 * 60 * 60 * 24);
		return diffDays;
	}
	
	public static String getCurrentTimeZone(){
        //TimeZone tz = Calendar.getInstance().getTimeZone();
        TimeZone tz = TimeZone.getTimeZone("Europe/Rome");
        System.out.println(tz.getDisplayName());
        return tz.getID();
	}
	
	public static String convertToCurrentTimeZone(String Date) {
        String converted_date = "";
        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(Date);

            DateFormat currentTFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            currentTFormat.setTimeZone(TimeZone.getTimeZone(getCurrentTimeZone()));

            converted_date =  currentTFormat.format(date);
        }catch (Exception e){ e.printStackTrace();}

        return converted_date;
	}
	
	public static Date convertToCurrentTimeZone(Date date) {
        Date converted_date = null;
        try {

            DateFormat currentTFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            currentTFormat.setTimeZone(TimeZone.getTimeZone(getCurrentTimeZone()));

            String sdate =  currentTFormat.format(date);

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            converted_date = utcFormat.parse(sdate);
        }catch (Exception e){ e.printStackTrace();}

        return converted_date;
	}

	public static List<LocalDate> countBusinessDaysBetween(final LocalDate startDate,
	        final LocalDate endDate,
	        final Optional<List<LocalDate>> holidays) {
		
	    // Validate method arguments
	    if (startDate == null || endDate == null) {
	        throw new IllegalArgumentException("Invalid method argument(s) to countBusinessDaysBetween (" + startDate
	            + "," + endDate + "," + holidays + ")");
	    }

	    // Predicate 1: Is a given date is a holiday
	    Predicate<LocalDate> isHoliday = date -> holidays.isPresent() 
	            && holidays.get().contains(date);

	    // Predicate 2: Is a given date is a weekday
	    Predicate<LocalDate> isWeekend = date -> date.getDayOfWeek() == DayOfWeek.SATURDAY
	            || date.getDayOfWeek() == DayOfWeek.SUNDAY;

	    // Get all days between two dates
	    long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);

	    // Iterate over stream of all dates and check each day against any weekday or
	    // holiday
	    return Stream.iterate(startDate, date -> date.plusDays(1))
	            .limit(daysBetween)
	            .filter(isHoliday.or(isWeekend).negate())
	            .collect(Collectors.toList());
	}
	
	public static LocalDate calcDayAddBusiness(final LocalDate startDate, int days,
	        final Optional<List<LocalDate>> holidays) {
		
	    // Validate method arguments
	    if (startDate == null) {
	        throw new IllegalArgumentException("Invalid method argument(s) to calcDayAddBusiness (" + startDate
	            + "," + holidays + ")");
	    }

	    // Predicate 1: Is a given date is a holiday
	    Predicate<LocalDate> isHoliday = date -> holidays.isPresent() 
	            && holidays.get().contains(date);

	    // Predicate 2: Is a given date is a weekday
	    Predicate<LocalDate> isWeekend = date -> date.getDayOfWeek() == DayOfWeek.SATURDAY
	            || date.getDayOfWeek() == DayOfWeek.SUNDAY;

	    /*
	    // calc end date
	    Calendar c = Calendar.getInstance();
	    c.setTime(Date.from(startDate.atStartOfDay()
			      .atZone(ZoneId.systemDefault())
			      .toInstant()));
	    c.add(Calendar.DATE, days); 
	    LocalDate endDate = c.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	    
	    // Get all days between two dates
	    long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);

	    // Iterate over stream of all dates and check each day against any weekday or
	    // holiday
	    List<LocalDate> dates = Stream.iterate(startDate, date -> date.plusDays(1))
	            .limit(daysBetween)
	            .filter(isHoliday.or(isWeekend).negate())
	            .collect(Collectors.toList());
	    */
	    
	    List<LocalDate> dates = new ArrayList<LocalDate>();
	    LocalDate date = startDate;
	    int daysBetween = days;
	    while (daysBetween > 0) {
	    	date = date.plusDays(1);

	    	if ((holidays.isPresent() && holidays.get().contains(date)) || 
	    			(date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY)) {
	    		continue;
	    	}
	    	
	    	dates.add(date);
	    	daysBetween--;
	    }

	    if (!dates.isEmpty())
	    	return dates.get(dates.size()-1);
	    
	    return null;
	}
	
	public static List<PublicHoliday> getPublicHolidays(int year, String country) {
		
		List<PublicHoliday> publicHolidies = new ArrayList<PublicHoliday>();
		Gson gson = new Gson();
		
		String json = new RestTemplate().getForObject("https://date.nager.at/api/v3/publicholidays/"+year+"/"+country, String.class);
		JsonElement rootJsonElement = new JsonParser().parse(json);
		JsonArray publicHolidays = rootJsonElement.getAsJsonArray();
		Iterator<JsonElement> iterator = publicHolidays.iterator();
		while (iterator.hasNext()) {
			JsonElement publicHolidayJson = (JsonElement)iterator.next();
			
			PublicHoliday publicHoliday = gson.fromJson(publicHolidayJson, PublicHoliday.class);
			publicHolidies.add(publicHoliday);
			
	        //System.out.println(publicHoliday);
		}

		return publicHolidies;
	}

	public static List<LocalDate> publicHolidaysToLocalDates(List<PublicHoliday> publicHolidies) {
		List<LocalDate> localDates = new ArrayList<LocalDate>();

		for (PublicHoliday publicHoliday : publicHolidies) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate localDate = LocalDate.parse(publicHoliday.date, formatter);
			localDates.add(localDate);
		}

		return localDates;
	}

	public static Date calcNextBusinessDay(Date startDate, int daysBetween, int year, int numYears, String country) {
		
		LocalDate date = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		List<PublicHoliday> publicHolidies = new ArrayList<PublicHoliday>();
		for (int i=0;i < numYears;i++) {
			publicHolidies.addAll(getPublicHolidays(year+1, country));
			
		}
		LocalDate nextDate = calcDayAddBusiness(date, daysBetween, Optional.of(publicHolidaysToLocalDates(publicHolidies)));
		
		return Date.from(nextDate.atStartOfDay()
			      .atZone(ZoneId.systemDefault())
			      .toInstant());
	}

	public static void main(String[] args) {
		/*
	      String json = new RestTemplate().getForObject("https://date.nager.at/api/v3/publicholidays/2022/IT", String.class);
	      JsonElement rootJsonElement = new JsonParser().parse(json);
	      JsonArray publicHolidays = rootJsonElement.getAsJsonArray();
	      Iterator<JsonElement> iterator = publicHolidays.iterator();
	      while (iterator.hasNext()) {
	        JsonElement publicHoliday = (JsonElement)iterator.next();
	        System.out.println(publicHoliday);
	      }
	      */
	      
		try {
		      List<PublicHoliday> publicHolidies = getPublicHolidays(2022, "IT");
		      for (PublicHoliday publicHoliday : publicHolidies) {
		    	  System.out.println(publicHoliday.toString());
		      }
		      
		      Optional<List<LocalDate>> result = Optional.of(publicHolidaysToLocalDates(publicHolidies));	 	      
		      LocalDate nextDate = calcDayAddBusiness(LocalDate.of(2021, 12, 21), 45, result);
		      if (nextDate != null)
		    	  System.out.println(nextDate);

		      Date theDate = new SimpleDateFormat("dd/MM/yyyy").parse("21/12/2021");  
		      Date nextBusinessDate = calcNextBusinessDay(theDate, 45, 2022, 2, "IT");
		      if (nextBusinessDate != null)
		    	  System.out.println(nextBusinessDate);
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
    }
}
