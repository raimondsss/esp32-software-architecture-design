package com.ruubel.utils;

public class PublicHoliday {

	public String date;
    public String localName;
    public String name;
    public String countryCode;
    public Boolean fixed;
    public Boolean countyOfficialHoliday;
    public Boolean countyAdministrationHoliday;
    public Boolean global;
    public String[] counties;
    public int launchYear;
    
    public String toString() {
    	return "{\"date\":\""+date+"\"\n" +
    			",\"localName\":\""+localName+"\"\n" +
    			",\"name\":\""+name+"\"\n" +
    			",\"countryCode\":\""+countryCode+"\"\n" +
    			",\"fixed\":"+fixed+"\n" +
    			",\"global\":"+global+"\n" +
    			",\"launchYear\":"+launchYear+"\n" +
    			"}";
    }
}
