package com.ruubel.utils;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.CharacterData;
import org.passay.PasswordGenerator;

import java.security.SecureRandom;
import java.util.Random;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by ahmed on 29.5.18.
 */
public class Utils {

	/**
     *  Special characters allowed in password.
     */
    //public static final String ALLOWED_SPL_CHARACTERS = "!@#$%^&*()_+";
    public static final String ALLOWED_SPL_CHARACTERS = "!@#$";

    public static final String ERROR_CODE = "ERRONEOUS_SPECIAL_CHARS";
    public static final int    MIN_LENGTH = 10;
    public static final int    NUMBER_OF_LOWERCASE = 2;
    public static final int    NUMBER_OF_UPPERCASE = 2;
    public static final int    NUMBER_OF_DIGIT = 2;

    Random random = new SecureRandom();

    
    public static String passwordEncoder(String password){

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(password);

        return encodedPassword;
    }
    
    public static String generatePassayPassword() {
        PasswordGenerator gen = new PasswordGenerator();
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(NUMBER_OF_LOWERCASE);
     
        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(NUMBER_OF_UPPERCASE);
     
        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(NUMBER_OF_DIGIT);
     
        CharacterData specialChars = new CharacterData() {
            public String getErrorCode() {
                return ERROR_CODE;
            }
     
            public String getCharacters() {
                return ALLOWED_SPL_CHARACTERS;
            }
        };
        CharacterRule splCharRule = new CharacterRule(specialChars);
        splCharRule.setNumberOfCharacters(2);
     
        String password = gen.generatePassword(MIN_LENGTH, splCharRule, lowerCaseRule,  upperCaseRule, digitRule);
        return password;
    }
    
    public static void main(String[] args) {
    	String password = "prova";
    	
    	System.out.println(passwordEncoder(password));
    }
}
