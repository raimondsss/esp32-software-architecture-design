package com.ruubel.utils;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;


public class PasswordUtils {

    //public static final String ALLOWED_SPL_CHARACTERS = "!@#$%^&*()_+";
    public static final String ALLOWED_SPL_CHARACTERS = "!@#$";
    public static final String ERROR_CODE = "ERRONEOUS_SPECIAL_CHARS";
    public static final int    MIN_LENGTH = 10;
    public static final int    NUMBER_OF_LOWERCASE = 2;
    public static final int    NUMBER_OF_UPPERCASE = 2;
    public static final int    NUMBER_OF_DIGIT = 2;

    Random random = new SecureRandom();

    
    public static String generatePassayPassword() {
        PasswordGenerator gen = new PasswordGenerator();
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(NUMBER_OF_LOWERCASE);
     
        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(NUMBER_OF_UPPERCASE);
     
        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(NUMBER_OF_DIGIT);
     
        CharacterData specialChars = new CharacterData() {
            public String getErrorCode() {
                return ERROR_CODE;
            }
     
            public String getCharacters() {
                return ALLOWED_SPL_CHARACTERS;
            }
        };
        CharacterRule splCharRule = new CharacterRule(specialChars);
        splCharRule.setNumberOfCharacters(2);
     
        String password = gen.generatePassword(MIN_LENGTH, splCharRule, lowerCaseRule,  upperCaseRule, digitRule);
        return password;
    }
    
    public static void main(String[] args) {
    	
    	System.out.println(generatePassayPassword());
    	
    	Date now = new Date();
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(now);
    	int day = calendar.get(Calendar.DAY_OF_MONTH);
    	int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
    	int week = calendar.get(Calendar.WEEK_OF_MONTH);
    	int month = calendar.get(Calendar.MONTH);

    	System.out.println(now);
    	System.out.println("DAY_OF_MONTH:"+day);
    	System.out.println("DAY_OF_WEEK:"+weekDay);
    	System.out.println("WEEK_OF_MONTH:"+week);
    	System.out.println("MONTH:"+month);
    }}
