package com.ruubel.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WordUtil {
    private final static Logger logger = LoggerFactory.getLogger(WordUtil.class);

    public static XWPFDocument replaceImage(XWPFDocument document, String imageOldName, String imagePathNew, int newImageWidth, int newImageHeight) throws Exception {
	    try {
	    	logger.info("replaceImage: old=" + imageOldName + ", new=" + imagePathNew);

	        int imageParagraphPos = -1;
	        XWPFParagraph imageParagraph = null;

	        List<IBodyElement> documentElements = document.getBodyElements();
	        for(IBodyElement documentElement : documentElements){
	            imageParagraphPos ++;
	            if(documentElement instanceof XWPFParagraph){
	                imageParagraph = (XWPFParagraph) documentElement;

	                if(imageParagraph != null && imageParagraph.getCTP() != null)
	                	logger.info(imageParagraph.getCTP().toString().trim());
	                
	                if(imageParagraph != null && imageParagraph.getCTP() != null && imageParagraph.getCTP().toString().trim().indexOf(imageOldName) != -1) {
	                    break;
	                }
	            }
	        }

	        if (imageParagraph == null) {
	            throw new Exception("Unable to replace image data due to the exception:\n"
	                    + "'" + imageOldName + "' not found in in document.");
	        }
	        ParagraphAlignment oldImageAlignment = imageParagraph.getAlignment();
	        

	        // remove old image
	        document.removeBodyElement(imageParagraphPos);

	        // now add new image

	        // BELOW LINE WILL CREATE AN IMAGE
	        // PARAGRAPH AT THE END OF THE DOCUMENT.
	        // REMOVE THIS IMAGE PARAGRAPH AFTER 
	        // SETTING THE NEW IMAGE AT THE OLD IMAGE POSITION
	        XWPFParagraph newImageParagraph = document.createParagraph();    
	        XWPFRun newImageRun = newImageParagraph.createRun();
	        //newImageRun.setText(newImageText);
	        newImageParagraph.setAlignment(oldImageAlignment);
	        
	        try (FileInputStream is = new FileInputStream(imagePathNew)) {
	            newImageRun.addPicture(is, XWPFDocument.PICTURE_TYPE_JPEG, imagePathNew,
	                         Units.toEMU(newImageWidth), Units.toEMU(newImageHeight)); 
	        } 

	        // set new image at the old image position
	        document.setParagraph(newImageParagraph, imageParagraphPos);

	        // NOW REMOVE REDUNDANT IMAGE FORM THE END OF DOCUMENT
	        document.removeBodyElement(document.getBodyElements().size() - 1);

	        return document;
	    } catch (Exception e) {
	        throw new Exception("Unable to replace image '" + imageOldName + "' due to the exception:\n" + e);
	    } finally {
	        // cleanup code
	    }
	}
	
	public static XWPFDocument replaceImageTable(XWPFDocument document, String imageOldName, String imagePathNew, int newImageWidth, int newImageHeight) throws Exception {
	    try {
	    	logger.info("replaceImage: old=" + imageOldName + ", new=" + imagePathNew);

	        int imageParagraphPos = -1;
	        XWPFParagraph imageParagraph = null;
	        XWPFTable imageTable = null;
	        XWPFTableCell imageTableCell = null;

	        List<IBodyElement> documentElements = document.getBodyElements();
	        for(IBodyElement documentElement : documentElements){
	            if(documentElement instanceof XWPFTable){
	            	imageTable = (XWPFTable) documentElement;

	            	boolean found = false;
	            	for (XWPFTableRow imageTableRow : imageTable.getRows()) {
		            	for (XWPFTableCell tableCell : imageTableRow.getTableCells()) {
			    	        imageParagraphPos = -1;
			            	for (XWPFParagraph imageTableParagraph : tableCell.getParagraphs()) {
			    	            imageParagraphPos ++;
			            		
				                if(imageTableParagraph != null && imageTableParagraph.getCTP() != null)
				                	logger.info(imageTableParagraph.getCTP().toString().trim());
				                
				                if(imageTableParagraph != null && imageTableParagraph.getCTP() != null && imageTableParagraph.getCTP().toString().trim().indexOf(imageOldName) != -1) {
				                	imageParagraph = imageTableParagraph;
				                	imageTableCell = tableCell;
				                	found = true;
				                	break;
				                }
			            	}
		            		
			            	if (found)
			            		break;
		            	}
	            		
		            	if (found)
		            		break;
	            	}
	                
	            }
	        }

	        if (imageParagraph == null) {
	        	return document;
	            //throw new Exception("Unable to replace image data due to the exception:\n"
	            //        + "'" + imageOldName + "' not found in in document.");
	        }
	        ParagraphAlignment oldImageAlignment = imageParagraph.getAlignment();
	        
	        // remove old image
	        //imageTableCell.removeParagraph(imageParagraphPos);	        
	        //document.removeBodyElement(imageParagraphPos);

	        // now add new image

	        // BELOW LINE WILL CREATE AN IMAGE
	        // PARAGRAPH AT THE END OF THE DOCUMENT.
	        // REMOVE THIS IMAGE PARAGRAPH AFTER 
	        // SETTING THE NEW IMAGE AT THE OLD IMAGE POSITION
	        //XWPFParagraph newImageParagraph = document.createParagraph();    
	        XWPFParagraph newImageParagraph = imageTableCell.addParagraph();    
	        XWPFRun newImageRun = newImageParagraph.createRun();
	        
	        //newImageRun.setFontFamily("Times New Roman");
	        //newImageRun.setText(newImageText);
	        //newImageParagraph.setAlignment(oldImageAlignment);
	        
	        try (FileInputStream is = new FileInputStream(imagePathNew)) {
	            newImageRun.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, imageOldName,
	                         Units.toEMU(newImageWidth), Units.toEMU(newImageHeight)); 
	        } 

	        // set new image at the old image position
	        imageTableCell.setParagraph(newImageParagraph);
	        //document.setParagraph(newImageParagraph, imageParagraphPos);

	        // NOW REMOVE REDUNDANT IMAGE FORM THE END OF DOCUMENT
	        document.removeBodyElement(document.getBodyElements().size() - 1);

	        return document;
	    } catch (Exception e) {
	        throw new Exception("Unable to replace image '" + imageOldName + "' due to the exception:\n" + e);
	    } finally {
	        // cleanup code
	    }
	}
	
	public static void trace(XWPFDocument document, String imageOldName) throws Exception {
	    try {
	        int imageParagraphPos = -1;
	        XWPFParagraph imageParagraph = null;
	        XWPFTable imageTable = null;
	        XWPFTableCell imageTableCell = null;

	        List<IBodyElement> documentElements = document.getBodyElements();
	        for(IBodyElement documentElement : documentElements){
	            if(documentElement instanceof XWPFTable){
	            	imageTable = (XWPFTable) documentElement;

	            	boolean found = false;
	            	for (XWPFTableRow imageTableRow : imageTable.getRows()) {
		            	for (XWPFTableCell tableCell : imageTableRow.getTableCells()) {
			    	        imageParagraphPos = -1;
			            	for (XWPFParagraph imageTableParagraph : tableCell.getParagraphs()) {
			    	            imageParagraphPos ++;
			            		
				                if(imageTableParagraph != null && imageTableParagraph.getCTP() != null)
				                	logger.info(imageTableParagraph.getCTP().toString().trim());
				                
				                if(imageTableParagraph != null && imageTableParagraph.getCTP() != null && imageTableParagraph.getCTP().toString().trim().indexOf(imageOldName) != -1) {
				                	imageParagraph = imageTableParagraph;
				                	imageTableCell = tableCell;
				                	found = true;
				                	break;
				                }
			            	}
		            		
			            	if (found)
			            		break;
		            	}
	            		
		            	if (found)
		            		break;
	            	}
	                
	            }
	        }

	    } catch (Exception e) {
	    	e.printStackTrace();
	    } finally {
	        // cleanup code
	    }
	}
	
    public static void main(String[] args) {
    	
    	String fileName = "/Users/raimondogiordano/Desktop/Ecomac/ecomac_be/wordTemplates/Contratto_preliminare_superficie_fotovoltaico.docx";
    	String outfileName = "/Users/raimondogiordano/Desktop/Ecomac/ecomac_be/wordTemplates/Contratto_preliminare_superficie_fotovoltaico.pdf";
    	
    	try {
			XWPFDocument doc = new XWPFDocument(Files.newInputStream(Paths.get(fileName)));    		
			doc = replaceImageTable(doc, "Immagine 1", "/Users/raimondogiordano/Desktop/Ecomac/ecomac_be/filestorage/firme/firma_1.png", 120, 100);
			trace(doc, "Immagine 1");
			
			PdfOptions options = PdfOptions.create();
	        OutputStream out = new FileOutputStream(new File(outfileName));
	        PdfConverter.getInstance().convert(doc, out, options);
			//FileOutputStream out = new FileOutputStream(fileName);
            //doc.write(out);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	
    }

}