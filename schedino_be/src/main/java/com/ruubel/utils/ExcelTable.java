package com.ruubel.utils;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;


/**
 * Created by xschen on 22/12/2016.
 */
public class ExcelTable extends DataTable {

   private static final Logger logger = LoggerFactory.getLogger(ExcelTable.class);

   private int numberOfSheets=0;
   private XSSFWorkbook myWorkBook;

   private ExcelTable() {

   }

   public ExcelTable(Supplier<InputStream> inputStreamReader) {
      try {
         myWorkBook = new XSSFWorkbook(inputStreamReader.get());
         numberOfSheets = myWorkBook.getNumberOfSheets();
      }
      catch (IOException e) {
         logger.error("Failed to read the excel file", e);
      }
}

   public int getNumberOfSheets() {
      return numberOfSheets;
   }

   public static DataTable load(byte[] bytes) {
      ExcelTable table = new ExcelTable();
      table.load(new ByteArrayInputStream(bytes));
      return table;
   }

   public static DataTable load(Supplier<InputStream> inputStreamReader) {
      ExcelTable table = new ExcelTable();
      return table.load(inputStreamReader.get());
   }

   public DataTable load(int sheetNumber, boolean skipHeader) {
      DataTable result = new DataTable();      
      loadRaw(result, myWorkBook.getSheetAt(sheetNumber), skipHeader);
      return result;
   }

   private DataTable load(InputStream fis) {
      DataTable result = this;

      try {
         XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
         XSSFSheet mySheet = myWorkBook.getSheetAt(0);
         loadFixed(result, mySheet);
         myWorkBook.close();
      }
      catch (IOException e) {
         logger.error("Failed to read the excel file", e);
      }

      return result;
   }

   private String getCellValue(Cell cell) {
      String value=null;
      try {
         //if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
         if (cell.getCellTypeEnum() == CellType.NUMERIC) {

            if (HSSFDateUtil.isCellDateFormatted(cell)) {
               //System.out.println("The cell contains a date value: " + cell.getDateCellValue());
               DataFormatter dataFormatter = new DataFormatter();
               dataFormatter.addFormat("m/d/yy", new java.text.SimpleDateFormat("dd/MM/yyyy"));
               value = dataFormatter.formatCellValue(cell);
               //System.out.println("The cell date value: " + value);
               //Date date = HSSFDateUtil.getJavaDate(cell.getNumberValue());
               //String df = cell.getCellStyle().getDataFormatString();
               //value = new CellDateFormatter(df).format(date); 
            } else {
                double d = cell.getNumericCellValue();
            	if( Math.floor(d) == d ) {
            		value = "" + (int)d;
        		} else {
        			value = "" + d;
        		}
               //System.out.println ("The cell contains a numeric value: " + cell.getNumericCellValue());
               //value = "" + cell.getNumericCellValue();
            }
            
            return value;
         } if (cell.getCellTypeEnum() == CellType.FORMULA) {
        	 FormulaEvaluator evaluator = myWorkBook.getCreationHelper().createFormulaEvaluator();
        	 CellValue cellValue = evaluator.evaluate(cell);
        	 
        	 switch (cellValue.getCellType()) {
        	     case Cell.CELL_TYPE_STRING:
        	         value = cellValue.getStringValue();
        	         break;
        	     case Cell.CELL_TYPE_BOOLEAN:
        	         value = Boolean.toString(cellValue.getBooleanValue());
        	         break;
        	     case Cell.CELL_TYPE_NUMERIC:
        	        double d = cellValue.getNumberValue();
                 	if( Math.floor(d) == d ) {
                 		value = "" + (int)d;
             		} else {
             			value = "" + d;
             		}
        	        break;
        	 }

        	 return value;
         } else {
            value = cell.getStringCellValue();
         }
      }
      catch (IllegalStateException ex) {
         logger.error("Failed to read cell", ex);
         //try {
         //   value = "" + cell.getNumericCellValue();
         //}
         //catch (IllegalStateException ex2) {
         //   value = "INVALID_HEADER";
         //}
      }

      return value;
   }

   private DataTable loadFixed(DataTable result, XSSFSheet mySheet) {

      try {

         result.setName(mySheet.getSheetName());
         Iterator<Row> rowIterator = mySheet.iterator();

         Map<Integer, String> columns = new HashMap<>();

         int rowIndex = 0;

         while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            DataRow dataRow = result.newRow();

            if (rowIndex == 0) {
               Iterator<Cell> cellIterator = row.cellIterator();
               int columnIndex = 0;

               while (cellIterator.hasNext()) {
                  Cell cell = cellIterator.next();

                  // skip hidden headers
                  if (cell.getCellStyle().getHidden()) {
                     logger.info("cell hidden");
                     continue;
                  }

                  String value = getCellValue(cell);
                  if (value == null) {
                     columnIndex++;
                     continue;
                  }

                  value = value.trim();
                  if (!StringUtils.isEmpty(value)) {
                     logger.info("column: {}", value);
                     columns.put(columnIndex, value);
                  }

                  columnIndex++;
               }

               for(String column : columns.values()) {
                  result.columns().add(column);
               }

            }

            else {
               ArrayList<Integer> columnKeys = new ArrayList<Integer>(columns.keySet());
               int numColumns = columnKeys.size();

               for (int i=0; i<numColumns; i++) {
                  int columnIndex = columnKeys.get(i);
                  Cell cell = row.getCell(columnIndex);
                  String value = "";

                  if (cell != null) {
                     value = getCellValue(cell);
                     logger.info("#" + (i + 1) + ": cell value: {}", value);
                  }

                  // final check for null values
                  if (value == null) {
                     value = "";
//                     logger.info("#" + (i + 1) + ": cell value (value null): {}", value);
                  }

                  dataRow.cell(columns.get(columnIndex), value);
               }

               result.rows().add(dataRow);
            }

            rowIndex++;
         }

      }
      catch (Exception e) {
         logger.error("Failed to read the excel file", e);
      }

      return result;

   }

   private DataTable loadRaw(DataTable result, XSSFSheet mySheet, boolean skipHeader) {

      try {

         result.setName(mySheet.getSheetName());
         Iterator<Row> rowIterator = mySheet.iterator();

         int rowIndex = 0;
         while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            
            if (rowIndex == 0 && skipHeader) {
            	rowIndex++;
            	continue;
            }
            
            int lastColumn = row.getLastCellNum();
            
            DataRow dataRow = result.newRow();
            int columnIndex = 0;
            //Iterator<Cell> cellIterator = row.cellIterator();
            //while (cellIterator.hasNext()) {
            //   Cell cell = cellIterator.next();
            for (int cn = 0; cn < lastColumn; cn++) {
        	   Cell cell = row.getCell(cn, Row.RETURN_BLANK_AS_NULL);
        	      
               String value = "";

               if (cell != null) {
            	   value = getCellValue(cell);
            	  /*
            	  try {
                     value = cell.getStringCellValue();
                     logger.info("#" + (columnIndex + 1) + ": cell value (string): {}", value);

                  } catch (IllegalStateException ex) {
                     try {
                        value = "" + (int) cell.getNumericCellValue();
                        logger.info("#" + (columnIndex + 1) + ": cell value (numeric): {}", value);

                     } catch (IllegalStateException ex2) {
                        value = "";
                        logger.info("#" + (columnIndex + 1) + ": cell value (neither string nor numeric): {}", value);
                     }
                  }
                  */
               }

               // final check for null values
               if (value == null) {
                  value = "";
//                     logger.info("#" + (i + 1) + ": cell value (value null): {}", value);
               }

               dataRow.cell(columnIndex, value);
               columnIndex++;
            }

            result.rows().add(dataRow);
            rowIndex++;
         }

      }
      catch (Exception e) {
         logger.error("Failed to process the excel file", e);
      }

      return result;

   }
}
