package com.ruubel.mail;

import com.ruubel.model.EmailQueue;

public interface ISendMail {

    public void send(EmailQueue emailQueue) throws Exception;
}
