package com.ruubel.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.ruubel.dto.EmailDto;
import com.ruubel.model.EmailQueue;
import com.ruubel.service.SendGridService;

@Component
@Configuration
public class SendMailSender implements ISendMail {

    @Autowired
    private JavaMailSender mailSender;

    
	public void send(EmailQueue emailQueue) throws Exception {    	
        //final SimpleMailMessage email = constructSimpleEmailMessage(emailQueue);
        final MimeMessage email = constructMimeEmailMessage(emailQueue);
        mailSender.send(email);
    }

    private final SimpleMailMessage constructSimpleEmailMessage(final EmailQueue emailQueue) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(emailQueue.getToAddress());
        email.setSubject(emailQueue.getRenderedSubject());
        email.setFrom(emailQueue.getFromAddress());
    	email.setText(emailQueue.getRenderedTemplate());
    	
        return email;
    }

    private final MimeMessage constructMimeEmailMessage(final EmailQueue emailQueue) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper email = new MimeMessageHelper(mimeMessage, true);

        email.setTo(emailQueue.getToAddress());
        email.setSubject(emailQueue.getRenderedSubject());
        email.setFrom(emailQueue.getFromAddress());
    	email.setText(emailQueue.getRenderedTemplate(), true);
    	
        return mimeMessage;
    }
}
