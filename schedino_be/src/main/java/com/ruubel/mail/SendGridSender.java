package com.ruubel.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.ruubel.dto.EmailDto;
import com.ruubel.model.EmailQueue;
import com.ruubel.service.SendGridService;

@Component
@Configuration
public class SendGridSender implements ISendMail {

	@Autowired
	SendGridService sendGridService;

	@Value("${app.registration.templatemail}")
    private String templateId;

	
	public void send(EmailQueue emailQueue) throws Exception {    	
        final EmailDto email = constructSendGridEmailMessage(emailQueue);
        sendGridService.sendMail(email);
    }

    private final EmailDto constructSendGridEmailMessage(final EmailQueue emailQueue) {
    	EmailDto email = new EmailDto();
    	email.setToEmail(emailQueue.getToAddress());
    	email.setEmailSubject(emailQueue.getRenderedSubject());
    	email.setFromEmail(emailQueue.getFromAddress());
    	email.setMessage(emailQueue.getRenderedTemplate());
    	email.setTemplateId(templateId);
    	
        return email;
    }

}
