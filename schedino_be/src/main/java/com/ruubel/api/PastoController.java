package com.ruubel.api;

import io.swagger.annotations.*;

import javax.servlet.ServletException;
//import springfox.documentation.annotations.*;
import javax.servlet.http.*;
import org.springframework.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.*;
import org.springframework.transaction.annotation.Transactional;
//import static org.springframework.http.MediaType.*;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ruubel.api.response.PastoListResponse;
import com.ruubel.api.response.PastoResponse;
import com.ruubel.api.response.OperationResponse;
import com.ruubel.api.response.OperationResponse.ResponseStatusEnum;
import com.ruubel.model.*;
import com.ruubel.repository.*;
import com.ruubel.service.*;

@RestController
@Transactional
@RequestMapping(value = "/schedino/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Pasto"})
public class PastoController {

	private final static Logger logger = LoggerFactory.getLogger(PastoController.class);

	private ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

	@Autowired private PastoService pastoService;

    @Autowired private FileStorageService fileStorageService;
    
    @Autowired
    private AuthenticationFacadeService authenticationFacadeService;

	@ApiOperation(value = "List of pasti", response = PastoListResponse.class)
	@RequestMapping(value = "/pasti", method = RequestMethod.GET)
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public PastoListResponse getEntitiesByPage(
			@ApiParam(value = ""    )               @RequestParam(value = "page"  ,  defaultValue="0"   ,  required = false) Integer page,
			@ApiParam(value = "between 1 to 1000" ) @RequestParam(value = "size"  ,  defaultValue="20"  ,  required = false) Integer size,
			@RequestParam(value = "codice"  , required = false) String codice,	
			@RequestParam(value = "nome"  , required = false) String nome,
			@RequestParam(value = "data"  , required = false) String data,
			@RequestParam(value = "id"  , required = false) Integer id,
			Pageable pageable
			) {
	  PastoListResponse resp = new PastoListResponse();
	  Pasto qry = new Pasto();

	  
      Page<Pasto> pg = pastoService.findAll(codice, nome, data, id, pageable);
      resp.setPageStats(pg, true);
      resp.setItems(pg.getContent());
      return resp;
    }
	

	@ApiOperation(value = "Gets pasto information", response = PastoResponse.class)
	@RequestMapping(value = "/pasto/{id}", method = RequestMethod.GET, produces = {"application/json"})
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public PastoResponse getEntityInformation(
			@PathVariable("id") Integer id, 
			HttpServletRequest req) {

		PastoResponse resp = new PastoResponse();
		Pasto qry = new Pasto();
        if (id != null)  { qry.setId(id); }
        Pasto entity = pastoService.getRepository().findOne(org.springframework.data.domain.Example.of(qry))
                .orElse(new Pasto());

		if (entity != null) {
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setData(entity);
		}
		else {
            resp.setOperationStatus(ResponseStatusEnum.NO_ACCESS);
			resp.setOperationMessage("No entity exists");
		}
		resp.setData(entity);
		return resp;
	}

    @ApiOperation(value = "Add new pasto", response = PastoResponse.class)
    @RequestMapping(value = "/pasto", method = RequestMethod.POST, produces = {"application/json"})
 //   @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
    public PastoResponse addNewEntity(@RequestBody Pasto pasto, HttpServletRequest req) {
    	PastoResponse resp = new PastoResponse();
        try {
        	Pasto addedEntity = this.pastoService.save(pasto);
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Entity Added");
            resp.setData(addedEntity);
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

        return resp;
    }


	@ApiOperation(value = "Update a pasto", response = PastoResponse.class)
	@RequestMapping(value = "/pasto", method = RequestMethod.PUT, produces = {"application/json"})
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public PastoResponse updateEntity(@RequestBody Pasto pasto, HttpServletRequest req) {
		PastoResponse resp = new PastoResponse();
        try {
            if (this.pastoService.getRepository().existsById(pasto.getId()) ){
            	Pasto updateEntity = this.pastoService.save(pasto);
                resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Entity updated");
                resp.setData(updateEntity);
            }
            else{
                resp.setOperationStatus(ResponseStatusEnum.ERROR);
                resp.setOperationMessage("No entity exist");
            }
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

		return resp;
	}

    @ApiOperation(value = "Delete a pasto", response = OperationResponse.class)
    @RequestMapping(value = "/pasto/{id}", method = RequestMethod.DELETE, produces = {"application/json"})
//   @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
    public OperationResponse deleteEntity(@PathVariable("id") Integer id, HttpServletRequest req) {
        OperationResponse resp = new OperationResponse();
        try {
            if (this.pastoService.getRepository().existsById(id) ){
                this.pastoService.delete(id);
                resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Entity Deleted");
            }
            else{
                resp.setOperationStatus(ResponseStatusEnum.ERROR);
                resp.setOperationMessage("No entity exist");
            }
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

        return resp;
    }

    @ApiOperation(value = "Upload logo", response = OperationResponse.class)
	@RequestMapping(value = "/pasto/logo", method = RequestMethod.POST)
	public OperationResponse uploadLogo(
			@RequestParam("file") MultipartFile file,
	        @RequestParam(value = "pastoId", required = false) Integer pastoId)
			throws ServletException, IOException {
    	 
	   OperationResponse resp = new OperationResponse();
 
	   try {
		   Pasto pasto = this.pastoService.getRepository().getOne(pastoId);
		   if (pasto == null || pasto.getId() == null) {
               resp.setOperationStatus(ResponseStatusEnum.ERROR);
               resp.setOperationMessage("No entity exist");			   
		   } else {
			   fileStorageService.store(file, "pasto", ""+pastoId);
	           this.pastoService.getRepository().save(pasto);
			   resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
	           resp.setOperationMessage("");			   
		   }

	   }catch(Exception ex) {
			logger.error("Failed to process the check file", ex);

            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ex.toString());
		}

        return resp;
	}

    @ApiOperation(value = "Upload pastos from excel", response = OperationResponse.class)
	@RequestMapping(value = "/pastos/upload", method = RequestMethod.POST)
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public OperationResponse uploadEntities(
			@RequestParam("file") MultipartFile file,
			@RequestParam("token") String token)
			throws ServletException, IOException {
	   logger.info("upload-excel invoked.");
 
	   OperationResponse resp = new OperationResponse();
 
	   final String label = UUID.randomUUID().toString() + ".xlsx";
	   final String filepath = "/tmp/" + label;
	   byte[] bytes = file.getBytes();
	   File fh = new File("/tmp/");
	   if(!fh.exists()){
		  fh.mkdir();
	   }
 
	   try {
			FileOutputStream writer = new FileOutputStream(filepath);
			writer.write(bytes);
			writer.close();

			logger.info("file bytes received: {}", bytes.length);

			executor.submit(() -> {
				try {
					pastoService.saveFromFile(filepath, token);
				}catch(Exception ex) {
					logger.error("Failed on saving records", ex);
				}
			  });

			resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Id: "+label);

	   }catch(IOException ex) {
			logger.error("Failed to process the uploaded file", ex);

            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ex.toString());
		}

        return resp;
	}
    
    
    /*@ApiOperation(value = "Sum of Progetti", response = PastoTotaleResponse.class)
	@RequestMapping(value = "/progetti-sum", method = RequestMethod.GET)
	public PastoTotaleResponse getSumEnitities(
	    @RequestParam(value = "data"  , required = false) String data,
	    @RequestParam(value = "startData"  , required = false) String startData,
	    @RequestParam(value = "endData"  , required = false) String endData,
	    @RequestParam(value = "scoutId"  , required = false) Integer scoutId,
	    @RequestParam(value = "stato"  , required = false) Integer[] stato
	  ) {
    	PastoTotaleResponse resp = new PastoTotaleResponse();
	
		try {
	      Double totaleContanti = pastoService.sumAll(
	    		  data != null ? new SimpleDateFormat("ddMMyyyy").parse(data) : null, 
				  startData != null ? new SimpleDateFormat("ddMMyyyy").parse(startData) : null, 
				  endData != null ? new SimpleDateFormat("ddMMyyyy").parse(endData) : null, 
						  scoutId, stato);
	      resp.setTotale(totaleContanti == null ? 0.0 : totaleContanti);
	      
	      Double totaleContantiParziale = pastoService.sumAll(
	    		  data != null ? new SimpleDateFormat("ddMMyyyy").parse(data) : null, 
				  startData != null ? new SimpleDateFormat("ddMMyyyy").parse(startData) : null, 
				  endData != null ? new SimpleDateFormat("ddMMyyyy").parse(endData) : null, 
						  scoutId, stato);
	      resp.setTotale(resp.getTotale()+(totaleContantiParziale == null ? 0.0 : totaleContantiParziale));
	      
	      Double totaleBancomat = pastoService.sumAll(
	    		  data != null ? new SimpleDateFormat("ddMMyyyy").parse(data) : null, 
				  startData != null ? new SimpleDateFormat("ddMMyyyy").parse(startData) : null, 
				  endData != null ? new SimpleDateFormat("ddMMyyyy").parse(endData) : null, 
						  scoutId, stato);
	      
	      resp.setTotale(totaleBancomat == null ? 0.0 : totaleBancomat);
          resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
		  
		} catch (Exception ex) {
		  resp.setOperationStatus(ResponseStatusEnum.ERROR);
		  resp.setOperationMessage(ex.toString());
		}
      
		return resp;
    }*/
 
}
