package com.ruubel.api;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;

import com.ruubel.api.response.OperationResponse;
import com.ruubel.api.response.OperationResponse.ResponseStatusEnum;
import com.ruubel.service.FileStorageService;



@RestController
@Transactional
@RequestMapping(value = "/schedino")
@Api(tags = {"FileStorage"})
public class FileStorageController {

	private final static Logger logger = LoggerFactory.getLogger(FileStorageController.class);

    @Autowired private FileStorageService fileStorageService;

	@ApiOperation(value = "Add new file", response = OperationResponse.class)
	@RequestMapping(value = "/filestorage", method = RequestMethod.POST, consumes = { "multipart/form-data" }, produces = {"application/json"})
	public OperationResponse addNewFile(
        @RequestParam("file") MultipartFile file,
        @RequestParam("key") String key, 
        HttpServletRequest req) {
        
		OperationResponse resp = new OperationResponse();
        try {
            fileStorageService.store(file);
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("File Added");
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

		return resp;
	}

	@ApiOperation(value = "Get a file", response = ResponseEntity.class)
	@RequestMapping(value = "/filestorage/{filename:.+}/{bust}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {

		String dir = "";
		String subDir = "";
		String[] tokens = filename.split("\\$");
		if (tokens.length == 1) {
			filename = tokens[0];			
		} else if (tokens.length == 2) {
			dir = tokens[0];
			filename = tokens[1];			
		} else if (tokens.length == 3) {
			dir = tokens[0];
			subDir = tokens[1];
			filename = tokens[2];			
		}
		
        Resource file = fileStorageService.loadFileAsResource(filename, dir, subDir);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

}