package com.ruubel.api;

import io.swagger.annotations.*;

import javax.servlet.ServletException;
//import springfox.documentation.annotations.*;
import javax.servlet.http.*;
import org.springframework.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.*;
import org.springframework.transaction.annotation.Transactional;
//import static org.springframework.http.MediaType.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ruubel.api.response.OperationResponse;
import com.ruubel.api.response.OperationResponse.ResponseStatusEnum;
import com.ruubel.dto.EmailMessageDto;
import com.ruubel.model.*;
import com.ruubel.repository.*;
import com.ruubel.service.*;
import com.ruubel.tools.HttpTools;

@RestController
@Transactional
@RequestMapping(value = "/schedino/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Email"})
public class EmailController {

	private final static Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailServiceProducer emailServiceProducer;

    
    @ApiOperation(value = "Send email message", response = OperationResponse.class)
    @RequestMapping(value = "/sendmail", method = RequestMethod.POST, produces = {"application/json"})
    public OperationResponse sendEmail(@RequestBody EmailMessageDto message, HttpServletRequest req) {
    	OperationResponse resp = new OperationResponse();
        try {
        	emailServiceProducer.sendEmail(message, req.getLocale());

            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Email enqueued");
        }
        catch ( Exception ge ){
        	logger.error("Errore sending email "+ge);
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

        return resp;
    }

}
