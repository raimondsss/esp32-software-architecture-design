package com.ruubel.api;

import io.swagger.annotations.*;

import javax.servlet.ServletException;
//import springfox.documentation.annotations.*;
import javax.servlet.http.*;
import org.springframework.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.*;
import org.springframework.transaction.annotation.Transactional;
//import static org.springframework.http.MediaType.*;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ruubel.api.response.CompagniaListResponse;
import com.ruubel.api.response.CompagniaResponse;
import com.ruubel.api.response.OperationResponse;
import com.ruubel.api.response.OperationResponse.ResponseStatusEnum;
import com.ruubel.model.*;
import com.ruubel.repository.*;
import com.ruubel.service.*;

@RestController
@Transactional
@RequestMapping(value = "/schedino/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = {"Compagnia"})
public class CompagniaController {

	private final static Logger logger = LoggerFactory.getLogger(CompagniaController.class);

	private ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

	@Autowired private CompagniaService compagniaService;

    @Autowired private FileStorageService fileStorageService;

	@ApiOperation(value = "List of companies", response = CompagniaListResponse.class)
	@RequestMapping(value = "/compagnie", method = RequestMethod.GET)
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public CompagniaListResponse getEntitiesByPage(
			@ApiParam(value = ""    )               @RequestParam(value = "page"  ,  defaultValue="0"   ,  required = false) Integer page,
			@ApiParam(value = "between 1 to 1000" ) @RequestParam(value = "size"  ,  defaultValue="20"  ,  required = false) Integer size,
			@RequestParam(value = "ragioneSociale"  , required = false) String ragioneSociale,	
			Pageable pageable
			) {
	  CompagniaListResponse resp = new CompagniaListResponse();
	  Compagnia qry = new Compagnia();

      Page<Compagnia> pg = compagniaService.findAll(ragioneSociale, pageable);
      resp.setPageStats(pg, true);
      resp.setItems(pg.getContent());
      return resp;
    }

	@ApiOperation(value = "Gets compagnia information", response = CompagniaResponse.class)
	@RequestMapping(value = "/compagnia/{id}", method = RequestMethod.GET, produces = {"application/json"})
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public CompagniaResponse getEntityInformation(
			@PathVariable("id") Integer id, 
			HttpServletRequest req) {

		CompagniaResponse resp = new CompagniaResponse();
		Compagnia qry = new Compagnia();
        if (id != null)  { qry.setId(id); }
        Compagnia entity = compagniaService.getRepository().findOne(org.springframework.data.domain.Example.of(qry))
                .orElse(new Compagnia());

		if (entity != null) {
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setData(entity);
		}
		else {
            resp.setOperationStatus(ResponseStatusEnum.NO_ACCESS);
			resp.setOperationMessage("No entity exists");
		}
		resp.setData(entity);
		return resp;
	}

    @ApiOperation(value = "Add new compagnia", response = CompagniaResponse.class)
    @RequestMapping(value = "/compagnia", method = RequestMethod.POST, produces = {"application/json"})
 //   @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
    public CompagniaResponse addNewEntity(@RequestBody Compagnia compagnia, HttpServletRequest req) {
    	CompagniaResponse resp = new CompagniaResponse();
        try {
        	Compagnia addedEntity = this.compagniaService.save(compagnia);
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Entity Added");
            resp.setData(addedEntity);
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

        return resp;
    }


	@ApiOperation(value = "Update a compagnia", response = CompagniaResponse.class)
	@RequestMapping(value = "/compagnia", method = RequestMethod.PUT, produces = {"application/json"})
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public CompagniaResponse updateEntity(@RequestBody Compagnia compagnia, HttpServletRequest req) {
		CompagniaResponse resp = new CompagniaResponse();
        try {
            if (this.compagniaService.getRepository().existsById(compagnia.getId()) ){
            	Compagnia updateEntity = this.compagniaService.save(compagnia);
                resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Entity updated");
                resp.setData(updateEntity);
            }
            else{
                resp.setOperationStatus(ResponseStatusEnum.ERROR);
                resp.setOperationMessage("No entity exist");
            }
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

		return resp;
	}

    @ApiOperation(value = "Delete a compagnia", response = OperationResponse.class)
    @RequestMapping(value = "/compagnia/{id}", method = RequestMethod.DELETE, produces = {"application/json"})
//   @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
    public OperationResponse deleteEntity(@PathVariable("id") Integer id, HttpServletRequest req) {
        OperationResponse resp = new OperationResponse();
        try {
            if (this.compagniaService.getRepository().existsById(id) ){
                this.compagniaService.delete(id);
                resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Entity Deleted");
            }
            else{
                resp.setOperationStatus(ResponseStatusEnum.ERROR);
                resp.setOperationMessage("No entity exist");
            }
        }
        catch ( Exception ge ){
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ge.toString());
        }

        return resp;
    }

    @ApiOperation(value = "Upload logo", response = OperationResponse.class)
	@RequestMapping(value = "/compagnia/logo", method = RequestMethod.POST)
	public OperationResponse uploadLogo(
			@RequestParam("file") MultipartFile file,
	        @RequestParam(value = "compagniaId", required = false) Integer compagniaId)
			throws ServletException, IOException {
    	 
	   OperationResponse resp = new OperationResponse();
 
	   try {
		   Compagnia compagnia = this.compagniaService.getRepository().getOne(compagniaId);
		   if (compagnia == null || compagnia.getId() == null) {
               resp.setOperationStatus(ResponseStatusEnum.ERROR);
               resp.setOperationMessage("No entity exist");			   
		   } else {
			   fileStorageService.store(file, "compagnia", ""+compagniaId);
			   compagnia.setLogo(file.getOriginalFilename());
	           this.compagniaService.getRepository().save(compagnia);
			   resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
	           resp.setOperationMessage("");			   
		   }

	   }catch(Exception ex) {
			logger.error("Failed to process the check file", ex);

            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ex.toString());
		}

        return resp;
	}

    @ApiOperation(value = "Upload companies from excel", response = OperationResponse.class)
	@RequestMapping(value = "/companies/upload", method = RequestMethod.POST)
//    @PreAuthorize("hasRole('ROLE_PRODUCT_ADMIN')")
	public OperationResponse uploadEntities(
			@RequestParam("file") MultipartFile file,
			@RequestParam("token") String token)
			throws ServletException, IOException {
	   logger.info("upload-excel invoked.");
 
	   OperationResponse resp = new OperationResponse();
 
	   final String label = UUID.randomUUID().toString() + ".xlsx";
	   final String filepath = "/tmp/" + label;
	   byte[] bytes = file.getBytes();
	   File fh = new File("/tmp/");
	   if(!fh.exists()){
		  fh.mkdir();
	   }
 
	   try {
			FileOutputStream writer = new FileOutputStream(filepath);
			writer.write(bytes);
			writer.close();

			logger.info("file bytes received: {}", bytes.length);

			executor.submit(() -> {
				try {
					compagniaService.saveFromFile(filepath, token);
				}catch(Exception ex) {
					logger.error("Failed on saving records", ex);
				}
			  });

			resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Id: "+label);

	   }catch(IOException ex) {
			logger.error("Failed to process the uploaded file", ex);

            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ex.toString());
		}

        return resp;
	}
 
}
