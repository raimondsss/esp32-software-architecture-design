package com.ruubel.api;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import com.ruubel.model.Credentials;
import com.ruubel.model.VerificationToken;
import com.ruubel.dto.ApiResponse;
import com.ruubel.dto.PasswordDto;
import com.ruubel.dto.UserDto;
import com.ruubel.error.InvalidOldPasswordException;
import com.ruubel.event.OnRegistrationCompleteEvent;
import com.ruubel.service.UserSecurityService;
import com.ruubel.service.UserService;


@RestController
@RequestMapping("/api/registration")
public class RegistrationController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserSecurityService securityUserService;
    
    @Autowired
    private MessageSource messages;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private Environment env;

    @Autowired
    private AuthenticationManager authenticationManager;

    
    public RegistrationController() {
        super();
    }

    // Registration
    @PostMapping("/registerUser")
    public ApiResponse registerUserAccount(@RequestBody @Valid final UserDto accountDto, final HttpServletRequest request) {
        LOGGER.debug("Registering user account with information: {}", accountDto);

        try {
            final Credentials registered = userService.registerNewUserAccount(accountDto);
            //userService.addUserLocation(registered, getClientIP(request));
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), getAppUrl(request)));
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS);
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }

    }

    // user activation - verification
    @GetMapping("/registerConfirm")
    @ResponseBody
    public ApiResponse registerUserConfirm(final HttpServletRequest request, @RequestParam("token") final String verificationToken) {
        try {
            final String result = userService.validateVerificationToken(verificationToken);
            if (result.equals(UserService.TOKEN_VALID)) {
                final Credentials user = userService.getUser(verificationToken);
                return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, messages.getMessage("message.accountVerified", null, request.getLocale()));
            }

            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, messages.getMessage("auth.message" + result, null, request.getLocale()));
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }

    }

    @GetMapping("/resendRegistrationToken")
    @ResponseBody
    public ApiResponse resendRegistrationToken(final HttpServletRequest request, @RequestParam("token") final String existingToken) {
        try {
            final VerificationToken newToken = userService.generateNewVerificationToken(existingToken);
            final Credentials user = userService.getUser(newToken.getToken());
            mailSender.send(constructResendVerificationTokenEmail(getAppUrl(request), request.getLocale(), newToken, user));
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, messages.getMessage("message.resendToken", null, request.getLocale()));
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

    // Reset password
    @PostMapping("/resetPassword")
    @ResponseBody
    public ApiResponse resetPassword(final HttpServletRequest request, @RequestParam("email") final String userEmail) {
        try {
            final Credentials user = userService.findUserByEmail(userEmail);
            if (user != null) {
                final String token = UUID.randomUUID().toString();
                userService.createPasswordResetTokenForUser(user, token);
                mailSender.send(constructResetTokenEmail(getAppUrl(request), request.getLocale(), token, user));
            }
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, messages.getMessage("message.resetPasswordEmail", null, request.getLocale()));
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

    @PostMapping("/changePassword")
    @ResponseBody
    public ApiResponse savePassword(final Locale locale, @Valid PasswordDto passwordDto) {
        try {
            final String result = securityUserService.validatePasswordResetToken(passwordDto.getToken());
            if(result != null) {
                return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, messages.getMessage("auth.message." + result, null, locale));
            }

            Optional<Credentials> user = userService.getUserByPasswordResetToken(passwordDto.getToken());
            if(user.isPresent()) {
                userService.changeUserPassword(user.get(), passwordDto.getNewPassword());
                return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, messages.getMessage("message.resetPasswordSuc", null, locale));
            } 

            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, messages.getMessage("auth.message.invalid", null, locale));
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }

    }

    // change user password
    @PostMapping("/updatePassword")
    @ResponseBody
    public ApiResponse changeUserPassword(final Locale locale, @Valid PasswordDto passwordDto) {
        try {
            final Credentials user = userService.findUserByEmail(((Credentials) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
            if (!userService.checkIfValidOldPassword(user, passwordDto.getOldPassword())) {
                throw new InvalidOldPasswordException();
            }
            userService.changeUserPassword(user, passwordDto.getNewPassword());
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, messages.getMessage("message.updatePasswordSuc", null, locale));
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

    @PostMapping("/update/2fa")
    @ResponseBody
    public ApiResponse modifyUser2FA(@RequestParam("use2FA") final boolean use2FA) throws UnsupportedEncodingException {
        final Credentials user = userService.updateUser2FA(use2FA);
        if (use2FA) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.generateQRUrl(user));
        }
        return null;
    }

    // ============== NON-API ============

    private SimpleMailMessage constructResendVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final Credentials user) {
        final String confirmationUrl = contextPath + "/registrationConfirm.html?token=" + newToken.getToken();
        final String message = messages.getMessage("message.resendToken", null, locale);
        return constructEmail("Resend Registration Token", message + " \r\n" + confirmationUrl, user);
    }

    private SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale, final String token, final Credentials user) {
        final String url = contextPath + "/user/changePassword?token=" + token;
        final String message = messages.getMessage("message.resetPassword", null, locale);
        return constructEmail("Reset Password", message + " \r\n" + url, user);
    }

    private SimpleMailMessage constructEmail(String subject, String body, Credentials user) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    private final String getClientIP(HttpServletRequest request) {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

    public void authWithHttpServletRequest(HttpServletRequest request, String username, String password) {
        try {
            request.login(username, password);
        } catch (ServletException e) {
            LOGGER.error("Error while login ", e);
        }
    }

    public void authWithAuthManager(HttpServletRequest request, String username, String password) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);
        authToken.setDetails(new WebAuthenticationDetails(request));
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
    }

    public void authWithoutPassword(Credentials user) {
        //List<Authority> privileges = user.getAuthorities().stream().map(role -> role.getPrivileges()).flatMap(list -> list.stream()).distinct().collect(Collectors.toList());
    	//List<GrantedAuthority> authorities = privileges.stream().map(p -> new SimpleGrantedAuthority(p.getName())).collect(Collectors.toList());
    	//Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, authorities);

        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
