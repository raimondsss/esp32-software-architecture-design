package com.ruubel.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.ruubel.model.Credentials;
import com.ruubel.dto.ApiResponse;
import com.ruubel.dto.PasswordDto;
import com.ruubel.dto.UserDto;
import com.ruubel.dto.UserProfile;
import com.ruubel.error.InvalidOldPasswordException;
import com.ruubel.event.OnRegistrationCompleteEvent;
import com.ruubel.event.OnResetPasswordCompleteEvent;
import com.ruubel.service.AuthenticationFacadeService;
import com.ruubel.service.EmailServiceProducer;
import com.ruubel.service.UserService;
import com.ruubel.utils.Utils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/schedino/api/v1/users")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MessageSource messages;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private Environment env;

    @Autowired
    private AuthenticationFacadeService authenticationFacadeService;
    
    @Autowired
    private EmailServiceProducer emailServiceProducer;

	private ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

	/*
    @RequestMapping("/me")
    public ResponseEntity<UserProfile> profile() {
        //Build some dummy data to return for testing
    	Credentials user = (Credentials) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = user.getUsername() + "@howtodoinjava.com";
 
        UserProfile profile = new UserProfile();
        profile.setName(user.getUsername());
        profile.setEmail(email);
 
        return ResponseEntity.ok(profile);
    }
    */
    
    @GetMapping("/me")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Principal> get(final Principal principal) {
        return ResponseEntity.ok(principal);
    }
    
    @GetMapping("/profile")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse profile(final Principal principal) {
        log.info(String.format("received request to profile user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        User user = (User)authenticationFacadeService.getAuthentication().getPrincipal();
        //return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.findUserByUsername(user.getUsername()));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.find(user.getUsername()));
    }
    
    @PutMapping("/profile")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse updateProfile(@RequestBody UserDto user){
        log.info(String.format("received request to create user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.update(user));
    }

    // Reset password
    @PostMapping("/resetPassword")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse resetPassword(@RequestParam("email") final String userEmail, final HttpServletRequest request) {
        log.info(String.format("received request to rest password for email %s", userEmail));
        final Credentials user = userService.findUserByEmail(userEmail);
        if (user == null) {
        	log.error("email not found");
            return new ApiResponse(HttpStatus.FORBIDDEN, ApiResponse.ERROR, "email not found");
        }
        	
    	final String tmpPassword = Utils.generatePassayPassword();
        log.debug(String.format("password generated %s", tmpPassword));
        //final String token = UUID.randomUUID().toString();
        //userService.createPasswordResetTokenForUser(user, token);
        userService.resetUserPassword(user, tmpPassword);
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(tmpPassword);
        emailServiceProducer.sendResetPasswordEmail(userDto, request.getLocale());
        //mailSender.send(constructResetPasswordEmail(request.getLocale(), tmpPassword, user));
        //eventPublisher.publishEvent(new OnResetPasswordCompleteEvent(user, tmpPassword, request.getLocale(), getAppUrl(request)));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS);
    }

    // change user password with check
    @PostMapping("/updatePassword")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse updateUserPassword(@RequestBody PasswordDto passwordDto) {
        log.info(String.format("received request to update password user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        User principal = (User)authenticationFacadeService.getAuthentication().getPrincipal();
        Credentials user = userService.findUserByUsername(principal.getUsername());
        
        //final Credentials user = userService.findUserByEmail(((Credentials) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
        if (!userService.checkIfValidOldPassword(user, passwordDto.getOldPassword())) {
//            throw new InvalidOldPasswordException();
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR);
        }
        userService.changeUserPassword(user, passwordDto.getNewPassword());
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS);
    }

    // change user password
    @PostMapping("/changePassword")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse changeUserPassword(@RequestBody PasswordDto passwordDto) {
        log.info(String.format("received request to change password user %s", passwordDto.getUsername()));
        Credentials user = userService.findUserByUsername(passwordDto.getUsername());
        
        userService.changeUserPassword(user, passwordDto.getNewPassword());
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS);
    }

    // change user password
    @PostMapping("/resetPasswordByEmail")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse changeUserPasswordByEmail(@RequestBody PasswordDto passwordDto, final HttpServletRequest request) {
        log.info(String.format("received request to reset password for email %s", passwordDto.getEmail()));
        final Credentials user = userService.findUserByEmail( passwordDto.getEmail());
        if (user == null) {
        	log.error("email not found");
            return new ApiResponse(HttpStatus.FORBIDDEN, ApiResponse.ERROR, "email not found");
        }

        log.debug(String.format("new password %s", passwordDto.getNewPassword()));
        userService.resetUserPassword(user, passwordDto.getNewPassword());
        eventPublisher.publishEvent(new OnResetPasswordCompleteEvent(user, passwordDto.getNewPassword(), request.getLocale(), getAppUrl(request)));

        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS);
    }

    @GetMapping("/list")
    public ApiResponse listUser(@RequestParam(value = "organizationId"  , required = false) Integer organizationId, 
    		@RequestParam(value = "username"  , required = false) String username,
    		@RequestParam(value = "role"  , required = false) String role){
        log.info(String.format("received request to list user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        if (organizationId != null && username != null)
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.findAllByOrganizationIdAndUsername(organizationId, username));
        else if (organizationId != null&& role != null)
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.findAllByOrganizationIdAndRole(organizationId, role));
        else if (organizationId != null)
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.findAllByOrganizationId(organizationId));
        else
        	return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.findAll());
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse create(@RequestBody @Valid UserDto user, HttpServletRequest req){
        log.info(String.format("received request to create user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        try {
        	UserDto result = userService.save(user, false, req.getLocale());	
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, result);                   	
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

	@PutMapping(value = "/update")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
	public ApiResponse update(@RequestBody @Valid UserDto user, HttpServletRequest req) {
        log.info(String.format("received request to update user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        
        try {
        	UserDto result = userService.save(user, true, req.getLocale());	
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, result);                   	
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
	}

	@PutMapping(value = "/updatepassword")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
	public ApiResponse updatePwd(@RequestBody @Valid PasswordDto password) {
        log.info(String.format("received request to update password %s", password.getUsername()));
        
        try {
            Credentials user = userService.findUserByUsername(password.getUsername());
            
            if (!userService.checkIfValidOldPassword(user, password.getOldPassword())) {
//                throw new InvalidOldPasswordException();
                return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, "Vecchia password errata");
            }
            userService.changeUserPassword(user, password.getNewPassword());

            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS);                   	
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
	}

	@GetMapping(value = "/get/{id}")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse getUser(@PathVariable long id){
        log.info(String.format("received request to get user %d", id));
        
        try {
        	UserDto result = userService.findOne(id);	
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, result);                   	
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

	@GetMapping(value = "/getbyusername/{username}")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse getUserByUsername(@PathVariable String username){
        log.info(String.format("received request to get user %s", username));
        
        try {
        	UserDto result = userService.find(username);	
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, result);                   	
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

	@GetMapping(value = "/exist/{id}")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse existUser(@PathVariable long id){
        log.info(String.format("received request to exist user %d", id));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, userService.existsById(id));
    }

	@DeleteMapping(value = "/delete/{id}")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse delete(@PathVariable(value = "id") Long id){
        log.info(String.format("received request to delete user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        try {
            userService.delete(id);
            return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, null);                   	
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.getMessage());        	
        }
    }

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
	public ApiResponse uploadUsers(
			@RequestParam("file") MultipartFile file,
			@RequestParam("token") String token)
			throws ServletException, IOException {
    	
	   log.info("upload-users invoked.");
 
	   ApiResponse resp = null;
 
	   final String label = UUID.randomUUID().toString() + ".xlsx";
	   final String filepath = "/tmp/" + label;
	   byte[] bytes = file.getBytes();
	   File fh = new File("/tmp/");
	   if(!fh.exists()){
		  fh.mkdir();
	   }
 
	   try {
			FileOutputStream writer = new FileOutputStream(filepath);
			writer.write(bytes);
			writer.close();

			log.info("file bytes received: {}", bytes.length);

			executor.submit(() -> {
				try {
					userService.saveFromFile(filepath, token);
				}catch(Exception ex) {
					log.error("Failed on saving records", ex);
				}
			  });

			resp = new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, "Id: "+label);

	   }catch(IOException ex) {
			log.error("Failed to process the uploaded file", ex);

			resp = new ApiResponse(HttpStatus.OK, ApiResponse.ERROR, ex.toString());
		}

        return resp;
	}
    
    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    private SimpleMailMessage constructResetPasswordEmail(final Locale locale, final String newPassword, final Credentials user) {
        final String message = messages.getMessage("message.resetPassword", null, locale);
        return constructEmail("Reset Password", message + " : " + newPassword, user);
    }

    private SimpleMailMessage constructEmail(String subject, String body, Credentials user) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

}