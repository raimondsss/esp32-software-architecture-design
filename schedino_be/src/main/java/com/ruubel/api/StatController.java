package com.ruubel.api;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.threeten.bp.LocalDate;
import org.springframework.core.io.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;

import com.ruubel.api.response.CredentialsListResponse;
import com.ruubel.api.response.DashboardStatResponse;
import com.ruubel.api.response.LongSingleDataSeriesResponse;
import com.ruubel.api.response.MapDataSeriesResponse;
import com.ruubel.api.response.MultiDataSeriesResponse;
import com.ruubel.api.response.OperationResponse;
import com.ruubel.api.response.OperationResponse.ResponseStatusEnum;
import com.ruubel.api.response.SingleDataSeriesResponse;
import com.ruubel.data.LongSingleSeries;
import com.ruubel.data.MultiSeries;
import com.ruubel.data.SingleSeries;
import com.ruubel.dto.StatDto;
import com.ruubel.model.Credentials;
import com.ruubel.model.Pasto;
import com.ruubel.service.PastoService;
import com.ruubel.service.UserService;
import com.ruubel.utils.TimeUtils;
import com.google.common.base.Strings;
import org.apache.commons.io.IOUtils;
import org.json.*;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.Executors;
import java.text.SimpleDateFormat;


@RestController
@Transactional
@RequestMapping(value = "/schedino/api/v1")
@Api(tags = {"StatController"})
public class StatController {

	private final static Logger logger = LoggerFactory.getLogger(StatController.class);

	private String[] weekDays = {"DOM", "LUN", "MAR", "MER", "GIO", "VEN", "SAB"};
	
	@Autowired private JdbcTemplate jdbcTemplate;
	
	@Autowired private PastoService pastoService;
	
	@Autowired private UserService userService;


	@ApiOperation(value = "General Stats", response = DashboardStatResponse.class)
    @RequestMapping(value = "/dashboard-stats", method = RequestMethod.GET)
    public DashboardStatResponse geDashboardStats(
			@RequestParam(value = "compagniaId"  , required = false) Integer compagniaId,
	        @RequestParam(value = "startDate", required = false) String startDate,
	        @RequestParam(value = "endDate", required = false) String endDate,
	        @RequestParam(value = "credentialsId"  , required = false) Integer credentialsId
    		) {
    	
        DashboardStatResponse resp = new DashboardStatResponse();
        StatDto data = new StatDto();

        // counter utenti
        String sql = "select count(*) as value from credentials";   
        Integer value = jdbcTemplate.queryForObject(sql, Integer.class);
        data.setNumUtenti(value);
        
        // counter schedini
        sql = "select count(*) as value from schedini";
        value = jdbcTemplate.queryForObject(sql, Integer.class);
        data.setNumSchedini(value);

        // counter prodotti
        sql = "select count(*) as value from prodotti";        
        value = jdbcTemplate.queryForObject(sql, Integer.class);
        data.setNumProdotti(value);
        
        // counter calorie odierne
        sql = "select sum(calorie) as value from pasti where credentials_id = "+ credentialsId;
        sql += " AND data between '"+startDate+" 00:00:00' AND '" + endDate + " 23:59:59'";
        value = jdbcTemplate.queryForObject(sql, Integer.class);
        data.setCalorieTotali(value == null ? 0 : value);
       
        
        resp.setData(data);
        resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
        resp.setOperationMessage("");
        return resp;
    }
	
	@ApiOperation(value = "Week Stats", response = MapDataSeriesResponse.class)
    @RequestMapping(value = "/week-stats", method = RequestMethod.GET)
    public MapDataSeriesResponse getDashboardWeekStats(
			@RequestParam(value = "userId"  , required = false) Integer userId,
	        @RequestParam(value = "startDate", required = false) String startDate,
	        @RequestParam(value = "endDate", required = false) String endDate
    		) {

		MapDataSeriesResponse resp = new MapDataSeriesResponse();

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate+" 00:00:00");
			Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate+" 23:59:59");

			String sql = "SELECT SUM(calorie),nome,data ";
			sql += " FROM pasti";
			sql += " WHERE credentials_id="+userId;
			if (startDate != null && !startDate.equals("") && endDate != null && !endDate.equals(""))
				sql += " AND data between '"+startDate+" 00:00:00' AND '" + endDate + " 23:59:59'";
			sql += " GROUP BY nome,data";
			sql += " ORDER BY nome,data";
			
			List<Pasto> pasti = pastoService.findAll(userId, startDate, endDate);
			List<LinkedHashMap<String, Object>> dataItemList = new ArrayList<LinkedHashMap<String, Object>>();
			String prodottoCorrente = "";
			
			LinkedHashMap<String, Object> mapSeries = null;
			
			LinkedHashMap<String, Object> totalSeries = new LinkedHashMap<String, Object>();
			totalSeries.put("PRODOTTO", "TOTALE");
			Date current = startTime;
			while(current.before(endTime)) {
				totalSeries.put(sdf.format(current), 0D);
									
				cal.setTime(current);
				cal.add(Calendar.DATE, 1);
				current = cal.getTime();
			}

			for (Pasto pasto : pasti) {
				
				if (prodottoCorrente.equals("") || !prodottoCorrente.equals(pasto.getNome())) {
					if (!prodottoCorrente.equals("")) {
						dataItemList.add(mapSeries);
					}
						
					mapSeries = new LinkedHashMap<String, Object>();
					mapSeries.put("PRODOTTO", pasto.getNome());

					current = startTime;
					while(current.before(endTime)) {
						mapSeries.put(sdf.format(current), 0D);
											
						cal.setTime(current);
						cal.add(Calendar.DATE, 1);
						current = cal.getTime();
					}

				}
				
				prodottoCorrente = pasto.getNome();
				
				for (String key : mapSeries.keySet()) {
					if (key.equals(sdf.format(pasto.getData()))) {
						mapSeries.put(key, pasto.getCalorie());
						break;
					}
				}

				for (String key : totalSeries.keySet()) {
					if (key.equals(sdf.format(pasto.getData()))) {
						totalSeries.put(key, pasto.getCalorie() + (Double)totalSeries.get(key));
						break;
					}
				}
			}

			if(mapSeries!= null)
				dataItemList.add(mapSeries);
			
			dataItemList.add(totalSeries);
			
			resp.setItems(dataItemList);
	        resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
	        resp.setOperationMessage("");	
	        
		} catch(Exception ex) {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ex.toString());
			
		}

        return resp;
    }
	
	@ApiOperation(value = "Schedini Associati", response = CredentialsListResponse.class)
    @RequestMapping(value = "/schedini-associati", method = RequestMethod.GET)
    public CredentialsListResponse getSchediniAssociati(
    		) {

		CredentialsListResponse resp = new CredentialsListResponse();
		
		try {
			
			List<Credentials> utenti = userService.findAllAssociati();
					
			resp.setItems(utenti);
	        resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
	        resp.setOperationMessage("");	
	        
		} catch(Exception ex) {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage(ex.toString());
			
		}

        return resp;
    }
 }
