package com.ruubel.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.ruubel.dto.ApiResponse;
import com.ruubel.dto.RoleDto;
import com.ruubel.error.InvalidOldPasswordException;
import com.ruubel.service.AuthenticationFacadeService;
import com.ruubel.service.AuthorityService;
import com.ruubel.utils.Utils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;


@RestController
@RequestMapping("/schedino/api/v1/authorities")
public class AuthorityController {
	static final Logger log = LoggerFactory.getLogger(AuthorityController.class);

    @Autowired
    private AuthorityService authorityService;

     @Autowired
    private AuthenticationFacadeService authenticationFacadeService;


     @GetMapping("/list")
    public ApiResponse list(){
        log.info(String.format("received request to list role %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, authorityService.findAll());
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse create(@RequestBody @Valid RoleDto role){
        log.info(String.format("received request to create role %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, authorityService.save(role));
    }

	@RequestMapping(value = "/role")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
	public ApiResponse update(@RequestBody @Valid RoleDto role) {
        log.info(String.format("received request to update role %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, authorityService.save(role));		
	}

	@GetMapping(value = "/get/{id}")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public ApiResponse get(@PathVariable long id){
        log.info(String.format("received request to update role %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        return new ApiResponse(HttpStatus.OK, ApiResponse.SUCCESS, authorityService.findOne(id));
    }

    @DeleteMapping(value = "/delete/{id}")
    @PreAuthorize("hasRole('ROLE_OAUTH_ADMIN')")
    public void delete(@PathVariable(value = "id") Long id){
        log.info(String.format("received request to delete role %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        authorityService.delete(id);
    }

}