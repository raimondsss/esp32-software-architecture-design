package com.ruubel.api.response;

import com.ruubel.model.Schedino;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchedinoListResponse extends PageResponse {
  @ApiModelProperty(required = true, value = "")
  private List<Schedino> items;
  public List<Schedino> getItems() {return items;}
  public void setItems(List<Schedino> list) {this.items = list;}
}