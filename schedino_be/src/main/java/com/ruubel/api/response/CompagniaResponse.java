package com.ruubel.api.response;

import com.ruubel.model.Compagnia;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CompagniaResponse extends OperationResponse {
  private Compagnia data = new Compagnia();
}
