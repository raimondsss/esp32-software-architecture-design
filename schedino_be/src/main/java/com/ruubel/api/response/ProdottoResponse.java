package com.ruubel.api.response;

import com.ruubel.model.Prodotto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ProdottoResponse extends OperationResponse {
  private Prodotto data = new Prodotto();
}
