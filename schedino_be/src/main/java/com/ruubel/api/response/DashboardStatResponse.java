package com.ruubel.api.response;

import com.ruubel.dto.StatDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class DashboardStatResponse extends OperationResponse {
  private StatDto data = new StatDto();
}
