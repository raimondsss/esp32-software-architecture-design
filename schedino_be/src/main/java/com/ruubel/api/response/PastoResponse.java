package com.ruubel.api.response;

import com.ruubel.model.Pasto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PastoResponse extends OperationResponse {
  private Pasto data = new Pasto();
}
