package com.ruubel.api.response;

import com.ruubel.model.Compagnia;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class CompagniaListResponse extends PageResponse {
  @ApiModelProperty(required = true, value = "")
  private List<Compagnia> items;
  public List<Compagnia> getItems() {return items;}
  public void setItems(List<Compagnia> list) {this.items = list;}
}