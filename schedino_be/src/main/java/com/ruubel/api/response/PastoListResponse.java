package com.ruubel.api.response;

import com.ruubel.model.Pasto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class PastoListResponse extends PageResponse {
  @ApiModelProperty(required = true, value = "")
  private List<Pasto> items;
  public List<Pasto> getItems() {return items;}
  public void setItems(List<Pasto> list) {this.items = list;}
}