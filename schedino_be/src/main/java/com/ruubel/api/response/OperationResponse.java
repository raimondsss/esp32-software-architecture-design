/**
    This is the common structure for all responses
    if the response contains a list(array) then it will have 'items' field
    if the response contains a single item then it will have 'item'  field
 */
package com.ruubel.api.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data //for getters and setters
public class OperationResponse {
  public enum ResponseStatusEnum {SUCCESS, ERROR, WARNING, NO_ACCESS};
  @ApiModelProperty(required = true)
  private ResponseStatusEnum  operationStatus;
  private String  operationMessage;
  private String  operationResult;
  
  public void setOperationStatus(ResponseStatusEnum operationStatus) {
	this.operationStatus = operationStatus;
  }

  public void setOperationMessage(String operationMessage) {
	this.operationMessage = operationMessage;
  }

  public void setOperationResult(String operationResult) {
	this.operationResult = operationResult;
  }
}
