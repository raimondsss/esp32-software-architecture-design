package com.ruubel.api.response;

import com.ruubel.model.Prodotto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class ProdottoListResponse extends PageResponse {
  @ApiModelProperty(required = true, value = "")
  private List<Prodotto> items;
  public List<Prodotto> getItems() {return items;}
  public void setItems(List<Prodotto> list) {this.items = list;}
}