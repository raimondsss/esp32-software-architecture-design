package com.ruubel.api.response;

import com.ruubel.model.Schedino;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SchedinoResponse extends OperationResponse {
  private Schedino data = new Schedino();
}
