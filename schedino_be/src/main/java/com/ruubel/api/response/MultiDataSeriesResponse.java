package com.ruubel.api.response;

import com.ruubel.data.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class MultiDataSeriesResponse extends OperationResponse {
    private List<MultiSeries> items;
}
