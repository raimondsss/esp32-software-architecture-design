package com.ruubel.api.response;

import java.util.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import lombok.Getter;
import lombok.Setter;

//@Data //for getters and setters
public class ListResponse extends OperationResponse {
  @Getter @Setter private long totalItems;
  @Getter @Setter private Sort sort;
  private List items;

  public void setTotalItems(int totalItems){
    this.totalItems = totalItems;
  }

  public void setSort(Sort sort){
    this.sort = sort;
  }

}
