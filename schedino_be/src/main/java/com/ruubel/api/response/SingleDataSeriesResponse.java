//This is a common http response model for providing data series

package com.ruubel.api.response;

import com.ruubel.data.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class SingleDataSeriesResponse extends OperationResponse {
    private List<SingleSeries> items;
}
