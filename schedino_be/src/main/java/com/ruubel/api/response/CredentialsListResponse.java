package com.ruubel.api.response;

import com.ruubel.model.Credentials;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class CredentialsListResponse extends PageResponse {
  @ApiModelProperty(required = true, value = "")
  private List<Credentials> items;
  public List<Credentials> getItems() {return items;}
  public void setItems(List<Credentials> list) {this.items = list;}
}