package com.ruubel.event.listener;

import java.util.Date;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.ruubel.service.SendGridService;
import com.ruubel.dto.EmailDto;
import com.ruubel.dto.UserDto;
import com.ruubel.event.OnGenerateAccountEvent;
import com.ruubel.mail.ISendMail;
import com.ruubel.model.EmailQueue;
import com.ruubel.repository.EmailQueueRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


@Component
@Configuration
public class GenerateAccountListener implements ApplicationListener<OnGenerateAccountEvent> {

	private final static Logger logger = LoggerFactory.getLogger(GenerateAccountListener.class);

	@Autowired
	private ApplicationContext context;

    @Autowired
    private TemplateEngine templateEngine;
    
    @Autowired
    private Environment env;

    @Autowired
    private EmailQueueRepo emailQueueRepository;

	@Value("${app.mail.classname}")
    private String sendMailClass;

	// API

    @Override
    public void onApplicationEvent(final OnGenerateAccountEvent event) {
    	try {
            this.sendEmail(event);    
            
			EmailQueue emailQueueToUpdate = emailQueueRepository.findOneById(event.getEmailQueue().getId());
			emailQueueToUpdate.setSent(true);
			emailQueueToUpdate.setSentDate(new Date());
			emailQueueRepository.save(emailQueueToUpdate);

    	} catch (Exception ex) {
    		logger.error("Error: "+ex);
        }
    }

    private void sendEmail(final OnGenerateAccountEvent event) throws Exception {
    	ISendMail sendMail = (ISendMail)context.getBean(Class.forName(sendMailClass));
		sendMail.send(event.getEmailQueue());
    }    

    /*
    private final MimeMessage constructEmailtemplateMessage(final OnGenerateAccountEvent event, final UserDto user) throws MessagingException {
    	Context context = new Context();
        context.setVariable("user", user);

        String process = templateEngine.process("new_account", context);
 
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setSubject("Credenziali per l'accesso alla piattaforma CMS");
        helper.setText(process, true);
        helper.setFrom(env.getProperty("support.email"));
        helper.setTo(user.getEmail());
        
        //FileSystemResource logoImage = new FileSystemResource("/templates/logo.png");
        ClassPathResource imageSource = new ClassPathResource("templates/logo.png");       
        helper.addInline("logoImage", imageSource, "image/png");

        return mimeMessage;
        
    }
	*/
}
