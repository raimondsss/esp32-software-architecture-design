package com.ruubel.event.listener;

import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.ruubel.service.UserService;
import com.ruubel.service.SendGridService;
import com.ruubel.dto.EmailDto;
import com.ruubel.model.Credentials;
import com.ruubel.event.OnResetPasswordCompleteEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


@Component
@Configuration
public class ResetPasswordListener implements ApplicationListener<OnResetPasswordCompleteEvent> {
	
	private final static Logger logger = LoggerFactory.getLogger(ResetPasswordListener.class);

	@Autowired
    private UserService service;

    @Autowired
    private TemplateEngine templateEngine;
    
    @Autowired
    private MessageSource messages;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment env;

	@Autowired
	SendGridService sendGridService;

	@Value("${app.registration.templatemail}")
    private String templateId;
	
	@Value("${app.support.mail}")
    private String supportEmail;

	// API

    @Override
    public void onApplicationEvent(final OnResetPasswordCompleteEvent event) {
    	try {
            this.confirmResetPassword(event);
    	} catch (Exception ex) {
    		logger.error("Error: "+ex);
        }
    }

    private void confirmResetPassword(final OnResetPasswordCompleteEvent event) throws Exception {
        final Credentials user = event.getUser();
        final String tmpPassword = event.getTmpPassword();

        //final SimpleMailMessage email = constructEmailMessage(event, user, token);
        //final MimeMessage email = constructEmailtemplateMessage(event, user);
        //mailSender.send(email);
        
        final EmailDto email = constructSendGridEmailMessage(event, user, tmpPassword);
        sendGridService.sendMail(email);
    }    

    

    /*private final SimpleMailMessage constructEmailMessage(final OnResetPasswordCompleteEvent event, final Credentials user, final String tmpPassword) {
        final String recipientAddress = user.getEmail();
        final String subject = "Reset password Confirmation";
        final String newPassword = "Temporary password=" + tmpPassword;
        final String message = messages.getMessage("message.resetSucc", null, "Reset password successfully.", event.getLocale());
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + newPassword);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }
    */
    
    private final EmailDto constructSendGridEmailMessage(final OnResetPasswordCompleteEvent event, final Credentials user, final String tmpPassword) {
    	EmailDto email = new EmailDto();
    	email.setToEmail(user.getEmail());
    	email.setEmailSubject("Reset password");
    	email.setFromEmail(env.getProperty("support.email"));
        final String newPassword = "Password temporanea=" + tmpPassword;
        final String message = messages.getMessage("message.resetPasswordSucc", null, "Reset password avvenuto con successo.", event.getLocale());
    	email.setMessage(message + " \r\n" + newPassword);
    	email.setTemplateId(templateId);
    	
        return email;
    }

    private final MimeMessage constructEmailtemplateMessage(final OnResetPasswordCompleteEvent event, final Credentials user) throws MessagingException {
    	Context context = new Context();
    	user.setPassword(event.getTmpPassword());
        context.setVariable("user", user);

        String process = templateEngine.process("reset_password", context);
 
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setSubject("Nuove credenziali per l'accesso alla piattaforma Scuola Lit For Education Classe");
        helper.setText(process, true);
        helper.setFrom(supportEmail);
        helper.setTo(user.getEmail());
        
        //FileSystemResource logoImage = new FileSystemResource("/templates/logo.png");
        ClassPathResource imageSource = new ClassPathResource("templates/logo.png");       
        helper.addInline("logoImage", imageSource, "image/png");

        return mimeMessage;
        
    }

}
