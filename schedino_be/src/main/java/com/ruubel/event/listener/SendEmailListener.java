package com.ruubel.event.listener;

import java.util.Date;

import com.ruubel.event.OnSendEmailEvent;
import com.ruubel.mail.ISendMail;
import com.ruubel.model.EmailQueue;
import com.ruubel.repository.EmailQueueRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class SendEmailListener implements ApplicationListener<OnSendEmailEvent> {

	private final static Logger logger = LoggerFactory.getLogger(SendEmailListener.class);

	@Autowired
	private ApplicationContext context;
	
    @Autowired
    private EmailQueueRepo emailQueueRepository;

	@Value("${app.mail.classname}")
    private String sendMailClass;

	// API

    @Override
    public void onApplicationEvent(final OnSendEmailEvent event) {
    	try {
            this.sendEmail(event);    
            
			EmailQueue emailQueueToUpdate = emailQueueRepository.findOneById(event.getEmailQueue().getId());
			emailQueueToUpdate.setSent(true);
			emailQueueToUpdate.setSentDate(new Date());
			emailQueueRepository.save(emailQueueToUpdate);

		} catch (Exception ex) {
			logger.error("Error: "+ex);
	    }

    }

    private void sendEmail(final OnSendEmailEvent event) throws Exception {
    	
    	ISendMail sendMail = (ISendMail)context.getBean(Class.forName(sendMailClass));
		sendMail.send(event.getEmailQueue());		
    }    

}
