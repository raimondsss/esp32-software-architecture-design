package com.ruubel.event.listener;

import java.util.UUID;

import com.ruubel.service.UserService;
import com.ruubel.service.SendGridService;
import com.ruubel.dto.EmailDto;
import com.ruubel.model.Credentials;
import com.ruubel.event.OnRegistrationCompleteEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

	private final static Logger logger = LoggerFactory.getLogger(RegistrationListener.class);

    @Autowired
    private UserService service;

    @Autowired
    private MessageSource messages;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment env;

	@Autowired
	SendGridService sendGridService;

	@Value("${app.registration.templatemail}")
    private String templateId;
	
	@Value("${app.support.mail}")
    private String supportEmail;

	// API

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
    	try {
	        this.confirmRegistration(event);
		} catch (Exception ex) {
			logger.error("Error: "+ex);
	    }
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) throws Exception {
        final Credentials user = event.getUser();
        final String token = UUID.randomUUID().toString();
        service.createVerificationTokenForUser(user, token);

        //final SimpleMailMessage email = constructEmailMessage(event, user, token);
        //mailSender.send(email);
        
        final EmailDto email = constructSendGridEmailMessage(event, user, token);
        sendGridService.sendMail(email);
    }    

    //

    private final SimpleMailMessage constructEmailMessage(final OnRegistrationCompleteEvent event, final Credentials user, final String token) {
        final String recipientAddress = user.getEmail();
        final String subject = "Registration Confirmation";
        final String confirmationUrl = event.getAppUrl() + "/registrationConfirm.html?token=" + token;
        final String message = messages.getMessage("message.regSucc", null, "You registered successfully. We will send you a confirmation message to your email account.", event.getLocale());
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }
    
    private final EmailDto constructSendGridEmailMessage(final OnRegistrationCompleteEvent event, final Credentials user, final String token) {
    	EmailDto email = new EmailDto();
    	email.setToEmail( user.getEmail());
    	email.setEmailSubject("Registration Confirmation");
    	email.setFromEmail(supportEmail);
        final String confirmationUrl = event.getAppUrl() + "/registrationConfirm.html?token=" + token;
        final String message = messages.getMessage("message.regSucc", null, "You registered successfully. We will send you a confirmation message to your email account.", event.getLocale());
    	email.setMessage(message + " \r\n" + confirmationUrl);
    	email.setTemplateId(templateId);
    	
        return email;
    }

}
