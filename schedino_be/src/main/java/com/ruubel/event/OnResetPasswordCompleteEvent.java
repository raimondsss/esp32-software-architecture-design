package com.ruubel.event;

import java.util.Locale;

import com.ruubel.model.Credentials;
import org.springframework.context.ApplicationEvent;

@SuppressWarnings("serial")
public class OnResetPasswordCompleteEvent extends ApplicationEvent {

    private final String appUrl;
    private final Locale locale;
    private final Credentials user;
    private final String tmpPassword;

    public OnResetPasswordCompleteEvent(final Credentials user, String tmpPassword, final Locale locale, final String appUrl) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
        this.tmpPassword = tmpPassword;
    }

    //

    public String getAppUrl() {
        return appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public Credentials getUser() {
        return user;
    }
    
    public String getTmpPassword() {
    	return tmpPassword;
    }

}
