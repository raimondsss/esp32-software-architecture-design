package com.ruubel.event;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

import com.ruubel.dto.EmailMessageDto;
import com.ruubel.model.EmailQueue;

@SuppressWarnings("serial")
public class OnSendEmailEvent extends ApplicationEvent {

    private final EmailQueue emailQueue;

    public OnSendEmailEvent(final EmailQueue emailQueue) {
        super(emailQueue);
        this.emailQueue = emailQueue;
    }

	public EmailQueue getEmailQueue() {
		return emailQueue;
	}
}
