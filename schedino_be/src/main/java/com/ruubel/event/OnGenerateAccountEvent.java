package com.ruubel.event;

import org.springframework.context.ApplicationEvent;

import com.ruubel.model.EmailQueue;

@SuppressWarnings("serial")
public class OnGenerateAccountEvent extends ApplicationEvent {

    private final EmailQueue emailQueue;

    public OnGenerateAccountEvent(final EmailQueue emailQueue) {
        super(emailQueue);
        this.emailQueue = emailQueue;
    }

	public EmailQueue getEmailQueue() {
		return emailQueue;
	}

}
