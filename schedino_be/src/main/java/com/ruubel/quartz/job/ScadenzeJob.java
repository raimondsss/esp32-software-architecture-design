package com.ruubel.quartz.job;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
/*
import com.ruubel.service.ContrattoService;
import com.ruubel.service.SviluppoService;*/

import com.ruubel.service.MessageService;

public class ScadenzeJob implements Job {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/*@Autowired private ContrattoService contrattoService;
	
	@Autowired private SviluppoService sviluppoService;*/

    @Autowired
	private MessageService messagingService;
    
    private static boolean alreadyStarted = false;

    /**
     * <p>
     * Called by the <code>{@link org.quartz.Scheduler}</code> when a
     * <code>{@link org.quartz.Trigger}</code> fires that is associated with
     * the <code>Job</code>.
     * </p>
     * 
     * @throws JobExecutionException
     *             if there is an exception while executing the job.
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
	  run();
	  try {
//		  JobDetail jobDetail = context.getScheduler().getJobDetail(new JobKey("Scadenze Trigger"));
//		  context.getScheduler().deleteJob(jobDetail.getKey());
		  Trigger trigger = context.getScheduler().getTrigger(new TriggerKey("Scadenze Trigger"));
		  context.getScheduler().deleteJob(trigger.getJobKey());
		  
	  } catch(Exception ex) {
		  
	  }
	}
    
    public void run() {
      try {
    	  log.info("Start ScadenzeJob processing........");
 
    	  /*sviluppoService.checkSviluppi();
    	  contrattoService.checkContratti();*/
    	  final String topic = "Bilancia/Salva";

    	  if (!alreadyStarted) {
        	  log.info("Subscribe topic "+topic);
    		  alreadyStarted = true;
        	  messagingService.mqttMessageRcvd(topic);
    	  }
    	  
    	  log.info("Fine ScadenzeJob........");
      } catch (Exception e) {
        log.error("ScadenzeJob error", e);
        //System.out.println(Utils.datetime2String(new java.util.Date())+"-Error Batch processing "+e);	          
        e.printStackTrace();
        
      } finally{
        log.info("ScadenzeJob processing ended");
      }		  
    }
}
