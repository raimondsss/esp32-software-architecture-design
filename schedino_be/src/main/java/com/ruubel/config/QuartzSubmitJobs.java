package com.ruubel.config;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import com.ruubel.quartz.job.ScadenzeJob;


@Configuration
public class QuartzSubmitJobs {
    private static final String CRON_EVERY_MINUTE = "0 * * ? * *";
    private static final String CRON_EVERY_FIVE_MINUTES = "0 0/5 * ? * * *";
    private static final String CRON_EVERY_DAY = "0 0 12 ? * * ";


    @Bean(name = "scadenze")
    public JobDetailFactoryBean jobDogana() {
        return QuartzConfig.createJobDetail(ScadenzeJob.class, "Scadenze Job");
    }

    @Bean(name = "scadenzeTrigger")
    public SimpleTriggerFactoryBean triggerDogana(@Qualifier("scadenze") JobDetail jobDetail) {
        return QuartzConfig.createOneTrigger(jobDetail, 2000L, 1L, "Scadenze Trigger");
    }

}