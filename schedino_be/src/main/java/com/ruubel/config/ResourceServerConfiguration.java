package com.ruubel.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends
        ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "api-resource";

	private static final String[] AUTH_WHITELIST = {
            // other public endpoints of your API may be appended to this array
			"/h2/**",
			"/api/signup", 
			"/api/signup/**",
			"/api/signup/authorize/**",
			"/api/login",
			"/schedino/api/v1/users/resetPassword",
			"/schedino/api/v1/users/resetPasswordByEmail",			
			"/schedino/api/v1/registration/registerUser",
			"/schedino/api/v1/registration/registerConfirm",
			"/schedino/api/v1/registration/registerSocialUser",
			"/schedino/api/v1/social/google",
			"/schedino/api/v1/social/facebook",
			"/schedino/filestorage/**",
			"/schedino/api/v1/schedini"
    };
	
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
    	/*
        http
                .authorizeRequests()
                .antMatchers("/api/v{[0-9]+}/user").hasRole("USER")
                .anyRequest().authenticated()
        ;
        */
        http
		.csrf().disable()
		.cors().and()
//        .addFilterBefore(new MyCorsFilter(), ChannelProcessingFilter.class)
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(AUTH_WHITELIST).permitAll()
        .anyRequest().authenticated()
        .and()
        .exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
    }
}
