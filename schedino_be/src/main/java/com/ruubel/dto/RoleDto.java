package com.ruubel.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.ruubel.validation.PasswordMatches;
import com.ruubel.validation.ValidEmail;

@PasswordMatches
public class RoleDto {

    private long id;

    @NotNull
    @NotEmpty
    private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
