package com.ruubel.dto;

import lombok.Data;

@Data
public class StatDto {
	
	private int numUtenti;
	private int numSchedini;
	private int numProdotti;
	
	private int calorieTotali;
	
	
}
