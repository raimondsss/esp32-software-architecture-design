package com.ruubel.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.ruubel.model.Credentials;
import com.ruubel.validation.PasswordMatches;
import com.ruubel.validation.ValidEmail;

@PasswordMatches
public class UserDto {

    private long id;

    @NotNull
    @NotEmpty
    private String username;

//    @NotNull
//    @NotEmpty
    private String firstName;

//    @NotNull
//    @NotEmpty
    private String lastName;
    
    private Integer organizationId;
    
    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;
    

    private boolean isUsing2FA;

    private boolean enabled;

    private boolean changePassword;
    
    private String avatar;

    private List<String> role;
    
    private String roleDescription;

    private String loginType;
    
    private String userId;
    
    private String token;
    
    private String landingPage;

    private boolean privacy;
    
    private Date    privacyDate;

    private String access_token;
    private String token_type;
    private String refresh_token;
    private String expires_in;
    private String scope;
    
    private Integer obbiettivoGiornaliero;
    private String schedino;

    
    public UserDto() {
    	
    }
    
    public UserDto(Credentials credential) {
    	setEmail(credential.getEmail());
    	setFirstName(credential.getFirstName());
    	setLastName(credential.getLastName());
    	setUsername(credential.getUsername());
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUsing2FA() {
        return isUsing2FA;
    }

    public void setUsing2FA(boolean isUsing2FA) {
        this.isUsing2FA = isUsing2FA;
    }

    public List<String> getRole() {
        return role;
    }

    public void setRole(List<String> role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isChangePassword() {
		return changePassword;
	}

	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	public boolean isPrivacy() {
		return privacy;
	}

	public void setPrivacy(boolean privacy) {
		this.privacy = privacy;
	}

	public Date getPrivacyDate() {
		return privacyDate;
	}

	public void setPrivacyDate(Date privacyDate) {
		this.privacyDate = privacyDate;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public Integer getObbiettivoGiornaliero() {
		return obbiettivoGiornaliero;
	}

	public void setObbiettivoGiornaliero(Integer obbiettivoGiornaliero) {
		this.obbiettivoGiornaliero = obbiettivoGiornaliero;
	}

	public String getSchedino() {
		return schedino;
	}

	public void setSchedino(String schedino) {
		this.schedino = schedino;
	}
}
