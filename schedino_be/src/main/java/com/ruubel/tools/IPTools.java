package com.ruubel.tools;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class IPTools {

	@SuppressWarnings("finally")
	public static long convertIPtodecimaim(String ip) {
		try {
			long result = 0;
			String[] atoms = ip.split("\\.");

			for (int i = 3; i >= 0; i--) {
				result |= (Long.parseLong(atoms[3 - i]) << (i * 8));
			}

			return result & 0xFFFFFFFF;
		} catch (NumberFormatException e) {
			log.error(ip);
		} finally {
			return 0;
		}
	}

}
