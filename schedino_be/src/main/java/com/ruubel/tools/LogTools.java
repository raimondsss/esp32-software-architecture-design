package com.ruubel.tools;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LogTools {

	public static Map<String, Object> createParametersMap(String key, Object object) {
		Map<String, Object> paramterMap = new HashMap<String, Object>();
		paramterMap.put(key, object);
		return paramterMap;
	}

	public static void logException(Logger logger, String methodName, Exception e) {
		logger.error("ERROR BEGINS *************");
		logger.error("TIMESTAMP: " + new Date());

		logger.error("METHOD: " + methodName);

		logger.error("MESSAGE: " + e.getMessage());

		StringWriter sWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(sWriter));
		logger.error("STACKTRACE: " + sWriter.getBuffer().toString());

		logger.error("ERROR ENDS   *************");
	}

	public static void logException(Logger logger, String methodName, Map<String, Object> keyValueMap, Exception e) {
		logger.error("ERROR BEGINS *************");
		logger.error("TIMESTAMP: " + new Date());

		logger.error("METHOD: " + methodName);
		for (String key : keyValueMap.keySet()) {
			logger.error("Parameter: " + key);
			if (keyValueMap.get(key) != null)
				logger.error("Value: " + keyValueMap.get(key).toString());
			else
				logger.error("Value: null");
		}

		logger.error("MESSAGE: " + e.getMessage());

		StringWriter sWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(sWriter));
		logger.error("STACKTRACE: " + sWriter.getBuffer().toString());

		logger.error("ERROR ENDS   *************");
	}

	public static void logException(String methodName, Map<String, Object> keyValueMap, Exception e) {
		log.error("ERROR BEGINS *************");
		log.error("TIMESTAMP: " + new Date());

		log.error("METHOD: " + methodName);
		for (String key : keyValueMap.keySet()) {
			log.error("Parameter: " + key);
			if (keyValueMap.get(key) != null)
				log.error("Value: " + keyValueMap.get(key).toString());
			else
				log.error("Value: null");
		}

		log.error("MESSAGE: " + e.getMessage());

		StringWriter sWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(sWriter));
		log.error("STACKTRACE: " + sWriter.getBuffer().toString());

		log.error("ERROR ENDS   *************");
	}

}
