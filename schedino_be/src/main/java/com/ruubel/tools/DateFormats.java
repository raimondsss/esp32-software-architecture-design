package com.ruubel.tools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

@Component
public class DateFormats {
	
	public static String formatDateTime(Date insertDate) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy 'at' HH:mm");
		String formattedDate = df.format(insertDate);
		return formattedDate;
	}
	
	public static String formatDateForInvoice(Date insertDate) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = df.format(insertDate);
		return formattedDate;
	}
	
	public static String formatDateForTwitterSearch(Date insertDate) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = df.format(insertDate);
		return formattedDate;
	}
	
	public static String formatDateTimeBySetting(Date insertDate, String pattern, String mode, Locale locale, String timeZone) {
		
		String patternToUse=null;
		
		if (mode.equals("full")) {
			patternToUse=pattern;
		} else if (mode.equals("date")) {
			String[] datePatternSplit = pattern.split(" ");
			patternToUse=datePatternSplit[0];
		} else if (mode.equals("time")) {
			String[] datePatternSplit = pattern.split(" ");
			patternToUse=datePatternSplit[1];
		}
		
		SimpleDateFormat df = new SimpleDateFormat(patternToUse,locale);
		
		if (timeZone != null && !timeZone.equals("")) {
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
		}
		
		String formattedDate = df.format(insertDate);
		return formattedDate;
	}
	
}
