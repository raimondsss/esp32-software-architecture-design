package com.ruubel.tools;

import java.io.File;
import java.io.FileInputStream;

import org.springframework.stereotype.Component;

import com.ruubel.exception.ApplicationServiceException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ImageService {

	public static byte[] loadImage(String filePath) throws ApplicationServiceException {
		try {
			File file = new File(filePath);
			int size = (int) file.length();
			byte[] buffer = new byte[size];
			FileInputStream in = new FileInputStream(file);
			try {
				in.read(buffer);
			} finally {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return buffer;
		} catch (Exception e) {
			LogTools.logException(log, "LoadImage", LogTools.createParametersMap("filePath", filePath), e);
			throw new ApplicationServiceException("A problem occurred while accessing system file/path.");
		}
	}

}
