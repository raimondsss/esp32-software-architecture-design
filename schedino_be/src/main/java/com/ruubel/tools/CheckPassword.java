package com.ruubel.tools;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class CheckPassword {

	/*
	 * Password should be less than 15 and more than 8 characters in length.
	 * Password should contain at least one upper case and one lower case alphabet.
	 * Password should contain at least one number. Password should contain at least
	 * one special character.
	 */

	public static String passwordValidation(String userName, String password) {
		String valid = "";
		if (password.length() > 30 || password.length() < 5) {
			valid = "Password should be less than 30 and more than 5 characters in length.";
		}

//            if (password.indexOf(userName) > -1)
//            {
//            	valid ="Password Should not be same as user name";
//            }

		String upperCaseChars = "(.*[A-Z].*)";
//            if (!password.matches(upperCaseChars ))
//            {
//            	valid ="Password should contain at least one upper case alphabet";
//            }
//            
		String lowerCaseChars = "(.*[a-z].*)";
//            if (!password.matches(lowerCaseChars ))
//            {
//            	valid ="Password should contain at least one lower case alphabet";
//            }
//            
		String numbers = "(.*[0-9].*)";
//            if (!password.matches(numbers ))
//            {
//            	valid ="Password should contain atleast one number.";
//            }

		if (!password.matches(numbers) || !password.matches(upperCaseChars) || !password.matches(lowerCaseChars)) {
			valid = "Password should contain atleast one number.";
		}

//            String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
//            if (!password.matches(specialChars ))
//            {
//            	valid ="Password should contain atleast one special character";
//            }

		return valid;
	}

	public static boolean isPasswordValid(CharSequence password) {

//    	String regex = "[A-Z0-9a-z]{6,30}";
		String regex = "^[A-Z0-9a-z!-~]{6,30}";

		// String regex = "^(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$";

		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(password).matches();
	}
}
