package com.ruubel.tools;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;

@Component
public class MD5Tools {
	
	public static String generateActivationToken(String token) {
		MessageDigest md;
		
		try {
			md = MessageDigest.getInstance("MD5");

			md.update(token.getBytes());

			byte byteData[] = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			token = sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return token;
	}
	
	 public static String hex(byte[] array) {
	      StringBuffer sb = new StringBuffer();
	      for (int i = 0; i < array.length; ++i) {
		  sb.append(Integer.toHexString((array[i]
		      & 0xFF) | 0x100).substring(1,3));        
	      }
	      return sb.toString();
	  }
	 
	  public static String md5Hex (String message) {
	      try {
		  MessageDigest md = 
		      MessageDigest.getInstance("MD5");
		  return hex (md.digest(message.getBytes("CP1252")));
	      } catch (NoSuchAlgorithmException e) {
	      } catch (UnsupportedEncodingException e) {
	      }
	      return null;
	  }
	
	  public static String getHash(String text) {
		  try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.update(text.getBytes(), 0, text.length());
			return "" + new BigInteger(1, m.digest()).toString(16);
	      } catch (Exception e) {
	      }
	      return null;
	  }
}
