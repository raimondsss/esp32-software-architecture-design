package com.ruubel.tools;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class HttpTools {
	
	public static String generateUrl(HttpServletRequest req) {
		String domain = "";
		if (req.getServerName().equals("localhost")) {
			// for debugging purpose only
			domain = "http://" + req.getServerName() + ":4200";
		} else {
			domain = "https://" + req.getServerName();
		}
		String url = domain + req.getContextPath();

		return url;
	}
	
}
