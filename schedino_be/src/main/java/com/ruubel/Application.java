package com.ruubel;

import com.ruubel.service.MessageService;
import com.ruubel.service.UserService;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.boot.jdbc.DataSourceBuilder;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application implements CommandLineRunner {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    //@Autowired
	//private MessageService messagingService;
    
	@Autowired
	private ConfigurableApplicationContext context;

    //private PasswordEncoder passwordEncoder;
    //private UserService userService;

    /*
    @Autowired
    public Application(PasswordEncoder passwordEncoder, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }
    */

	@Bean
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource mainDataSource() {
		//return DataSourceBuilder.create().type(HikariDataSource.class).build();
		return DataSourceBuilder.create().build();
	}

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
	public void run(String... args) throws Exception {
		final String topic = "Bilancia/Salva";

		//messagingService.subscribe(topic);

		//messagingService.publish(topic, "Hi\nThis is a sample message published to topic roytuts/topic/event", 0, true);

		//context.close();
	}
    
}
