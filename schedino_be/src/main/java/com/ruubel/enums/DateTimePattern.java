package com.ruubel.enums;

public enum DateTimePattern {
	
	//slash
	
	ddmmyyyy_hhmmss_slash {
	    public String toString() {
	        return "dd/MM/yyyy HH:mm";
	    }
	},
	
	mmddyyyy_hhmmss_slash {
	    public String toString() {
	        return "MM/dd/yyyy HH:mm";
	    }
	},
	
	yyyyddmm_hhmmss_slash {
	    public String toString() {
	        return "yyyy/dd/MM HH:mm";
	    }
	},
	
	
	//point
	ddmmyyyy_hhmmss_point {
	    public String toString() {
	        return "dd.MM.yyyy HH:mm";
	    }
	},
	
	mmddyyyy_hhmmss_point {
	    public String toString() {
	        return "MM.dd.yyyy HH:mm";
	    }
	},
	yyyyddmm_hhmmss_point {
	    public String toString() {
	        return "yyyy.dd.MM HH:mm";
	    }
	},
	
	//dash
	ddmmyyyy_hhmmss_dash {
	    public String toString() {
	        return "dd-MM-yyyy HH:mm";
	    }
	},
	
	mmddyyyy_hhmmss_dash {
	    public String toString() {
	        return "MM-dd-yyyy HH:mm";
	    }
	},
	
	yyyyddmm_hhmmss_dash {
	    public String toString() {
	        return "yyyy-dd-MM HH:mm";
	    }
	}
	
}
