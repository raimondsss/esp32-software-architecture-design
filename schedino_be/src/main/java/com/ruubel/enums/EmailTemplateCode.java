package com.ruubel.enums;

public enum EmailTemplateCode {

	ACCOUNT_REGISTRATION {
		public String toString() {
			return "ACCOUNT_REGISTRATION";
		}
	},
	
	FORGOT_PASSWORD_REQUEST {
		public String toString() {
			return "FORGOT_PASSWORD_REQUEST";
		}
	}, 

	CHANGE_PASSWORD_REQUEST {
		public String toString() {
			return "CHANGE_PASSWORD_REQUEST";
		}
	}, 
	
};
