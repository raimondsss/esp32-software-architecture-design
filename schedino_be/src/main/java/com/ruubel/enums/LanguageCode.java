package com.ruubel.enums;

public enum LanguageCode {
	
		it {
		    public String toString() {
		        return "it";
		    }
		},
		 
		en {
		    public String toString() {
		        return "en";
		    }
		},
		 
		fr {
		    public String toString() {
		        return "fr";
		    }
		},
		 
		es {
		    public String toString() {
		        return "es";
		    }
		};

};
