package com.ruubel.enums;

public enum DateTimeFormat {
	
	//slash
	
	ddmmyyyy_hhmmss_slash {
	    public String toString() {
	        return "dd/mm/yyyy hh:mm:ss";
	    }
	},
	
	mmddyyyy_hhmmss_slash {
	    public String toString() {
	        return "mm/dd/yyyy hh:mm:ss";
	    }
	},
	
	yyyyddmm_hhmmss_slash {
	    public String toString() {
	        return "yyyy/dd/mm hh:mm:ss";
	    }
	},
	
	
	//point
	ddmmyyyy_hhmmss_point {
	    public String toString() {
	        return "dd.mm.yyyy hh.mm.ss";
	    }
	},
	
	mmddyyyy_hhmmss_point {
	    public String toString() {
	        return "mm.dd.yyyy hh.mm.ss";
	    }
	},
	yyyyddmm_hhmmss_point {
	    public String toString() {
	        return "yyyy.dd.mm hh.mm.ss";
	    }
	},
	
	//dash
	ddmmyyyy_hhmmss_dash {
	    public String toString() {
	        return "dd-mm-yyyy hh:mm:ss";
	    }
	},
	
	mmddyyyy_hhmmss_dash {
	    public String toString() {
	        return "mm-dd-yyyy hh:mm:ss";
	    }
	},
	
	yyyyddmm_hhmmss_dash {
	    public String toString() {
	        return "yyyy-dd-mm hh:mm:ss";
	    }
	}
	
}
